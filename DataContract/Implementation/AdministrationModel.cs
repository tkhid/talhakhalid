﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataContract.Implementation
{

    #region Common
    public class ReqFilePathsVM
    {
        public int FilePathID { get; set; }
        public string TransactionID { get; set; }
        public string FilePathText { get; set; }
        public string FileName { get; set; }
        public int? FileTypeID { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? FeedbackID { get; set; }
        public int? ProductID { get; set; }
        public int? StaffID { get; set; }
        public int? ModificationID { get; set; }
        public int FileTypeStatusID { get; set; }
        public FileTypesVM FileTypes { get; set; }
    }
    public class FileTypesVM
    {
        public int FileTypeID { get; set; }
        public int FilePathID { get; set; }
        public string FileName { get; set; }
        public string FileTypeCode { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? CreatedBy { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
    }
    //public class GenericResponseViewModel
    //{
    //    public int StatusCode { get; set; }
    //    public string Message { get; set; }
    //    public int ReturnID { get; set; }

    //}
    public class LoginModel
    {

        public string UserName { get; set; }
        public string Password { get; set; }

    }
    public class GetUserParamModel
    {
        public List<int> RoleID { get; set; }
        public int UserTypeID { get; set; }

    }
    public class RolePermissionsModel
    {
        public long RoleID { get; set; }
        public long ActionID { get; set; }
    }
    public class PublicUserViewModel
    {

        public int StatusCode { get; set; }
        public string Message { get; set; }

    }
    public class CountryListViewModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<CountryModel> Data { get; set; }

    }
    public class UserListViewModel
    {

        public string UserDisplayName { get; set; }
        public string UserDispalyStatus { get; set; }
        public string UserName { get; set; }
        public string CypherKey { get; set; }
        public long UserID { get; set; }
        public string UserEmpID { get; set; }
        public string UserEID { get; set; }
        public string BusinessSystem { get; set; }
        public string Designation { get; set; }

        public long UserDesignationID { get; set; }
        public long UserOUID { get; set; }
        public string Nationality { get; set; }
        public string CreationDate { get; set; }
        public string SuspensionDate { get; set; }
        public string OrganizationalUnit { get; set; }
        public int UserStatus { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }
        public string UserDeviceUDID { get; set; }
        public long CountryID { get; set; }
        public long UserTypeID { get; set; }
        public long BusinessSystemID { get; set; }
        public long ActionGroupID { get; set; }
        public long CompanyID { get; set; }
    }

    public class UserTypeModel
    {
        public long UserTypeID { get; set; }
        public string UserType { get; set; }
    }

    public class CountryModel
    {
        public long CountryID { get; set; }
        public string CountryNameAr { get; set; }
        public string CountryNameEn { get; set; }

    }
    public class DesignationModel
    {
        public long DesignationID { get; set; }
        public string DesignationAr { get; set; }
        public string DesignationEn { get; set; }

    }
    public class CityModel
    {
        public long CityID { get; set; }
        public Nullable<long> CountryID { get; set; }
        public string CityNameAr { get; set; }
        public string CityNameEn { get; set; }

    }
    public class DesignationViewModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<DesignationModel> Data { get; set; }

    }
    public class UserTypeViewModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<UserTypeModel> Data { get; set; }

    }

    public class UserJasonModel
    {

        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserEmpID { get; set; }
        public string DisplayName { get; set; }
        public string UserEID { get; set; }
        public string serNationalityID { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }
        public string UserTypeID { get; set; }
        public string UserDesignationID { get; set; }
        public string UserDeviceUDID { get; set; }
        public string UserOUID { get; set; }
        public string UserCompanyID { get; set; }
        public string BusinessSystemID { get; set; }
    }
    #endregion

    #region OrgUnit

    public class OrgUnitsViewModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<OrgUnitsModel> Data { get; set; }

    }

    public class GenericOrgUnitModel
    {
        public OrgUnitsModel Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }
    }
    public class OrgUnitFetchViewModel
    {
        public OrgUnitDataModel Data { get; set; }
        public long StatusCode { get; set; }
        public string Message { get; set; }

    }
    public class OrgUnitDataModel
    {
        public List<OrgUnitsModel> Data { get; set; }
        public GridActivitiesModel GridActivities { get; set; }
    }
    public class OrgUnitSearchPagerModel
    {
        public OrgUnitSearchModel Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }

    }
    public class OrgUnitsModel
    {
        public long OUID { get; set; }
        public long ParentID { get; set; }
        public string UserID { get; set; }
        public string OUNameAr { get; set; }
        public string OUDescAr { get; set; }
        public string OUNameEn { get; set; }
        public string OUDescEn { get; set; }
        public Nullable<long> OUHead { get; set; }
    }

    #endregion

    #region Business Systems
    public class ActionTypeViewModel
    {
        public List<ActionTypeModel> Data { get; set; }
        public long StatusCode { get; set; }
        public string Message { get; set; }

    }

    public class ActionTypeModel
    {
        public long ActionTypeID { get; set; }
        public string ActionName { get; set; }
    }

    public class BusinessSystemsListViewModel
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<BusinessSystemsModel> Data { get; set; }

    }

    public class GeneriBusinessSystemsModel
    {
        public BusinessSystemsModel Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }
    }
    public class BusinessSystemsFetchViewModel
    {
        public BusinessSystemsDataModel Data { get; set; }
        public long StatusCode { get; set; }
        public string Message { get; set; }

    }
    public class BusinessSystemsDataModel
    {
        public List<BusinessSystemsModel> Data { get; set; }
        public GridActivitiesModel GridActivities { get; set; }
    }
    public class BusinessSystemsSearchPagerModel
    {
        public BusinessSystemsSearchModel Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }

    }

    public class BusinessSystemsModel
    {
        public long BusinessSystemID { get; set; }
        public string UserID { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
        public long? BusinessSystemOwnerOUID { get; set; }
        public string BusinessSystemDescAr { get; set; }
        public string BusinessSystemDescEn { get; set; }
    }

    public class OrgUnitListViewModel
    {
        public List<OrgUnitListModel> Data { get; set; }
        public long StatusCode { get; set; }
        public string Message { get; set; }

    }

    public class OrgUnitListModel
    {
        public long OUID { get; set; }
        public long ParentID { get; set; }
        public string OUNameAr { get; set; }
        public string OUDescAr { get; set; }
        public string OUNameEn { get; set; }
        public string OUDescEn { get; set; }
        public Nullable<long> OUHead { get; set; }
        public string OUHeadEn { get; set; }
        public string OUHeadAr { get; set; }
    }


    public class ActionGroupListViewModel
    {
        public List<ActionGroupListModel> Data { get; set; }
        public long StatusCode { get; set; }
        public string Message { get; set; }

    }

    public class ActionGroupListModel
    {
        public long ActionGroupID { get; set; }
        public long OUID { get; set; }
        public long ParentID { get; set; }
        public string UserID { get; set; }
        public string ActionGroupNameAr { get; set; }
        public string ActionGroupDescAr { get; set; }
        public string ActionGroupNameEn { get; set; }
        public string ActionGroupParentNameEN { get; set; }
        public string ActionGroupParentNameAr { get; set; }
        public string ActionGroupDescEn { get; set; }
    }

    public class BusinessScreenModel
    {
        public string UserID { get; set; }
        public long ScreenID { get; set; }
        public long BusinessSystemID { get; set; }
        public string ScreenNameAr { get; set; }
        public string ScreenDescAr { get; set; }
        public string ScreenNameEn { get; set; }
        public string ScreenDescEn { get; set; }
        public Nullable<bool> Status { get; set; }
    }

    public class BusinessScreenActionModel
    {
        public string UserID { get; set; }
        public long ActionID { get; set; }
        public long ScreenID { get; set; }
        public string ActionName { get; set; }
        public string ActionDesc { get; set; }
        public string ActionCommunicationMethod { get; set; }
        public Nullable<long> ActionTypeID { get; set; }
        public Nullable<bool> Status { get; set; }
    }
    #endregion

    #region Screen List

    public class ScreenListViewModel
    {
        public List<ScreenListModel> Data { get; set; }
        public long StatusCode { get; set; }
        public string Message { get; set; }

    }
    public class ScreenListModel
    {
        public long ScreenID { get; set; }
        public long BusinessSystemID { get; set; }
        public string ScreenNameAr { get; set; }
        public string ScreenDescAr { get; set; }
        public string ScreenNameEn { get; set; }
        public string ScreenDescEn { get; set; }
    }

    public class ScreenActionListViewModel
    {
        public List<ScreenActionListModel> Data { get; set; }
        public long StatusCode { get; set; }
        public string Message { get; set; }

    }
    public class ScreenActionListModel
    {
        public long ActionID { get; set; }
        public long ActionTypeID { get; set; }
        public long ScreenID { get; set; }
        public string ActionName { get; set; }
        public string ActionDesc { get; set; }
        public string ActionCommunicationMethod { get; set; }
    }

    #endregion

    #region Role Management

    public class RolesModel
    {
        public long RoleID { get; set; }
        public string UserID { get; set; }
        public string ActionsIDList { get; set; }
        public string RoleNameAr { get; set; }
        public string RoleDescAr { get; set; }
        public string RoleNameEn { get; set; }
        public string RoleDescEn { get; set; }
        public long BusinessSystemID { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public Nullable<bool> Status { get; set; }
    }
    public class RolesViewModel
    {
        public List<RolesModel> Data { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
    public class RolesPermissionsViewModel
    {
        public List<RolePermissionsModel> Data { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
    public class UpdateRolesViewModel
    {
        public RolesModel Data { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
    public class ScreensActionsListModel
    {
        public long ScreenID { get; set; }
        public string ScreenNameAr { get; set; }
        public string ScreenNameEn { get; set; }
        public List<ActionsListModel> ActionsList { get; set; }
    }
    public class ActionsListModel
    {
        public long ActionID { get; set; }
        public string ActionName { get; set; }
    }
    public class ScreenActionsViewModel
    {
        public List<ScreensActionsListModel> Data { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
    #endregion
}
