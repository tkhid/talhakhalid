﻿using System;
using System.Collections.Generic;
namespace DataContract.Implementation
{
    public class InspectionPlanSearchPagerModel
    {
        public InspectionPlanSearchModel Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }

    }

    public class InspectionPlanSearchModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int PlanStatusId { get; set; }
        public long InspectionPlanId { get; set; }
        public long UserId { get; set; }
    }

    public class InspectionPlanListandGridDataModel
    {
        public List<InspectionPlanListModel> Data { get; set; }
        public GridActivitiesModel GridData { get; set; }
    }

    //public class InspectionPlanVersionsListandGridDataModel
    //{
    //    public List<InspectionPlanVersionModel> Data { get; set; }
    //    public GridActivitiesModel GridData { get; set; }
    //}

    public class InspectionPlanListModel
    {
        public long InspectionPlanID { get; set; }
        public long UserID { get; set; }
        public long RoleId { get; set; }
        public long InspectionPlanVersionNo { get; set; }
        public string InspectionPlanDisplayID { get; set; }
        public string InspectionPlanDescription { get; set; }
        public Nullable<long> InspectionPlanCreatorID { get; set; }
        public string InspectionPlanCreatorName { get; set; }
        public Nullable<System.DateTime> InspectionPlanDate { get; set; }
        public string InspectionPlanDateStr { get; set; }
        public long InspectionPlanStatusID { get; set; }
        public string InspectionPlanStatusEn { get; set; }
        public string InspectionPlanStatusAr { get; set; }
        public Nullable<System.DateTime> InspectionPlanLastChangeDate { get; set; }
        public string InspectionPlanLastChangeDateStr { get; set; }
        public Nullable<int> InspectionPlanYear { get; set; }
        public long ActionID { get; set; }
        public string InspectionPlanComment { get; set; }
        public List<InspectionActivityListModel> InspectionActivityLists { get; set; }
        public List<InspectionPlanHistoryModel> InspectionPlanHistories { get; set; }
        public InspectionPlanStatusModel InspectionPlanStatu { get; set; }
        public List<InspectionPlanVersionModel> InspectionPlanVersions { get; set; }
    }

    public class InspectionPlanStatusModel
    {
        public long InspectionPlanStatusID { get; set; }
        public string InspectionPlanStatusAr { get; set; }
        public string InspectionPlanStatusEn { get; set; }
        public List<InspectionPlanHistoryModel> InspectionPlanHistories { get; set; }
        public List<InspectionPlanListModel> InspectionPlanLists { get; set; }
    }

    public class InspectionPlanHistoryModel
    {
        public long InspectionPlanHistoryID { get; set; }
        public long InspectionPlanID { get; set; }
        public string ChangeDescription { get; set; }
        public Nullable<long> ChangeMadeByID { get; set; }
        public string ChangeMadeByName { get; set; }

        public Nullable<System.DateTime> ChangeDate { get; set; }
        public long InspectionPlanStatusAfterChange { get; set; }

        public string InspectionPlanStatusAfterChangeEn { get; set; }
        public string InspectionPlanStatusAfterChangeAr { get; set; }

        public InspectionPlanListModel InspectionPlanList { get; set; }
        public InspectionPlanStatusModel InspectionPlanStatu { get; set; }

    }

    public class InspectionPlanVersionModel
    {
        public long InspectionPlanVersionID { get; set; }
        public long InspectionPlanID { get; set; }
        public Nullable<long> VersionNumber { get; set; }
        public string InspectionPlanDescription { get; set; }
        public Nullable<long> InspectionPlanCreatorID { get; set; }
        public string InspectionPlanCreatorName { get; set; }

        public Nullable<System.DateTime> InspectionPlanDate { get; set; }
        public Nullable<long> InspectionPlanStatusID { get; set; }
        public string InspectionPlanStatusNameEn { get; set; }
        public string InspectionPlanStatusNameAr { get; set; }
        public Nullable<System.DateTime> InspectionPlanLastChangeDate { get; set; }

        public InspectionPlanListModel InspectionPlanList { get; set; }
        public InspectionPlanStatusModel InspectionPlanStatu { get; set; }
    }

    public class InspectionActivityListModel
    {
        public long InspectionActivityID { get; set; }
        public long InspectionPlanID { get; set; }
        public string InspctionActivityDescriptionAr { get; set; }
        public string InspectionActivityDescriptionEn { get; set; }
        public long InsuranceCompanyID { get; set; }
        public long InspectionPlanVersionID { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        public string InsuranceCompanyLocation { get; set; }
        public Nullable<System.DateTime> InspectionActivityStartDate { get; set; }
        public string InspectionActivityStartDateStr { get; set; }
        public Nullable<System.DateTime> InspectionActivityCloseDate { get; set; }
        public string InspectionActivityCloseDateStr { get; set; }
        public long InspectionTypeID { get; set; }
        public string InspectionTypeNameEn { get; set; }
        public string InspectionTypeNameAr { get; set; }
        public long InspectionActivityStatusID { get; set; }
        public string InspectionActivityStatusNameEn { get; set; }
        public string InspectionActivityStatusNameAr { get; set; }
        public long ActionID { get; set; }
        public InspectionActivityStatusModel InspectionActivityStatus { get; set; }
        public List<ExecutionSummaryReportListModel> ExecutionSummaryReportLists { get; set; }
        public List<InspectionActivityHistoryModel> InspectionActivityHistories { get; set; }
        public List<InspectionActivityInspectorsRelationModel> InspectionActivityInspectorsRelations { get; set; }
        public string InspectionActivityInspectionTeam { get; set; }
        public InspectionPlanListModel InspectionPlanList { get; set; }
        public InspectionTypeModel InspectionType { get; set; }
        public List<InspectionActivityRequirementModel> InspectionActivityRequirements { get; set; }
        public List<InspectionActivityRequirementsAttachmentModel> InspectionActivityRequirementsAttachments { get; set; }
        public List<OffsiteReportListModel> OffsiteReportLists { get; set; }
        public List<OnsiteReportListModel> OnsiteReportLists { get; set; }
    }

    public class InspectinPlanAddCompaniesModel
    {
        public long InspectionPlanID { get; set; }
        public List<long> CompaniesIdList { get; set; }
    }

    public class ExecutionSummaryReportListModel
    {
        public long ExecutionSummaryReportID { get; set; }
        public string ExecutionSummaryReportDisplayID { get; set; }
        public long InspectionActivityID { get; set; }
        public Nullable<System.DateTime> ExecutionSummaryReportDate { get; set; }
        public string ExecutionSummaryReportSubject { get; set; }
        public string ExecutionSummaryReportHeader { get; set; }
        public string ExecutionSummaryReportFooter { get; set; }
        public long ExecutionSummaryReportApproverID { get; set; }
        public Nullable<System.DateTime> ExecutionSummaryReportApprovalDate { get; set; }

        public List<ExecutionSummaryReportHistoryModel> ExecutionSummaryReportHistories { get; set; }
        public InspectionActivityListModel InspectionActivityList { get; set; }
    }

    public class ExecutionSummaryReportHistoryModel
    {
        public long ExecutionSummaryReportHistoryID { get; set; }
        public long ExecutionSummaryReportID { get; set; }
        public string ChangeDescription { get; set; }
        public long ChangeMadeByID { get; set; }
        public Nullable<System.DateTime> ChangeDate { get; set; }
        public Nullable<long> ExecutionSummaryReportStatusAfterChange { get; set; }

        public ExecutionSummaryReportListModel ExecutionSummaryReportList { get; set; }
        public ExecutionSummaryReportStatusModel ExecutionSummaryReportStatu { get; set; }
    }

    public class ExecutionSummaryReportStatusModel
    {
        public long ExecutionSummaryReportStatusID { get; set; }
        public string ExecutionSummaryReportStatusAr { get; set; }
        public string ExecutionSummaryReportStatusEn { get; set; }

        public List<ExecutionSummaryReportHistoryModel> ExecutionSummaryReportHistories { get; set; }
    }

    public class InspectionActivityHistoryModel
    {
        public long InspectionActivityHistoryID { get; set; }
        public long InspectionActivityID { get; set; }
        public string ChangeDescription { get; set; }
        public long ChangeMadeByID { get; set; }
        public Nullable<System.DateTime> ChangeDate { get; set; }
        public Nullable<long> InspectionActivityStatusAfterChange { get; set; }

        public InspectionActivityListModel InspectionActivityList { get; set; }
        public InspectionActivityStatusModel InspectionActivityStatus { get; set; }
    }

    public partial class InspectionActivityStatusModel
    {
        public long InspectionActivityStatusID { get; set; }
        public string InspectionActivityStatusAr { get; set; }
        public string InspectionActivityStatusEn { get; set; }

        public List<InspectionActivityHistoryModel> InspectionActivityHistories { get; set; }
    }

    public class InspectionActivityInspectorsRelationModel
    {
        public long InspIR { get; set; }
        public long InspectionActivityID { get; set; }
        public long InspectorID { get; set; }
        public string InspectorName { get; set; }

        public InspectionActivityListModel InspectionActivityList { get; set; }
    }

    public class InspectionTypeModel
    {
        public long InspectionTypeID { get; set; }
        public string InspectionTypeAr { get; set; }
        public string InspectionTypeEn { get; set; }

        public List<InspectionActivityListModel> InspectionActivityLists { get; set; }
    }

    public class InspectionActivityRequirementModel
    {
        public long InspectionActivityRequirementID { get; set; }
        public long InspectionActivityID { get; set; }
        public string InspectionActivityRequirementsList { get; set; }

        public InspectionActivityListModel InspectionActivityList { get; set; }
    }

    public class InspectionActivityRequirementsAttachmentModel
    {
        public long InspectionActivityRequirementAttachmentID { get; set; }
        public long InspectionActivityID { get; set; }
        public string AttachmentDescription { get; set; }
        public string AttachmentFileLocation { get; set; }
        public long AttachmentAddedByID { get; set; }
        public Nullable<System.DateTime> AttachmentDate { get; set; }

        public InspectionActivityListModel InspectionActivityList { get; set; }
    }

    public class OffsiteReportListModel
    {
        public long OffsiteReportID { get; set; }
        public string OffsiteReportDisplayID { get; set; }
        public long InspectionActivityID { get; set; }
        public Nullable<System.DateTime> OffsiteReportDate { get; set; }
        public string OffsiteReportDescription { get; set; }
        public string OffsiteReportDetails { get; set; }
        public long OffsiteReportAddedByID { get; set; }

        public InspectionActivityListModel InspectionActivityList { get; set; }
    }

    public class OnsiteReportListModel
    {

        public long OnsiteReportID { get; set; }
        public string OnsiteReportDisplayID { get; set; }
        public long InspectionActivityID { get; set; }
        public Nullable<System.DateTime> OnsiteReportDate { get; set; }
        public long OnsiteReportAddedByID { get; set; }

        public InspectionActivityListModel InspectionActivityList { get; set; }
        public List<SubCatDetailOnsiteReportRelationModel> SubCatDetailOnsiteReportRelations { get; set; }
    }

    public class SubCatDetailOnsiteReportRelationModel
    {
        public long OnsiteReportDetailID { get; set; }
        public long OnsiteReportID { get; set; }
        public long SOWSubCategoryDetailID { get; set; }
        public string OnsiteReportDetailComment { get; set; }
        public string OnsiteReportDetailScope { get; set; }
        public string OnsiteReportDetailFindings { get; set; }
        public string OnsiteReportDetailRecommendations { get; set; }
        public long OnsiteReportDetailResponsiblePartyID { get; set; }
        public Nullable<System.DateTime> OnsiteReportDetailDeadline { get; set; }
        public Nullable<bool> OnsiteReportDetailClosed { get; set; }

        public OnsiteReportListModel OnsiteReportList { get; set; }
        public ScopeOfWorkSubCategoryDetailModel ScopeOfWorkSubCategoryDetail { get; set; }
    }
}
