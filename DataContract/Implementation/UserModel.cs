﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataContract.Implementation
{


    public class UserModel
    {

        public long UserID { get; set; }
        public string UserName { get; set; }
        public string UserEmpID { get; set; }
        public string UserEID { get; set; }
        public Nullable<long> UserNationalityID { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }
        public Nullable<long> UserTypeID { get; set; }
        public Nullable<long> UserDesignationID { get; set; }
        public string UserDeviceUDID { get; set; }
        public Nullable<long> UserOUID { get; set; }
        public Nullable<long> UserCompanyID { get; set; }
        public byte[] UserSignature { get; set; }
        public byte[] ProfileImage { get; set; }
        public Nullable<bool> UserStatus { get; set; }
        public Nullable<long> BusinessSystemID { get; set; }
        public string DesignationEn { get; set; }
        public string UserDisplayName { get; set; }
        public string DesignationAr { get; set; }
        public string CountryNameEn { get; set; }
        public string CountryNameAr { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public string UserTypeEn { get; set; }
        public string UserTypeAr { get; set; }
        public string OUNameAr { get; set; }
        public string OUNameEn { get; set; }
        public long CountryID { get; set; }
        public long ActionGroupID { get; set; }
        public string AccessToken { get; set; }
        public string CreationDate { get; set; }
        public string SuspensionDate { get; set; }
        public string Password { get; set; }
        public DateTime LinkExp { get; set; }
        public int LoginAttempt { get; set; }
        public bool IsClicked { get; set; }
        public string UserDispalyStatus { get; set; }
        public List<int> RoleIDs { get; set; }
    }

    public class MainUserModel
    {

        public long UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserEmpID { get; set; }
        public string DisplayName { get; set; }
        public string UserEID { get; set; }
        public Nullable<long> UserNationalityID { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }
        public Nullable<long> UserTypeID { get; set; }
        public Nullable<long> UserDesignationID { get; set; }
        public string UserDeviceUDID { get; set; }
        public Nullable<long> UserOUID { get; set; }
        public Nullable<long> UserCompanyID { get; set; }
        public byte[] UserSignature { get; set; }
        public Nullable<long> BusinessSystemID { get; set; }
        public bool UserStatus { get; set; }
        public byte[] ProfileImage { get; set; }
        public Nullable<long> ActionGroupID { get; set; }
        public List<RolesList> RolesList { get; set; }
    }

    public class RolesList
    {
        public string UserID { get; set; }
        public string RoleID { get; set; }

    }
    public class PublicUserModel
    {
        public string UserName { get; set; }
        public string UserDisplayName { get; set; }
        public string UserPassword { get; set; }
        public string UserEID { get; set; }
        public long BusinessSystemID { get; set; }
        public Nullable<long> UserNationalityID { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }
    }

    public class RolesAndPermissionsModel
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public long BusinessSystemID { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public List<RoleModel> UserRoles { get; set; }
    }
    public class RoleModel
    {
        public long RoleID { get; set; }
        public string RoleNameAr { get; set; }
        public string RoleNameEn { get; set; }

        public List<RolePermissionModel> RolePermissions { get; set; }

    }
    public class RolePermissionModel
    {
        public long ScreenID { get; set; }
        public long RoleID { get; set; }
        public string ScreenNameAr { get; set; }
        public string ScreenNameEn { get; set; }
        public List<ScreenActionsModel> Actions { get; set; }

    }
    public class ScreenActionsModel
    {
        public long ActionID { get; set; }
        public string ActionName { get; set; }
        public string ActionDesc { get; set; }
        public long ActionTypeID { get; set; }
        public string ActionCommunicationMethod { get; set; }


    }
    public class RolesAndPermissionsMianModel
    {
        public long UserID { get; set; }
        public string UserNameEn { get; set; }
        public string UserNameAr { get; set; }
        public long BusinessSystemID { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public long RoleID { get; set; }
        public string RoleNameEn { get; set; }
        public string RoleNameAr { get; set; }
        public long ScreenID { get; set; }
        public string ScreenNameEn { get; set; }
        public string ScreenNameAr { get; set; }
        public long ActionID { get; set; }
        public string ActionName { get; set; }
        public string ActionDesc { get; set; }
        public string ActionCommunicationMethod { get; set; }

    }


    public class UserTasksModel
    {
        public long TaskID { get; set; }
        public Nullable<long> OriginSystemID { get; set; }
        public Nullable<System.DateTime> TaskDate { get; set; }
        public string TaskDescription { get; set; }
        public string TaskRecepientList { get; set; }
        public Nullable<long> TaskStatus { get; set; }
        public string TaskURL { get; set; }

    }

    public class UserTasksInputModel
    {
        public long OriginSystemID { get; set; }
        public string UserID { get; set; }
        public string TaskDescription { get; set; }
        public string TaskRecepientList { get; set; }
        public long TaskStatus { get; set; }
        public string TaskURL { get; set; }
        public long ActionID { get; set; }
        public string OUID { get; set; }
        public bool IsUrgent { get; set; }

    }

    public class NotificationModel
    {
        public string UserID { get; set; }
        public string RoleID { get; set; }
        public long ActionTypeID { get; set; }
    }

    public class UserBasicModel
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }
        public string DesignationEn { get; set; }
        public string UserDisplayName { get; set; }
        public string DesignationAr { get; set; }
        
      
    }
}