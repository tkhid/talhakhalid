﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataContract.Implementation
{
    public class ScopeOfWorkCommentsModel
    {
        public long SOWID { get; set; }
        public string SourceComponentID { get; set; }
        public string CommentDescription { get; set; }
        public long CommentAddedByID { get; set; }
        public long SOWStatusID { get; set; }
        public long ActionID { get; set; }
    }

    public partial class ScopeOfWorkDetailModel
    {
        public long SOWDetailID { get; set; }
        public long SOWID { get; set; }
        public long SOWMainCategoryID { get; set; }
        public Nullable<long> SOWCreatorID { get; set; }
        public Nullable<System.DateTime> SOWDate { get; set; }
        public long SOWStatusID { get; set; }
        public long SOWTypeID { get; set; }
        public Nullable<bool> SOWActiveState { get; set; }
        public Nullable<System.DateTime> SOWLastChangeDate { get; set; }

        //public ScopeOfWorkListModel ScopeOfWorkList { get; set; }
        //public ScopeOfWorkMainCategoryModel ScopeOfWorkMainCategory { get; set; }
        //public ScopeOfWorkStatusModel ScopeOfWorkStatu { get; set; }
        //public ScopeOfWorkTypeModel ScopeOfWorkType { get; set; }
    }

    public class ScopeOfWorkListModel
    {

        public long SOWID { get; set; }
        public string SOWDisplayID { get; set; }
        public string SOWDescriptionEn { get; set; }
        public string SOWDescriptionAr { get; set; }
        public Nullable<long> SOWCreatorID { get; set; }
        public Nullable<System.DateTime> SOWDate { get; set; }
        public long SOWStatusID { get; set; }
        public long SOWTypeID { get; set; }
        public Nullable<bool> SOWActiveState { get; set; }
        public Nullable<System.DateTime> SOWLastChangeDate { get; set; }
        public long ActionID { get; set; }

        /* UnComment Below User If Arabic Description require from controller**/
        //public string ChangeDescriptionText { get; set; }
        //public string ChangeTypeText { get; set; }
        //public string ChangeState { get; set; }

        //public List<ScopeOfWorkDetailModel> ScopeOfWorkDetails { get; set; }
        //public ScopeOfWorkStatusModel ScopeOfWorkStatus { get; set; }
        //public ScopeOfWorkTypeModel ScopeOfWorkType { get; set; }
        //public List<ScopeOfWorkSubCatDetailRelationModel> ScopeOfWorkSubCatDetailRelations { get; set; }
        //public List<ScopeOfWorkHistoryModel> SOWHistories { get; set; }
    }

    public class ScopeOfWorkStatusModel
    {

        public long SOWStatusID { get; set; }
        public string SOWStatusAr { get; set; }
        public string SOWStatusEn { get; set; }

        //public List<ScopeOfWorkDetailModel> ScopeOfWorkDetails { get; set; }
        //public List<ScopeOfWorkListModel> ScopeOfWorkLists { get; set; }
        //public List<ScopeOfWorkHistoryModel> SOWHistories { get; set; }
    }

    public class ScopeOfWorkSubCatDetailRelationModel
    {
        public long SOWRID { get; set; }
        public long SOWID { get; set; }
        public long SOWSubCategoryID { get; set; }
        public long SOWSubCategoryDetailID { get; set; }

        //public ScopeOfWorkListModel ScopeOfWorkList { get; set; }
        //public ScopeOfWorkSubCategoryModel ScopeOfWorkSubCategory { get; set; }
        //public ScopeOfWorkSubCategoryDetailModel ScopeOfWorkSubCategoryDetail { get; set; }
    }


    public class ScopeOfWorkSubCategoryModel
    {


        public long SOWSubCategoryID { get; set; }
        public long SOWMainCategoryID { get; set; }
        public string SOWSubCategoryNameAr { get; set; }
        public string SOWSubCategoryNameEn { get; set; }
        public string SOWSubCategoryDescriptionAr { get; set; }
        public string SOWSubCategoryDescriptionEn { get; set; }
        public string SOWSubCategoryRelatedLawDescription { get; set; }
        public string SOWSubCategoryRelatedLawDetails { get; set; }
        public long ActionID { get; set; }
        //public ScopeOfWorkMainCategoryModel ScopeOfWorkMainCategory { get; set; }
        //public List<ScopeOfWorkSubCatDetailRelationModel> ScopeOfWorkSubCatDetailRelations { get; set; }
        //public List<ScopeOfWorkSubCategoryDetailModel> ScopeOfWorkSubCategoryDetails { get; set; }

    }


    public partial class ScopeOfWorkSubCategoryDetailModel
    {
        public long _Sowid { get; set; }
        public long SOWSubCategoryDetailID { get; set; }
        public long SOWSubCategoryID { get; set; }
        public string SOWSubCategoryDetailAr { get; set; }
        public string SOWSubCategoryDetailEn { get; set; }

        public string SOWSubCategoryDetailRelatedLawDescription { get; set; }
        public string SOWSubCategoryDetailRelatedLawDetails { get; set; }

        public long ActionID { get; set; }
        //public  List<ExecutionSummaryReportDetail> ExecutionSummaryReportDetails { get; set; }
        //public  List<RecommendationsLetterDetail> RecommendationsLetterDetails { get; set; }
        public List<ScopeOfWorkSubCatDetailRelationModel> ScopeOfWorkSubCatDetailRelations { get; set; }
        //    public ScopeOfWorkSubCategoryModel ScopeOfWorkSubCategory { get; set; }
        //public  List<SubCatDetailOnsiteReportRelation> SubCatDetailOnsiteReportRelations { get; set; }
    }



    public class ScopeOfWorkTypeModel
    {

        public long SOWTypeID { get; set; }
        public string SOWTypeAr { get; set; }
        public string SOWTypeEn { get; set; }

        //public List<ScopeOfWorkDetailModel> ScopeOfWorkDetails { get; set; }
        //public List<ScopeOfWorkListModel> ScopeOfWorkLists { get; set; }
    }

    public partial class ScopeOfWorkHistoryModel
    {
        public long SOWHistoryID { get; set; }
        public long SOWID { get; set; }
        public string ChangeDescription { get; set; }
        public long ChangeMadeByID { get; set; }
        public Nullable<System.DateTime> ChangeDate { get; set; }
        public Nullable<long> SOWStatusAfterChange { get; set; }


    }

    public class ScopeOfWorkMainCategoryModel
    {
        public long SOWID { get; set; }
        public long SOWMainCategoryID { get; set; }
        public string SOWMainCategoryNameAr { get; set; }
        public string SOWMainCategoryNameEn { get; set; }
        public string SOWMainCategoryDescriptionAr { get; set; }
        public string SOWMainCategoryDescriptionEn { get; set; }
        public long ActionID { get; set; }
        public List<ScopeOfWorkDetailModel> ScopeOfWorkDetails { get; set; }
        // public List<ScopeOfWorkSubCategoryModel> ScopeOfWorkSubCategories { get; set; }
    }
    public class SOWSearchPagerModel
    {
        public SOWSearchModel Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }


    }
    public class GenericScopeOfWorkModel
    {
        public GenericSearch Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }
    }
    public class GenericSearch
    {
        public long SOWID { get; set; }
        public long SOWMainCategoryID { get; set; }
        public long SOWSubCategoryID { get; set; }
        public long SOWSubCategoryDetailID { get; set; }
    }
    public class SOWSearch
    {
        public long SOWID { get; set; }
        public long SOWMainCategoryID { get; set; }
        public long SOWSubCategoryID { get; set; }
        public long SOWSubCategoryDetailID { get; set; }
    }
    //public class SOWListandGridDataModel
    //{
    //    public List<SOWSearchModel> Data { get; set; }
    //    public GridActivitiesModel GridData { get; set; }
    //}

    public class SOWSearchModel
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public long SowStatusId { get; set; }
        public long SowTypeId { get; set; }

        //public string PageNumber { get; set; }
        //public string PageSize { get; set; }
        //public string SortColum { get; set; }
        //public string SortDir { get; set; }

    }

}
