﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContract.Implementation
{
    public class PageModel
    {
        public long Page_Id { get; set; }
        public string Page_Name_En { get; set; }
        public string Page_Name { get; set; }
        public string Page_Content_En { get; set; }
        public string page_ShortDesc_En { get; set; }
        public string page_ShortDesc_Ar { get; set; }
        public Nullable<long> Parent_Id { get; set; }
        public string ParentName { get; set; }
        public string Meta_Title_En { get; set; }
        public string Meta_Keywords_En { get; set; }
        public string Meta_Description_En { get; set; }
        public Nullable<bool> IsStandalone { get; set; }
        public string Page_Name_Ar { get; set; }
        public string Page_Content_Ar { get; set; }
        public string ImgUrl { get; set; }
        public string Meta_Title_Ar { get; set; }
        public string Meta_Keywords_Ar { get; set; }
        public string Meta_Description_Ar { get; set; }
        public Nullable<long> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<long> Updated_By { get; set; }
        public Nullable<System.DateTime> Updated_Date { get; set; }
        public Nullable<long> Row_Status_Id { get; set; }
        public string RowStatus { get; set; }
        public List<long> PageMenus { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
        public List<PageModel> SubMenus { get; set; }
        public bool IsActive { get; set; }

    }

    public class EServicesListMainModel
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public List<EServicesListModel> Data { get; set; }


    }
    public class EServicesListModel
    {
        public string serviceIcon { get; set; }
        public string serviceNameEn { get; set; }
        public string serviceNameAr { get; set; }
        public string serviceURL { get; set; }
        public string serviceURLAr { get; set; }

    }


    public class NewsListMainModel
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public List<NewsListModel> Data { get; set; }

    }
    public class NewsListModel
    {
        public string newsImage { get; set; }
        public string date { get; set; }
        public string newsTitleAr { get; set; }
        public string newsTitleEn { get; set; }
        public string newsDetailAr { get; set; }
        public string newsDetailEn { get; set; }

    }


    public class ContactMainModel
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public List<ContactListModel> Data { get; set; }

    }
    public class ContactListModel
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string officeTitleAr { get; set; }
        public string officeTitleEn { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public string openingHoursAr { get; set; }
        public string OpeningHoursEn { get; set; }

    }

    public class EventsMainModel
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public List<EventsListModel> Data { get; set; }

    }
    public class EventsListModel
    {
        public string eventTitleAr { get; set; }
        public string eventTitleEn { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string webSiteAddress { get; set; }
        public string phoneNumber { get; set; }
        public string venueEn { get; set; }
        public string venueAr { get; set; }
        public string email { get; set; }
        public string fax { get; set; }

    }

    public class PublicationMainModel
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public List<PublicationsListModel> Data { get; set; }

    }
    public class PublicationsListModel
    {
        public string publicationIcon { get; set; }
        public string publicationTitleAr { get; set; }
        public string publicationTitleEn { get; set; }
        public string Edition { get; set; }
        public string publicationURL { get; set; }

    }
    public class mediaMainModel
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public List<mediaVideoModelList> Videos { get; set; }
        public List<mediaPhotoModelList> Photos { get; set; }
    }
    public class mediaVideoModelList
    {
        public string mediaURL { get; set; }
        public string mediaTitleAr { get; set; }
        public string mediaTitleEn { get; set; }

    }

    public class mediaPhotoModelList
    {
        public string mediaURL { get; set; }
        public string mediaTitleAr { get; set; }
        public string mediaTitleEn { get; set; }

    }

    public class AboutusMainModel
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public List<AboutusMainModelList> Data { get; set; }

    }

    public class AboutusMainModelList
    {
        public string aboutUsDetailAR { get; set; }
        public string aboutUsDetailEn { get; set; }
        public string aboutUsImageURL { get; set; }

    }
    public class SuggestionMainModel
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public SuggestionModelList Request { get; set; }
    }
    public class SuggestionModelList
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string phoneNumber { get; set; }
        public string message { get; set; }

    }
}
