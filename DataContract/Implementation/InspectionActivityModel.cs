﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataContract.Implementation
{
    public class InspectionActivityModel
    {

        public long InspectionActivityID { get; set; }
        public long InspectionPlanID { get; set; }
        public string InspctionActivityDescriptionAr { get; set; }
        public string InspectionActivityDescriptionEn { get; set; }
        public long InsuranceCompanyID { get; set; }
        public Nullable<System.DateTime> InspectionActivityStartDate { get; set; }
        public Nullable<System.DateTime> InspectionActivityCloseDate { get; set; }
        public long InspectionTypeID { get; set; }
        public long InspectionActivityStatusID { get; set; }
    }
    

    public class OffsiteReportModel
    {

        public long OffsiteReportID { get; set; }
        public string OffsiteReportDisplayID { get; set; }
        public long InspectionActivityID { get; set; }
        public Nullable<System.DateTime> OffsiteReportDate { get; set; }
        public string OffsiteReportDescription { get; set; }
        public string OffsiteReportDetails { get; set; }
        public long OffsiteReportAddedByID { get; set; }
    }

    public class OnsiteReportModel
    {

        public long OnsiteReportID { get; set; }
        public string OnsiteReportDisplayID { get; set; }
        public long InspectionActivityID { get; set; }
        public Nullable<System.DateTime> OnsiteReportDate { get; set; }
        public long OnsiteReportAddedByID { get; set; }
    }

   
}
