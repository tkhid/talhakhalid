﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DataContract.Implementation
{
    public class UrgentInspectionRequestModel
    {
    }
    public partial class UrgentInspectionRequestListModel
    {
        public long UrgentInspectionRequestID { get; set; }
        public string UrgentInspectionRequestDisplayID { get; set; }
        public System.DateTime UrgentInspectionRequestDate { get; set; }
        public long UrgentInspectionRequestDepartmentID { get; set; }
        public long UrgentInspectionRequestCompanyID { get; set; }
        public string UrgentInspectionRequestDetails { get; set; }
        public string UrgentInspectionRequestComments { get; set; }
        public long UrgentInspectionRequestStatusID { get; set; }
        public long UrgentInspectionRequestAddedByID { get; set; }
        public long ActionID { get; set; }

    }
    public class UrgentInspectionRequestStatusModel
    {

        public long UrgentInspectionStatusID { get; set; }
        public string UrgentInspectionStatusAr { get; set; }
        public string UrgentInspectionStatusEn { get; set; }
    }
    public class UIRSearchPagerModel
    {
        public UIRCustomSearchModel Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }


    }
    public class UIRCustomSearchModel
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public long CompanyId { get; set; }
        public long DepartmentId { get; set; }


    }
    public class UIRStatusModel
    {
        public long ActionID { get; set; }
        public long UrgentInspectionRequestID { get; set; }
        public long UpdatedStatus { get; set; }
        public string UrgentInspectionRequestComments { get; set; }

    }
}
