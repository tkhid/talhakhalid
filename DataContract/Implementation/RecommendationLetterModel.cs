﻿using IA.EC.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataContract.Implementation
{
    public class RecommendationLetterModel
    {
    }

    public class RecommendationsLetterListModel
    {
        public long RecommendationsLetterID { get; set; }
        public string RecommendationsLetterDisplayID { get; set; }
        public long InspectionActivityID { get; set; }
        public Nullable<System.DateTime> RecommendationsLetterDate { get; set; }
        public string RecommendationsLetterCover { get; set; }
        public Nullable<System.DateTime> RecommendationsLetterDeadline { get; set; }
        public Nullable<System.DateTime> RecommendationsLetterReminderDate { get; set; }
        public string RecommendationsLetterRemindingFrequency { get; set; }
        public long RecommendationsLetterStatusID { get; set; }
        public long RecommendationsLetterApprovedByID { get; set; }
        public long UserID { get; set; }
        public Nullable<System.DateTime> RecommendationsLetterApprovalDate { get; set; }

        public List<RecommendationLetterFeedbackModel> RecommendationLetterFeedbacks { get; set; }
        public List<RecommendationsLetterHistory> RecommendationsLetterHistories { get; set; }
        public RecommendationsLetterStatu RecommendationsLetterStatu { get; set; }
    }

    public class RecommendationLetterFeedbackModel
    {
        public long RecommendationLetterFeedbackID { get; set; }
        public long RecommendationLetterID { get; set; }
        public string RecommendationLetterFeedbackComment { get; set; }
        public string RecommendationLetterFeedbackAttachmentDescription { get; set; }
        public string RecommendationLetterFeedbackAttachmentFileLocation { get; set; }
        public Nullable<long> RecommendationLetterFeedbackAttachmentAddedByID { get; set; }
        public Nullable<System.DateTime> RecommendationLetterFeedbackAttachmentDate { get; set; }
        public long UserID { get; set; }

        public RecommendationsLetterList RecommendationsLetterList { get; set; }
    }
    public class RecommendationsLetterStatuModel
    {

        public long RecommendationsLetterStatusID { get; set; }
        public string RecommendationsLetterStatusStatusAr { get; set; }
        public string RecommendationsLetterStatusEn { get; set; }

        public List<RecommendationsLetterHistory> RecommendationsLetterHistories { get; set; }
        public List<RecommendationsLetterList> RecommendationsLetterLists { get; set; }
    }

    public class RecommendationsLetterDetailModel
    {
        public long RecommendationsLetterDetailID { get; set; }
        public long SOWSubCategoryDetailID { get; set; }

        public ScopeOfWorkSubCategoryDetail ScopeOfWorkSubCategoryDetail { get; set; }
    }


    public class RecommendationsLetterHistoryModel
    {
        public long RecommendationsLetterHistoryID { get; set; }
        public long RecommendationsLetterID { get; set; }
        public string ChangeDescription { get; set; }
        public Nullable<long> ChangeMadeByID { get; set; }
        public Nullable<System.DateTime> ChangeDate { get; set; }
        public Nullable<long> RecommendationsLetterStatusAfterChange { get; set; }

        public RecommendationsLetterList RecommendationsLetterList { get; set; }
        public RecommendationsLetterStatu RecommendationsLetterStatu { get; set; }
    }
}
