﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace DataContract.Implementation
{
    public class EInspectionCommentModel
    {
        public long CommentID { get; set; }
        public string SourceComponentID { get; set; }
        public string CommentDescription { get; set; }
        public Nullable<long> CommentAddedByID { get; set; }

        public string CommentDateString { get; set; }

        public Nullable<System.DateTime> CommentDate { get; set; }
    }

    public class EInspectionAttachmentModel
    {
        public long AttachmentID { get; set; }
        public string SourceComponentID { get; set; }
        public string AttachmentDescription { get; set; }
        public string AttachmentFileLocation { get; set; }
        public Nullable<long> AttachmentAddedByID { get; set; }
        public Nullable<System.DateTime> AttachmentDate { get; set; }
        public string AttachmentDateString { get; set; }

    }
}
