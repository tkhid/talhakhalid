﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataContract.Implementation
{
    public class NominationRequestModel
    {
        public long NominationRequestID { get; set; }
        public string NominationRequestDisplayID { get; set; }
        public DateTime NominationRequestDate { get; set; }
        public string NominationRequestLetterDraft { get; set; }
        public string DepartmentConcatenatedName { get; set; }
        public string DepartmentNameEn { get; set; }
        public string DepartmentNameAr { get; set; }
        public string NominationRequestDateString { get; set; }
        public long UserID { get; set; }
        public long ActionID { get; set; }
        public List<NominationRequestDepartmentList> NominationRequestDepartmentLists { get; set; }
        public List<NominationRequestDetailModel> NominationRequestDetails { get; set; }
    }

    public class NominationRequestDepartmentList
    {
        public long NominationRequestDepartmentID { get; set; }
        public long NominationRequestID { get; set; }
        public long NominationRequestOrgUnitID { get; set; }
    }

    public class NominationRequestDetailModel
    {
        public long NominationRequestDetailID { get; set; }
        public long NominationRequestID { get; set; }
        public long NominationRequestOrgUnitID { get; set; }
        public long NominationInsuranceCompanyID { get; set; }
        public string NominationReason { get; set; }
        public string OrgUnitNameEn { get; set; }
        public string OrgUnitNameAr { get; set; }
        public string InsuranceCompanyNameEn { get; set; }
        public string InsuranceCompanyNameAr { get; set; }
        public long UserID { get; set; }
        public DateTime NominationDate { get; set; }
        public int CurrentYearPlan { get; set; }
        public int FutureYearPlan { get; set; }
        public string NominationDateString { get; set; }
    }

    public class DepartmentListModel
    {

        public long OUID { get; set; }
        public string OUNameAr { get; set; }
        public string OUDescAr { get; set; }
        public string OUNameEn { get; set; }
        public string OUDescEn { get; set; }
        public long OUHead { get; set; }

    }

    public class InsuranceCompaniesModel
    {
        public long InsuranceCompanyID { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string InsuranceCompanyBuisnessNature { get; set; }
        public string CompanyAddress { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        public DateTime InsuranceCompanyFirstLicensingDate { get; set; }
        public DateTime InsuranceCompanyLastLicensingDate { get; set; }
        public DateTime InsuranceCompanyLastInspectionDate { get; set; }
        public string InsuranceCompanyFirstLicensingDateStr { get; set; }
        public string InsuranceCompanyLastLicensingDateStr { get; set; }
        public string InsuranceCompanyLastInspectionDateStr { get; set; }
        public string InsuranceCompanyLicensingNumber { get; set; }
        public string InsuranceCompanyStatus { get; set; }
        public string InsuranceCompanyNominationReason { get; set; }
        public string InsuranceCompanyNominatedByDepartmentEn { get; set; }
        public string InsuranceCompanyNominatedByDepartmentAr { get; set; }
        public DateTime InsuranceCompanyNominatedRequestDate { get; set; }
        public string InsuranceCompanyNominatedRequestDateStr { get; set; }
        public int TotalCount { get; set; }
        public int FilteredCount { get; set; }
        public bool IsAddedToPlan { get; set; }
    }

    public class InsuranceCompanyDetailModel
    {
        public int SequenceID { get; set; }
        public long InsuranceCompanyID { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string InsuranceCompanyBuisnessNature { get; set; }
        public string CompanyAddress { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        public DateTime InsuranceCompanyFirstLicensingDate { get; set; }
        public DateTime InsuranceCompanyLastLicensingDate { get; set; }
        public string InsuranceCompanyFirstLicensingDateStr { get; set; }
        public string InsuranceCompanyLastLicensingDateStr { get; set; }
        public string InsuranceCompanyLicensingNumber { get; set; }
        public string InsuranceCompanySiteLocation { get; set; }
        public string InsuranceCompanyLocationOnMap { get; set; }
        public string InsuranceCompanyLongitute { get; set; }
        public string InsuranceCompanyLatitude { get; set; }
        public string TelephoneNumber { get; set; }
        public string Faxnumber { get; set; }
        public string OwnerName { get; set; }
        public string OwnerTelephone { get; set; }
        public string OwnerMobile { get; set; }
        public string OwnerEmailAddress { get; set; }
        public string GeneralManagerName { get; set; }
        public string GeneralManagerTelephone { get; set; }
        public string GeneralManagerMobile { get; set; }
        public string GeneralManagerEmailAddress { get; set; }
        public string InsuranceCompanyEmailAddress { get; set; }
        public string InsuranceCompanyWebsite { get; set; }
    }

    public class CompanyBusinessNatureAPIModel
    {
        public List<CompanyBusinessNatureModel> Data { get; set; }
        public string Message { get; set; }
        public long UserId { get; set; }
        public long RoleId { get; set; }
    }

    public class CompanyBusinessNatureModel
    {
        public long InsuranceTypeID { get; set; }
        public string InsuranceTypeCode { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
    }

    public class CriteriaListDetailPopupModel
    {
        public long CriteriaListDetailID { get; set; }
        public long CriteriaListID { get; set; }
        public long CriteriaListFieldToCompareID { get; set; }
        public string CriteriaListFieldToCompareEn { get; set; }
        public string CriteriaListFieldToCompareAr { get; set; }
        public string CriteriaFieldType { get; set; }
        public string CriteriaListComparisonOperator { get; set; }
        public string CriteriaListComparisonValueFrom { get; set; }
        public string CriteriaListComparisonValueTo { get; set; }
        public string ExternalSystemConnection { get; set; }
        public string BusinessSystemTableNameEn { get; set; }


    }

    public class SearchModel
    {
        public string InsuranceCompanyName { get; set; }
        public int InsuranceCompanyBuisnessNatureID { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        public DateTime InsuranceCompanyFirstLicensingDate { get; set; }
        public DateTime InsuranceCompanyLastLicensingDate { get; set; }
        public DateTime InsuranceCompanyLastInspectionDate { get; set; }
        public string InsuranceCompanyLicensingNumber { get; set; }

        public long CriteriaListID { get; set; }
        public List<long> CriteriaDetailSelectionIds { get; set; }
        public string strCriteriaDetailSelectionIds { get; set; }
        public List<CriteriaListDetailPopupModel> ActiveCriteriaSelection { get; set; }

        public DateTime InsuranceCompanyNominationFromDate { get; set; }
        public DateTime InsuranceCompanyNominationToDate { get; set; }
        public long InsuranceCompanyNominationDepartmentId { get; set; }
        public long InspectionPlanId { get; set; }
        public long InspectionPlanVersionNo { get; set; }
    }

    public class NominationListandGridDataModel
    {
        public List<NominationRequestModel> Data { get; set; }
        public GridActivitiesModel GridActivities { get; set; }
    }

    public class NominationReasonListandGridDataModel
    {
        public List<NominationRequestDetailModel> Data { get; set; }
        public GridActivitiesModel GridActivities { get; set; }
    }

    public class NominationSearchPagerModel
    {
        public SearchCriteriaNomination Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }

    }

    public class SearchCriteriaNomination
    {
        public DateTime NominationDateTo { get; set; }
        public DateTime NominationDateFrom { get; set; }
        public long NominationRequestID { get; set; }
        public long OrgUnitID { get; set; }
        public long IsFullRole { get; set; }

    }

    public class InsuranceCompaniesSearchPagerModel
    {
        public SearchModel Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }

    }

    public class InsuranceCompaniesListModel
    {
        public List<InsuranceCompaniesModel> Data { get; set; }
        public GridActivitiesModel GridActivities { get; set; }

    }


    public class CriteriaDetailforExternalModel
    {
        public long CriteriaListFieldToCompareID { get; set; }
        public string CriteriaListComparisonOperator { get; set; }
        public string CriteriaListComparisonValueFrom { get; set; }
        public string CriteriaListComparisonValueTo { get; set; }
        public string BusinessSystemTableNameEn { get; set; }
        
    }

    public class InsuranceCompaniesFetchModel
    {
        //public string InsuranceCompanyName { get; set; }
        //public int InsuranceCompanyBuisnessNatureID { get; set; }
        //public string InsuranceCompanyAddress { get; set; }
        //public DateTime InsuranceCompanyFirstLicensingDate { get; set; }
        //public DateTime InsuranceCompanyLastLicensingDate { get; set; }
        //public string InsuranceCompanyLicensingNumber { get; set; }
        //public int PageStart { get; set; }
        //public int PageSize { get; set; }

        ////public int TotalCount { get; set; }
        //public int FilteredCount { get; set; }
        //public string SortDirection { get; set; }
        //public string SortColumn { get; set; }
        //public string CompanyIds { get; set; }

        public string InsuranceCompanyName { get; set; }
        public string InsuranceCompanyBuisnessNatureID { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        public DateTime? InsuranceCompanyFirstLicensingDate { get; set; }
        public DateTime? InsuranceCompanyLastLicensingDate { get; set; }
        public string InsuranceCompanyLicensingNumber { get; set; }
        public int? PageStart { get; set; }
        public int? PageSize { get; set; }
        public int? PageCount { get; set; }
        public int? FilteredCount { get; set; }
        public string SortDirection { get; set; }
        public string SortColumn { get; set; }
        public string CompanyIds { get; set; }
    }

    public class InsurenceCompaniesParamVM
    {
        public string InsuranceCompanyName { get; set; }
        public int InsuranceCompanyID { get; set; }
        public string InsuranceCompanyBuisnessNatureID { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        public DateTime? InsuranceCompanyFirstLicensingDate { get; set; }
        public DateTime? InsuranceCompanyLastLicensingDate { get; set; }
        public string InsuranceCompanyLicensingNumber { get; set; }
        public int? PageStart { get; set; }
        public int? PageSize { get; set; }
        public int? PageCount { get; set; }
        public int? FilteredCount { get; set; }
        public string SortDirection { get; set; }
        public string SortColumn { get; set; }
        public string CompanyIds { get; set; }
    }
    public class InsurenceCompanies
    {
        public int? FilteredCount { get; set; }
        public int? TotalCount { get; set; }
        public Int64? InsuranceCompanyID { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string InsuranceCompanyBuisnessNature { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        //public DateTime? InsuranceCompanyFirstLicensingDate { get; set; }
        //public DateTime? InsuranceCompanyLastLicensingDate { get; set; }
        //public string InsuranceCompanyLicensingNumber { get; set; }
        //public string InsuranceCompanyStatus { get; set; }

        public string CompanyAddress { get; set; }

        public DateTime? FirstLisensingDate { get; set; }

        public DateTime? LastLisensingDate { get; set; }

        public string RegistrationNumber { get; set; }

        public string CompanySiteLocation { get; set; }

        public string CompanyLatitude { get; set; }

        public string CompanyLongitude { get; set; }

        public string CompanyLocationSite { get; set; }

        public string TelephoneNumber { get; set; }

        public string FaxNumber { get; set; }

        public string OwnerName { get; set; }

        public string OwnerPhoneNumber { get; set; }

        public string OwnerMobileNumber { get; set; }

        public string OwnerEmailAddress { get; set; }

        public string GeneralManagerName { get; set; }

        public string GeneralManagerPhone { get; set; }
        
        public string GeneralManagerMobile { get; set; }

        public string GeneralManagerFax { get; set; }

        public string CompanyEmail { get; set; }

        public string CompanyWebsite { get; set; }

        public string CompanyStatusCode { get; set; }
    }
}
