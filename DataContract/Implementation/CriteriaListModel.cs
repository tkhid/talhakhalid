﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataContract.Implementation
{
    public class CriteriaSearchPagerModel
    {
        public SearchCriteriaModel Search { get; set; }
        public GridActivitiesModel GridActivities { get; set; }

    }

    public class CriteriaDeleteModel
    {
        public long UserID { get; set; }
        public string DeleteID { get; set; }
        public long BuisnessSystemID { get; set; }
        public string ActionMachineName { get; set; }

        public long ActionID { get; set; }

    }

    public class CriteriaListandGridDataModel
    {
        public List<CriteriaListModel> Data { get; set; }
        public GridActivitiesModel GridActivities { get; set; }
    }

    public class CriteriaListDetailModelVM
    {
        public CriteriaListModel Data { get; set; }
        public List<CriteriaCategoryModel> listCategory { get; set; }
        public List<BusinessSystemModel> listBusinessSystem { get; set; }
        public List<CriteriaCompareOperatorModel> listCriteriaCompareOperator { get; set; }

    }

    public class CriteriaListDetailModel
    {
        public long CriteriaListDetailID { get; set; }
        public long CriteriaListID { get; set; }
        public long CriteriaListFieldToCompareID { get; set; }
        public string CriteriaListComparisonOperator { get; set; }
        public string CriteriaListComparisonValueFrom { get; set; }
        public string CriteriaListComparisonValueTo { get; set; }
        public long CriteriaCategoryID { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string BusinessSystemFieldNameEn { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public string BusinessSystemFieldNameAr { get; set; }


    }

    public class CriteriaRejectionModel
    {
        public long UserID { get; set; }
        public long CriteriaListID { get; set; }
        public long CriteriaListStatusID { get; set; }
        public string CriteriaListDisplayID { get; set; }
        public string RejectionComment { get; set; }
        public long ActionID { get; set; }
    }

    public class CriteriaListDetailModelMobile
    {
        public List<CriteriaListDetailModel> listCriteriaModel { get; set; }
        public List<CriteriaCategoryModel> listCategory { get; set; }
    }

    public class CriteriaListDetailModelPopupMobile
    {
        public List<BusinessSystemModel> listBusinessSystem { get; set; }
        public List<CriteriaCompareOperatorModel> listCriteriaCompareOperator { get; set; }
    }


    public class CriteriaListDetailPrintModel
    {
        public long CriteriaListDetailID { get; set; }
        public long CriteriaListID { get; set; }
        public long CriteriaListFieldToCompareID { get; set; }
        public string CriteriaListComparisonOperator { get; set; }
        public string CriteriaListComparisonValueFrom { get; set; }
        public string CriteriaListComparisonValueTo { get; set; }
        public long CriteriaCategoryID { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string BusinessSystemFieldNameEn { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public string BusinessSystemFieldNameAr { get; set; }
        public string CriteriaCategoryNameEn { get; set; }
        public string CriteriaCategoryNameAr { get; set; }
        public string CreatedBy { get; set; }
        public string CreationDate { get; set; }
        public string CriteriaListName { get; set; }
        public string CriteriaListDescription { get; set; }

    }


    public class CriteriaListModel
    {
        public long CriteriaListID { get; set; }
        public string CriteriaListDisplayID { get; set; }
        public string CriteriaListName { get; set; }
        public string CriteriaListDescription { get; set; }
        public long CriteriaListStatusID { get; set; }
        public bool? CriteriaListActiveState { get; set; }
        public string CriteriaListStatusEn { get; set; }
        public string CriteriaListStatusAr { get; set; }
        public long UserID { get; set; }
        public DateTime CriteriaListDate { get; set; }
        public string CriteriaListDateString { get; set; }
        public long ActionID { get; set; }
        public List<CriteriaListDetailModel> CriteriaListDetails { get; set; }

    }

    public class CriteriaListStatusModel
    {

        public long CriteriaListStatusID { get; set; }
        public string CriteriaListStatusAr { get; set; }
        public string CriteriaListStatusEn { get; set; }


    }

    public class CriteriaCategoryModel
    {
        public long CriteriaCategoryID { get; set; }
        public string CriteriaCategoryNameAr { get; set; }
        public string CriteriaCategoryNameEn { get; set; }
    }


    public class CriteriaFieldModel
    {
        public long CriteriaFieldID { get; set; }
        public long? CriteriaTableID { get; set; }
        public string CriteriaFieldNameAr { get; set; }
        public string CriteriaFieldNameEn { get; set; }
        public string CriteriaFieldType { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public string BusinessSystemTableNameEn { get; set; }
        public string ExternalSystemConnection { get; set; }
    }

    public class CriteriaTableModel
    {

        public long CriteriaTableID { get; set; }
        public long? CriteriaSystemID { get; set; }
        public string CriteriaTableName { get; set; }

    }

    public class BusinessSystemModel
    {
        public long BusinessSystemID { get; set; }
        public string UserID { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
        public long? BusinessSystemOwnerOUID { get; set; }
        public string BusinessSystemDescAr { get; set; }
        public string BusinessSystemDescEn { get; set; }
        public string OwnerOrgUnit { get; set; }
    }

    public class OrgUnitModel
    {
      
        public long OUID { get; set; }
        public string UserID { get; set; }
        public string OUNameAr { get; set; }
        public string OUDescAr { get; set; }
        public string OUNameEn { get; set; }
        public string OUDescEn { get; set; }
        public Nullable<long> OUHead { get; set; }
        public long ParentID { get; set; }
    }

    public class CriteriaCompareOperatorModel
    {
        public long CriteriaListComparisonOperatorID { get; set; }
        public string CriteriaListComparisonOperator { get; set; }
    }

    public class SearchCriteriaModel
    {
        public string CriteriaListName { get; set; }
        public string CriteriaListDescription { get; set; }
        public int CriteriaListStatusID { get; set; }
        public DateTime CriteriaDateTo { get; set; }
        public DateTime CriteriaDateFrom { get; set; }

    }

    public class GridActivitiesModel
    {
        public int PageStart { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int FilteredCount { get; set; }
        public string SortDirection { get; set; }
        public string SortColumn { get; set; }

    }

    public class CompanyListViewModel
    {
        public List<InsurenceCompanies> Data { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class ResponseOfListOfInsurenceCompanies
    {
        public Boolean IsSuccess { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public List<InsurenceCompanies> Data { get; set; }

    }


}
