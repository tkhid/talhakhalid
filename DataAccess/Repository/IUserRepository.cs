﻿using Common;
using DataAccess.CommonRespository;
using DataAccess.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IUserRepository : ISharedComponentGenericRepository<User>
    {
        /// <summary>
        /// GetUserInfo
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        User GetUserInfo(string UserName, string Password);
        /// <summary>
        /// GetUserInfoByUserName
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        User GetUserInfoByUserName(string UserName);
        /// <summary>
        /// GetScreensList
        /// </summary>
        /// <param name="BusinessSystemID"></param>
        /// <returns></returns>
        List<Screen> GetScreensList(long BusinessSystemID);
        /// <summary>
        /// GetUserInfoByEmail
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        User GetUserInfoByEmail(string Email);
        /// <summary>
        /// GetUsersListByBusinessSystemID
        /// </summary>
        /// <param name="BusinessSystemID"></param>
        /// <param name="IsForAdmin"></param>
        /// <returns></returns>
        List<User> GetUsersListByBusinessSystemID(long BusinessSystemID, bool IsForAdmin);
        /// <summary>
        /// GetUsersByRoleID
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        List<UserRole> GetUsersByRoleID(long RoleID);
        List<User> GetUsersByCompanyID(long RoleID);
        /// <summary>
        /// VerifyDuplicateUser
        /// </summary>
        /// <param name="UserNameEn"></param>
        /// <param name="Useremail"></param>
        /// <param name="Usermobile"></param>
        /// <param name="EmiratesId"></param>
        /// <returns></returns>
        User VerifyDuplicateUser(string UserNameEn, string Useremail, string Usermobile, string EmiratesId);
        /// <summary>
        /// VerifyPublicDuplicateUser
        /// </summary>
        /// <param name="UserNameEn"></param>
        /// <param name="Useremail"></param>
        /// <param name="Usermobile"></param>
        /// <param name="EmiratesId"></param>
        /// <returns></returns>
        UserSubscriptionRequest VerifyPublicDuplicateUser(string UserNameEn, string Useremail, string Usermobile, string EmiratesId);
        /// <summary>
        /// VerifyDuplicateUserToUpdate
        /// </summary>
        /// <param name="UserNameEn"></param>
        /// <param name="Useremail"></param>
        /// <param name="Usermobile"></param>
        /// <param name="EmiratesId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        User VerifyDuplicateUserToUpdate(string UserNameEn, string Useremail, string Usermobile, string EmiratesId, long UserId);
        /// <summary>
        /// GetScreensListByUserID
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        List<ScreenPermissionModel> GetScreensListByUserID(long UserId);
        /// <summary>
        /// UpdateUserTask
        /// </summary>
        /// <param name="objUserTask"></param>
        /// <returns></returns>
        bool UpdateUserTask(UserTask objUserTask);
        /// <summary>
        /// RegisterPublicUser
        /// </summary>
        /// <param name="objUserSubscriptionRequest"></param>
        /// <returns></returns>
        bool RegisterPublicUser(UserSubscriptionRequest objUserSubscriptionRequest);
        /// <summary>
        /// UpdateLinkStatus
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        bool UpdateLinkStatus(long UserID, bool Status);
        /// <summary>
        /// GetUsersType
        /// </summary>
        /// <returns></returns>
        List<UserType> GetUsersType();
    }
}
