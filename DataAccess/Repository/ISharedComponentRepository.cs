﻿using Common;
using DataAccess.CommonRespository;
using DataAccess.Database;
using IA.EC.BusinessEntities;
using System.Collections.Generic;

namespace DataAccess.Repository
{
    public interface ISharedComponentRepository : ISharedComponentGenericRepository<Field>
    {

        #region Business System
        /// <summary>
        /// GetBusinessSystem
        /// </summary>
        /// <returns></returns>
        List<BusinessSystem> GetBusinessSystem();
        /// <summary>
        /// GetActionTypes
        /// </summary>
        /// <returns></returns>
        List<ActionType> GetActionTypes();
        /// <summary>
        /// GetBusinessSystemByID
        /// </summary>
        /// <param name="BusinessID"></param>
        /// <returns></returns>
        List<BusinessSystem> GetBusinessSystemByID(long BusinessID);
        /// <summary>
        /// GetBusinessSystemFields
        /// </summary>
        /// <param name="SystemID"></param>
        /// <returns></returns>
        List<Field> GetBusinessSystemFields(long SystemID);
        /// <summary>
        /// VerifyBusinessSystemDuplicateTOAdd
        /// </summary>
        /// <param name="BusinessSystemNameAr"></param>
        /// <param name="BusinessSystemNameEn"></param>
        /// <returns></returns>
        BusinessSystem VerifyBusinessSystemDuplicateTOAdd(string BusinessSystemNameAr, string BusinessSystemNameEn);
        /// <summary>
        /// VerifyBusinessSystemDuplicateTOUpdate
        /// </summary>
        /// <param name="BusinessSystemNameAr"></param>
        /// <param name="BusinessSystemNameEn"></param>
        /// <param name="BusinessSystemId"></param>
        /// <returns></returns>
        BusinessSystem VerifyBusinessSystemDuplicateTOUpdate(string BusinessSystemNameAr, string BusinessSystemNameEn, long BusinessSystemId);
        /// <summary>
        /// AddUpdateBusinessSystem
        /// </summary>
        /// <param name="objBusinessSystem"></param>
        /// <returns></returns>
        int AddUpdateBusinessSystem(BusinessSystem objBusinessSystem);
        /// <summary>
        /// GetOrgUnitList
        /// </summary>
        /// <returns></returns>
        List<OrgUnit> GetOrgUnitList();
        /// <summary>
        /// RemoveBusinessSystem
        /// </summary>
        /// <param name="BusinessSystemID"></param>
        /// <returns></returns>
        bool RemoveBusinessSystem(long BusinessSystemID);
        /// <summary>
        /// GetBusinessSystem
        /// </summary>
        /// <param name="GA"></param>
        /// <param name="SC"></param>
        /// <param name="totalcount"></param>
        /// <returns></returns>
        List<BusinessSystemsCustomModel> GetBusinessSystem(GridActivities GA, BusinessSystemsSearchModel SC, out int totalcount);

        #endregion

        #region Org Unit
        /// <summary>
        /// RemoveOrgUnit
        /// </summary>
        /// <param name="OrgUnitID"></param>
        /// <returns></returns>
        bool RemoveOrgUnit(long OrgUnitID);
        /// <summary>
        /// VerifyOrgUnitDuplicateTOAdd
        /// </summary>
        /// <param name="OUNameEn"></param>
        /// <param name="OUNameAr"></param>
        /// <returns></returns>
        OrgUnit VerifyOrgUnitDuplicateTOAdd(string OUNameEn, string OUNameAr);
        /// <summary>
        /// VerifyOrgUnitDuplicateTOUpdate
        /// </summary>
        /// <param name="OUNameEn"></param>
        /// <param name="OUNameAr"></param>
        /// <param name="OrgUnitID"></param>
        /// <returns></returns>
        OrgUnit VerifyOrgUnitDuplicateTOUpdate(string OUNameEn, string OUNameAr, long OrgUnitID);
        /// <summary>
        /// GetOrgUnitList
        /// </summary>
        /// <param name="ga"></param>
        /// <returns></returns>
        List<OrgUnitCustomModel> GetOrgUnitList(GridActivities GA, OrgUnitSearchModel SC, out int totalcount);
        /// <summary>
        /// AddUpdateOrgUnit
        /// </summary>
        /// <param name="objOrgUnit"></param>
        /// <returns></returns>
        int AddUpdateOrgUnit(OrgUnit objOrgUnit);

        /// <summary>
        /// GetOrgUnitByID
        /// </summary>
        /// <param name="OUID"></param>
        /// <returns></returns>
        List<OrgUnit> GetOrgUnitByID(long OUID);
        /// <summary>
        /// GetActionGroupByOUID
        /// </summary>
        /// <param name="OUID"></param>
        /// <returns></returns>
        List<ActionGroup> GetActionGroupByOUID(long OUID);
        /// <summary>
        /// GetActionGroupByID
        /// </summary>
        /// <param name="GroupID"></param>
        /// <returns></returns>
        List<ActionGroup> GetActionGroupByID(long GroupID);
        #endregion

        #region Screens
        /// <summary>
        /// GetScreenByID
        /// </summary>
        /// <param name="ScreenID"></param>
        /// <returns></returns>
        List<Screen> GetScreenByID(long ScreenID);
        /// <summary>
        /// GetScreenActionsByID
        /// </summary>
        /// <param name="ActionsID"></param>
        /// <returns></returns>
        List<ScreenAction> GetScreenActionsByID(long ActionsID);
        /// <summary>
        /// VerifyBusinessScreensDuplicateTOUpdate
        /// </summary>
        /// <param name="ScreenNameAr"></param>
        /// <param name="ScreenNameEn"></param>
        /// <param name="ScreenID"></param>
        /// <param name="BusinessSystemID"></param>
        /// <returns></returns>
        Screen VerifyBusinessScreensDuplicateTOUpdate(string ScreenNameAr, string ScreenNameEn, long ScreenID, long BusinessSystemID);
        /// <summary>
        /// VerifyBusinessScreensActionDuplicateTOUpdate
        /// </summary>
        /// <param name="ActionName"></param>
        /// <param name="ActionID"></param>
        /// <param name="ScreenID"></param>
        /// <returns></returns>
        ScreenAction VerifyBusinessScreensActionDuplicateTOUpdate(string ActionName, long ActionID, long ScreenID);
        /// <summary>
        /// VerifyBusinessScreensActionDuplicateTOAdd
        /// </summary>
        /// <param name="ActionName"></param>
        /// <param name="ScreenID"></param>
        /// <returns></returns>
        ScreenAction VerifyBusinessScreensActionDuplicateTOAdd(string ActionName, long ScreenID);
        /// <summary>
        /// VerifyBusinessScreensDuplicateTOAdd
        /// </summary>
        /// <param name="ScreenNameAr"></param>
        /// <param name="ScreenNameEn"></param>
        /// <param name="BusinessSystemID"></param>
        /// <returns></returns>
        Screen VerifyBusinessScreensDuplicateTOAdd(string ScreenNameAr, string ScreenNameEn, long BusinessSystemID);
        /// <summary>
        /// AddUpdateScreen
        /// </summary>
        /// <param name="objScreen"></param>
        /// <returns></returns>
        bool AddUpdateScreen(Screen objScreen);
        /// <summary>
        /// AddUpdateAction
        /// </summary>
        /// <param name="objAction"></param>
        /// <returns></returns>
        bool AddUpdateAction(ScreenAction objAction);
        /// <summary>
        /// RemoveScreen
        /// </summary>
        /// <param name="ScreenID"></param>
        /// <returns></returns>
        bool RemoveScreen(long ScreenID);
        /// <summary>
        /// RemoveAction
        /// </summary>
        /// <param name="ActionID"></param>
        /// <returns></returns>
        bool RemoveAction(long ActionID);
        /// <summary>
        /// GetScreensList
        /// </summary>
        /// <param name="BusinessSystemID"></param>
        /// <returns></returns>
        List<Screen> GetScreensList(long BusinessSystemID);
        /// <summary>
        /// GetScreensActionsList
        /// </summary>
        /// <param name="ScreenID"></param>
        /// <returns></returns>
        List<ScreenAction> GetScreensActionsList(long ScreenID);
        /// <summary>
        /// RemoveActionGroup
        /// </summary>
        /// <param name="ActionGroupID"></param>
        /// <returns></returns>
        bool RemoveActionGroup(long ActionGroupID);
        /// <summary>
        /// AddUpdateActionGroup
        /// </summary>
        /// <param name="objActionGroup"></param>
        /// <returns></returns>
        int AddUpdateActionGroup(ActionGroup objActionGroup);
        /// <summary>
        /// VerifyActionGroupDuplicateTOAdd
        /// </summary>
        /// <param name="ActionGroupNameAr"></param>
        /// <param name="ActionGroupNameEn"></param>
        /// <returns></returns>
        ActionGroup VerifyActionGroupDuplicateTOAdd(string ActionGroupNameAr, string ActionGroupNameEn);
        /// <summary>
        /// VerifyActionGroupDuplicateTOUpdate
        /// </summary>
        /// <param name="ActionGroupNameAr"></param>
        /// <param name="ActionGroupNameEn"></param>
        /// <param name="ActionGroupID"></param>
        /// <param name="OUID"></param>
        /// <returns></returns>
        ActionGroup VerifyActionGroupDuplicateTOUpdate(string ActionGroupNameAr, string ActionGroupNameEn, long ActionGroupID, long OUID);
        #endregion

        #region Common
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        SharedAuditTrail GetOTPByUserID(long UserID);
        /// <summary>
        /// SaveAuditTrail
        /// </summary>
        /// <param name="objAuditTrail"></param>
        /// <returns></returns>
        bool SaveAuditTrail(SharedAuditTrail objAuditTrail);
        /// <summary>
        /// SaveAuditTrailEComplaint
        /// </summary>
        /// <param name="objAuditTrail"></param>
        /// <returns></returns>
        bool SaveAuditTrailEComplaint(EAuditTrail objAuditTrail);
        /// <summary>
        /// GetDepartments
        /// </summary>
        /// <returns></returns>
        List<OrgUnit> GetDepartments();
        /// <summary>
        /// GetCountryList
        /// </summary>
        /// <returns></returns>
        List<Country> GetCountryList();
        /// <summary>
        /// GetDesignations
        /// </summary>
        /// <returns></returns>
        List<Designation> GetDesignations();
        /// <summary>
        /// GetCitiesList
        /// </summary>
        /// <param name="CountryID"></param>
        /// <returns></returns>
        List<City> GetCitiesList(long CountryID);
        /// <summary>
        /// AddEmailNotification
        /// </summary>
        /// <param name="objEmailNotification"></param>
        /// <returns></returns>
        bool AddEmailNotification(EmailNotification objEmailNotification);
        /// <summary>
        /// GetActionCommunicationMethodByActionID
        /// </summary>
        /// <param name="ActionID"></param>
        /// <returns></returns>
        string GetActionCommunicationMethodByActionID(long ActionID);
        /// <summary>
        /// GetUserRolesByRoleID
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        List<UserRolesCustomModel> GetUserDetailByRoleID(string RoleID);
        /// <summary>
        /// GetUserDetailByOUID
        /// </summary>
        /// <param name="OUID"></param>
        /// <returns></returns>
        List<UserRolesCustomModel> GetUserDetailByOUID(string OUID);
        /// <summary>
        /// GetNotificationsListToEmail
        /// </summary>
        /// <returns></returns>
        List<EmailNotification> GetNotificationsListToEmail();
        /// <summary>
        /// UpdateEmailNotificationStatus
        /// </summary>
        /// <param name="NotificationID"></param>
        /// <param name="StatusID"></param>
        /// <returns></returns>
        bool UpdateEmailNotificationStatus(long NotificationID, long StatusID);

        #endregion

        #region Role Management
        /// <summary>
        /// GetRolesListByUserID
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        List<UserRole> GetRolesListByUserID(long UserID);
        /// <summary>
        /// GetActionsByRoleID
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        List<RolePermission> GetActionsByRoleID(long RoleID);
        /// <summary>
        /// VerifyDuplicateRoleToAdd
        /// </summary>
        /// <param name="RoleNameEn"></param>
        /// <param name="RoleNameAr"></param>
        /// <param name="BusinessSystemID"></param>
        /// <returns></returns>
        Role VerifyDuplicateRoleToAdd(string RoleNameEn, string RoleNameAr, long BusinessSystemID);
        /// <summary>
        /// VerifyDuplicateRoleToUpdate
        /// </summary>
        /// <param name="RoleNameEn"></param>
        /// <param name="RoleNameAr"></param>
        /// <param name="BusinessSystemID"></param>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        Role VerifyDuplicateRoleToUpdate(string RoleNameEn, string RoleNameAr, long BusinessSystemID, long RoleID);
        /// <summary>
        /// AddUpdateRole
        /// </summary>
        /// <param name="objRole"></param>
        /// <returns></returns>
        int AddUpdateRole(Role objRole);
        /// <summary>
        /// RemoveRole
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        bool RemoveRole(long RoleID);
        /// <summary>
        /// GetRoleByID
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        Role GetRoleByID(long RoleID);
        /// <summary>
        /// GetRolesListByBusinessSystemID
        /// </summary>
        /// <param name="BusinessSystemID"></param>
        /// <returns></returns>
        List<Role> GetRolesListByBusinessSystemID(long BusinessSystemID);
        /// <summary>
        /// GetRolesList
        /// </summary>
        /// <returns></returns>
        List<Role> GetRolesList();
        /// <summary>
        /// AddUserRoles
        /// </summary>
        /// <param name="objRoles"></param>
        /// <returns></returns>
        int AddUserRoles(List<UserRole> objRoles);
        /// <summary>
        /// AddRolePermissions
        /// </summary>
        /// <param name="objRolePermission"></param>
        /// <returns></returns>
        int AddRolePermissions(List<RolePermission> objRolePermission);
        #endregion
    }
}
