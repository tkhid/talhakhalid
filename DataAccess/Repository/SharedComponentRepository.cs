﻿using Common;
using DataAccess.CommonRespository;
using DataAccess.Database;
using IA.EC.BusinessEntities;
using IA.EC.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class SharedComponentRepository : SharedComponentGenericRepository<Field>, ISharedComponentRepository
    {
        public SharedComponentRepository(SharedComponentsEntities context)
                : base(context)
        {

        }

        #region Common
        public SharedAuditTrail GetOTPByUserID(long UserID)
        {
            SharedAuditTrail objAuditTrail = new SharedAuditTrail();
            var dbAuditTrail = _context.SharedAuditTrails.Where(x => x.ActionUserID.Value == UserID && x.ActionDescription.Contains("OTP -")).ToList().OrderByDescending(x => x.ActionTime);
            if (dbAuditTrail.Count() > 0)
            {
                objAuditTrail = dbAuditTrail.FirstOrDefault();
            }
            return objAuditTrail;
        }
        public bool SaveAuditTrail(SharedAuditTrail objAuditTrail)
        {
            bool IsOk = false;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    context.SharedAuditTrails.Add(objAuditTrail);
                    int Result = context.SaveChanges();
                    if (Result > 0)
                    {
                        IsOk = true;
                    }
                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public List<Designation> GetDesignations()
        {
            List<Designation> objDesignation = new List<Designation>();
            try
            {
                objDesignation = _context.Designations.ToList();
                return objDesignation;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<Country> GetCountryList()
        {
            List<Country> objCountry = new List<Country>();
            try
            {
                objCountry = _context.Countries.ToList();
                return objCountry;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<City> GetCitiesList(long CountryID)
        {
            List<City> objCity = new List<City>();
            try
            {
                objCity = _context.Cities.Where(x => x.CountryID == CountryID).ToList();
                return objCity;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool AddEmailNotification(EmailNotification objEmailNotification)
        {
            bool IsOk = false;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    context.EmailNotifications.Add(objEmailNotification);
                    int Result = context.SaveChanges();
                    if (Result > 0)
                    {
                        IsOk = true;
                    }
                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public List<OrgUnit> GetDepartments()
        {
            List<OrgUnit> objDepartment = new List<OrgUnit>();
            try
            {
                objDepartment = _context.OrgUnits.OrderBy(x => x.OUNameEn).Where(x => x.Status == true).ToList();
                return objDepartment;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public string GetActionCommunicationMethodByActionID(long ActionID)
        {
            return _context.ScreenActions.Where(x => x.ActionID == ActionID).FirstOrDefault().ActionCommunicationMethod;
        }
        public UserRole GetUserRolesByRoleID(long RoleID)
        {
            return _context.UserRoles.Find(RoleID);
        }
        public List<UserRolesCustomModel> GetUserDetailByRoleID(string RolesID)
        {
            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter { ParameterName = "RoleID", Value = RolesID };
                using (SharedComponentsEntities context = new SharedComponentsEntities())
                {
                    IEnumerable<UserRolesCustomModel> ScreensList = context.Database.SqlQuery<UserRolesCustomModel>("exec Proc_GetUserByRoles  @RoleID", parameters).ToList();

                    return ScreensList.ToList();


                }
            }
            catch (DbUpdateException e)
            {
                SqlException s = e.InnerException.InnerException as SqlException;

                return null;
            }
        }
        public List<UserRolesCustomModel> GetUserDetailByOUID(string OUID)
        {
            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter { ParameterName = "OUID", Value = OUID };
                using (SharedComponentsEntities context = new SharedComponentsEntities())
                {
                    IEnumerable<UserRolesCustomModel> ScreensList = context.Database.SqlQuery<UserRolesCustomModel>("exec Proc_GetUserByOrganizationalUnitID  @OUID", parameters).ToList();

                    return ScreensList.ToList();


                }
            }
            catch (DbUpdateException e)
            {
                SqlException s = e.InnerException.InnerException as SqlException;

                return null;
            }
        }
        public List<EmailNotification> GetNotificationsListToEmail()
        {
            List<EmailNotification> objEmailNotification = new List<EmailNotification>();
            try
            {
                objEmailNotification = _context.EmailNotifications.ToList();
                objEmailNotification = objEmailNotification.Where(x => x.EmailStatusID != Convert.ToInt64(eInspectionSystemWebApi.Common.EmailStatus.Sent)).ToList();
                return objEmailNotification;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public bool UpdateEmailNotificationStatus(long NotificationID, long StatusID)
        {

            bool IsOK = false;
            try
            {
                var result = _context.EmailNotifications.Find(NotificationID);
                result.EmailStatusID = StatusID;
                _context.SaveChanges();
                IsOK = true;
            }
            catch (Exception ex)
            {
                return false;

            }
            return IsOK;
        }
        #endregion

        #region Business Systems
        public BusinessSystem VerifyBusinessSystemDuplicateTOAdd(string BusinessSystemNameAr, string BusinessSystemNameEn)
        {
            return _context.BusinessSystems.Where(x => x.BusinessSystemNameEn == BusinessSystemNameEn || x.BusinessSystemNameAr == BusinessSystemNameAr).FirstOrDefault();
        }
        public BusinessSystem VerifyBusinessSystemDuplicateTOUpdate(string BusinessSystemNameAr, string BusinessSystemNameEn, long BusinessSystemID)
        {
            var objBusinessSystem = new BusinessSystem();
            var BusinessList = new List<BusinessSystem>();
            BusinessList = _context.BusinessSystems.Where(x => x.BusinessSystemID != BusinessSystemID).ToList();
            objBusinessSystem = BusinessList.Where(x => x.BusinessSystemNameEn == BusinessSystemNameEn || x.BusinessSystemNameAr == BusinessSystemNameAr).FirstOrDefault();
            if (objBusinessSystem != null)
            {
                return objBusinessSystem;
            }
            else
            {
                return new BusinessSystem();
            }

        }
        public int AddUpdateBusinessSystem(BusinessSystem objBusinessSystem)
        {
            int Result = 0;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    objBusinessSystem.BStatus = true;
                    context.BusinessSystems.Add(objBusinessSystem);
                    if (objBusinessSystem.BusinessSystemID == 0)
                    {
                        context.BusinessSystems.Add(objBusinessSystem);
                        _context.SaveChanges();

                        Result = context.SaveChanges();

                    }
                    else
                    {
                        _context.Entry(objBusinessSystem).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();

                    }

                }
                catch (Exception ex)
                {


                }
                return Result;

            }

        }
        public bool RemoveBusinessSystem(long BusinessSystemID)
        {
            bool IsOk = false;
            BusinessSystem objBusinessSystem = new BusinessSystem();

            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    objBusinessSystem.BusinessSystemID = BusinessSystemID;
                    objBusinessSystem.BStatus = false;
                    var obj = context.BusinessSystems.Find(BusinessSystemID);
                    objBusinessSystem.BusinessSystemDescAr = obj.BusinessSystemDescAr;
                    objBusinessSystem.BusinessSystemDescEn = obj.BusinessSystemDescEn;
                    objBusinessSystem.BusinessSystemNameEn = obj.BusinessSystemNameEn;
                    objBusinessSystem.BusinessSystemNameAr = obj.BusinessSystemNameAr;
                    objBusinessSystem.BusinessSystemOwnerOUID = obj.BusinessSystemOwnerOUID;
                    objBusinessSystem.ConnectionString = obj.ConnectionString;
                    objBusinessSystem.DatabaseName = obj.DatabaseName;

                    context.BusinessSystems.Add(objBusinessSystem);
                    _context.Entry(objBusinessSystem).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    IsOk = true;

                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public List<BusinessSystemsCustomModel> GetBusinessSystem(GridActivities ga, BusinessSystemsSearchModel sc, out int totalCount)
        {
            try
            {

                if (ga.PageStart == 0)
                    ga.PageStart = 1;
                else
                {

                    ga.PageStart = ga.PageStart;
                }

                var parameters = new SqlParameter[9];
                parameters[0] = new SqlParameter { ParameterName = "@PageSize", Value = ga.PageSize };
                parameters[1] = new SqlParameter { ParameterName = "@PageNumber", Value = ga.PageStart };

                if (string.IsNullOrEmpty(ga.SortColumn))
                    parameters[2] = new SqlParameter { ParameterName = "@SortColum", Value = "BusinessSystemID" };
                else
                    parameters[2] = new SqlParameter { ParameterName = "@SortColum", Value = ga.SortColumn };

                if (string.IsNullOrEmpty(ga.SortColumn))
                    parameters[3] = new SqlParameter { ParameterName = "@sortDir", Value = "asc" };
                else
                    parameters[3] = new SqlParameter { ParameterName = "@sortDir", Value = ga.SortDirection };

                if (string.IsNullOrEmpty(sc.BusinessSystemNameEn))
                    parameters[4] = new SqlParameter { ParameterName = "@BusinessSystemNameEn", Value = DBNull.Value };
                else
                    parameters[4] = new SqlParameter { ParameterName = "@BusinessSystemNameEn", Value = sc.BusinessSystemNameEn };


                if (string.IsNullOrEmpty(sc.BusinessSystemNameAr))
                    parameters[5] = new SqlParameter { ParameterName = "@BusinessSystemNameAr", Value = DBNull.Value };
                else
                    parameters[5] = new SqlParameter { ParameterName = "@BusinessSystemNameAr", Value = sc.BusinessSystemNameAr };

                if (string.IsNullOrEmpty(sc.BusinessSystemDescEn))
                    parameters[6] = new SqlParameter { ParameterName = "@BusinessSystemDescEn", Value = DBNull.Value };
                else
                    parameters[6] = new SqlParameter { ParameterName = "@BusinessSystemDescEn", Value = sc.BusinessSystemDescEn };


                if (string.IsNullOrEmpty(sc.BusinessSystemDescAr))
                    parameters[7] = new SqlParameter { ParameterName = "@BusinessSystemDescAr", Value = DBNull.Value };
                else
                    parameters[7] = new SqlParameter { ParameterName = "@BusinessSystemDescAr", Value = sc.BusinessSystemDescAr };

                parameters[8] = new SqlParameter { ParameterName = "@totalCount", Value = 0, Direction = ParameterDirection.Output };


                List<BusinessSystemsCustomModel> objOut = _context.Database.SqlQuery<BusinessSystemsCustomModel>("exec Proc_GetBusinessSystem @PageSize ,@PageNumber ,@SortColum ,@sortDir,@BusinessSystemNameEn,@BusinessSystemNameAr,@BusinessSystemDescEn,@BusinessSystemDescAr,@totalCount out", parameters).ToList();
                totalCount = Convert.ToInt32(parameters[8].Value);
                return objOut;

            }
            catch (Exception ex)
            {
                throw ex;
            }



        }
        public List<BusinessSystem> GetBusinessSystem()
        {
            List<BusinessSystem> objReturn = new List<BusinessSystem>();
            try
            {
                objReturn = _context.BusinessSystems.Where(x => x.BStatus == true).ToList();
                return objReturn;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<BusinessSystem> GetBusinessSystemByID(long BusinessID)
        {
            List<BusinessSystem> objReturn = new List<BusinessSystem>();
            try
            {
                objReturn = _context.BusinessSystems.Where(x => x.BusinessSystemID == BusinessID).ToList();
                return objReturn;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<Field> GetBusinessSystemFields(long SystemID)
        {
            var parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter { ParameterName = "@CriteriaSystemID", Value = SystemID };
            List<Field> objReturn = new List<Field>();
            try
            {
                if (SystemID > 0)
                    objReturn = _context.Database.SqlQuery<Field>("exec Proc_GetFieldList @CriteriaSystemID", parameters).ToList();
                else
                    objReturn = _context.Fields.ToList();
                return objReturn;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public List<ActionType> GetActionTypes()
        {
            return _context.ActionTypes.ToList();
        }

        #endregion

        #region Organizational Units

        public List<OrgUnitCustomModel> GetOrgUnitList(GridActivities ga, OrgUnitSearchModel sc, out int totalCount)
        {
            try
            {

                if (ga.PageStart == 0)
                    ga.PageStart = 1;
                else
                {

                    ga.PageStart = ga.PageStart;
                }

                var parameters = new SqlParameter[9];
                parameters[0] = new SqlParameter { ParameterName = "@PageSize", Value = ga.PageSize };
                parameters[1] = new SqlParameter { ParameterName = "@PageNumber", Value = ga.PageStart };

                if (string.IsNullOrEmpty(ga.SortColumn))
                    parameters[2] = new SqlParameter { ParameterName = "@SortColum", Value = "OUID" };
                else
                    parameters[2] = new SqlParameter { ParameterName = "@SortColum", Value = ga.SortColumn };

                if (string.IsNullOrEmpty(ga.SortColumn))
                    parameters[3] = new SqlParameter { ParameterName = "@sortDir", Value = "asc" };
                else
                    parameters[3] = new SqlParameter { ParameterName = "@sortDir", Value = ga.SortDirection };

                if (string.IsNullOrEmpty(sc.OUNameEn))
                    parameters[4] = new SqlParameter { ParameterName = "@OUNameEn", Value = DBNull.Value };
                else
                    parameters[4] = new SqlParameter { ParameterName = "@OUNameEn", Value = sc.OUNameEn };


                if (string.IsNullOrEmpty(sc.OUNameAr))
                    parameters[5] = new SqlParameter { ParameterName = "@OUNameAr", Value = DBNull.Value };
                else
                    parameters[5] = new SqlParameter { ParameterName = "@OUNameAr", Value = sc.OUNameAr };

                if (string.IsNullOrEmpty(sc.OUDescEn))
                    parameters[6] = new SqlParameter { ParameterName = "@OUDescEn", Value = DBNull.Value };
                else
                    parameters[6] = new SqlParameter { ParameterName = "@OUDescEn", Value = sc.OUDescEn };


                if (string.IsNullOrEmpty(sc.OUDescAr))
                    parameters[7] = new SqlParameter { ParameterName = "@OUDescAr", Value = DBNull.Value };
                else
                    parameters[7] = new SqlParameter { ParameterName = "@OUDescAr", Value = sc.OUDescAr };

                parameters[8] = new SqlParameter { ParameterName = "@totalCount", Value = 0, Direction = ParameterDirection.Output };


                List<OrgUnitCustomModel> ObjCriteriaList = _context.Database.SqlQuery<OrgUnitCustomModel>("exec Proc_GetOrgUnits @PageSize ,@PageNumber ,@SortColum ,@sortDir,@OUNameEn,@OUNameAr,@OUDescEn,@OUDescAr,@totalCount out", parameters).ToList();
                totalCount = Convert.ToInt32(parameters[8].Value);
                return ObjCriteriaList;

            }
            catch (Exception ex)
            {
                throw ex;
            }



        }
        public List<OrgUnit> GetOrgUnitList()
        {
            List<OrgUnit> objOrgUnit = new List<OrgUnit>();
            try
            {
                objOrgUnit = _context.OrgUnits.Where(x => x.Status == true).ToList();
                return objOrgUnit;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public OrgUnit VerifyOrgUnitDuplicateTOAdd(string OUNameAr, string OUNameEn)
        {
            return _context.OrgUnits.Where(x => x.OUNameAr == OUNameAr || x.OUNameEn == OUNameEn).FirstOrDefault();
        }
        public OrgUnit VerifyOrgUnitDuplicateTOUpdate(string OUNameAr, string OUNameEn, long OUID)
        {
            var objOrgUnit = new OrgUnit();
            var resultlist = new List<OrgUnit>();
            resultlist = _context.OrgUnits.Where(x => x.OUID != OUID).ToList();
            objOrgUnit = resultlist.Where(x => x.OUNameAr == OUNameAr || x.OUNameEn == OUNameEn).FirstOrDefault();
            if (objOrgUnit != null)
            {
                return objOrgUnit;
            }
            else
            {
                return new OrgUnit();
            }

        }
        public int AddUpdateOrgUnit(OrgUnit objOrgUnit)
        {
            int Result = 0;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {


                    objOrgUnit.Status = true;
                    context.OrgUnits.Add(objOrgUnit);
                    if (objOrgUnit.OUID == 0)
                    {
                        context.OrgUnits.Add(objOrgUnit);
                        _context.SaveChanges();
                        Result = context.SaveChanges();

                    }
                    else
                    {
                        var result = _context.OrgUnits.Find(objOrgUnit.OUID);

                        result.OUDescAr = objOrgUnit.OUDescAr;
                        result.OUDescEn = objOrgUnit.OUDescEn;
                        result.OUNameEn = objOrgUnit.OUNameEn;
                        result.OUNameAr = objOrgUnit.OUNameAr;
                        result.ParentID = objOrgUnit.ParentID;
                        result.OUID = objOrgUnit.OUID;
                        result.OUHead = objOrgUnit.OUHead;
                        //_context.Entry(result).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {

                }
                return Result;

            }

        }
        public bool RemoveOrgUnit(long OrgUnitID)
        {
            bool IsOk = false;
            OrgUnit objOrgUnit = new OrgUnit();

            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    objOrgUnit.Status = false;
                    var obj = context.OrgUnits.Find(OrgUnitID);
                    objOrgUnit.OUDescAr = obj.OUDescAr;
                    objOrgUnit.OUDescEn = obj.OUDescEn;
                    objOrgUnit.OUID = obj.OUID;
                    objOrgUnit.OUNameAr = obj.OUNameAr;
                    objOrgUnit.OUNameEn = obj.OUNameEn;
                    objOrgUnit.OUHead = obj.OUHead;
                    context.OrgUnits.Add(objOrgUnit);
                    _context.Entry(objOrgUnit).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    IsOk = true;

                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public List<OrgUnit> GetOrgUnitByID(long OUID)
        {
            List<OrgUnit> objReturn = new List<OrgUnit>();
            try
            {
                objReturn = _context.OrgUnits.Where(x => x.OUID == OUID).ToList();
                return objReturn;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<ActionGroup> GetActionGroupByOUID(long OUID)
        {
            List<ActionGroup> objReturn = new List<ActionGroup>();
            try
            {
                objReturn = _context.ActionGroups.Where(x => x.OUID == OUID && x.Status == true).ToList();
                return objReturn;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public int AddUpdateActionGroup(ActionGroup objActionGroup)
        {
            int Result = 0;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    objActionGroup.Status = true;
                    context.ActionGroups.Add(objActionGroup);
                    if (objActionGroup.ActionGroupID == 0)
                    {
                        context.ActionGroups.Add(objActionGroup);
                        _context.SaveChanges();
                        Result = context.SaveChanges();

                    }
                    else
                    {
                        var result = _context.ActionGroups.Find(objActionGroup.ActionGroupID);

                        result.ActionGroupNameEn = objActionGroup.ActionGroupNameEn;
                        result.ActionGroupNameAr = objActionGroup.ActionGroupNameAr;
                        result.ActionGroupDescEn = objActionGroup.ActionGroupDescEn;
                        result.ActionGroupDescAr = objActionGroup.ActionGroupDescAr;
                        result.ParentID = objActionGroup.ParentID;
                        //_context.Entry(result).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {

                }
                return Result;

            }

        }
        public bool RemoveActionGroup(long ActionGroupID)
        {
            bool IsOk = false;
            ActionGroup objActionGroup = new ActionGroup();

            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    objActionGroup.Status = false;
                    var obj = context.ActionGroups.Find(ActionGroupID);
                    objActionGroup.ActionGroupDescAr = obj.ActionGroupDescAr;
                    objActionGroup.ActionGroupDescEn = obj.ActionGroupDescEn;
                    objActionGroup.OUID = obj.OUID;
                    objActionGroup.ActionGroupID = obj.ActionGroupID;
                    objActionGroup.ActionGroupNameAr = obj.ActionGroupNameAr;
                    objActionGroup.ActionGroupNameEn = obj.ActionGroupNameEn;
                    context.ActionGroups.Add(objActionGroup);
                    _context.Entry(objActionGroup).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    IsOk = true;

                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public ActionGroup VerifyActionGroupDuplicateTOAdd(string ActionGroupNameAr, string ActionGroupNameEn)
        {
            return _context.ActionGroups.Where(x => x.ActionGroupNameAr == ActionGroupNameEn || x.ActionGroupNameAr == ActionGroupNameAr).FirstOrDefault();
        }
        public ActionGroup VerifyActionGroupDuplicateTOUpdate(string ActionGroupNameAr, string ActionGroupNameEn, long ActionGroupID, long OUID)
        {
            var objOrgUnit = new ActionGroup();
            var resultlist = new List<ActionGroup>();
            resultlist = _context.ActionGroups.Where(x => x.OUID == OUID).ToList();
            resultlist = _context.ActionGroups.Where(x => x.ActionGroupID != ActionGroupID).ToList();
            objOrgUnit = resultlist.Where(x => x.ActionGroupNameAr == ActionGroupNameAr || x.ActionGroupNameEn == ActionGroupNameEn).FirstOrDefault();
            if (objOrgUnit != null)
            {
                return objOrgUnit;
            }
            else
            {
                return new ActionGroup();
            }

        }
        public List<ActionGroup> GetActionGroupByID(long GroupID)
        {
            List<ActionGroup> objReturn = new List<ActionGroup>();
            try
            {
                objReturn = _context.ActionGroups.Where(x => x.ActionGroupID == GroupID).ToList();
                return objReturn;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Screen List

        public List<Screen> GetScreensList(long BusinessSystemID)
        {
            List<Screen> objScreen = new List<Screen>();
            try
            {
                objScreen = _context.Screens.Where(x => x.BusinessSystemID == BusinessSystemID && x.Status == true).OrderByDescending(x => x.ScreenID).ToList();
                return objScreen;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<ScreenAction> GetScreensActionsList(long ScreenID)
        {
            List<ScreenAction> objScreenAction = new List<ScreenAction>();
            try
            {
                objScreenAction = _context.ScreenActions.Where(x => x.ScreenID == ScreenID && x.Status == true).ToList();
                return objScreenAction;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public Screen VerifyBusinessScreensDuplicateTOAdd(string ScreenNameAr, string ScreenNameEn, long BusinessSystemID)
        {
            return _context.Screens.Where(x => x.ScreenNameEn == ScreenNameEn || x.ScreenNameAr == ScreenNameAr && x.BusinessSystemID == BusinessSystemID).FirstOrDefault();
        }
        public Screen VerifyBusinessScreensDuplicateTOUpdate(string ScreenNameAr, string ScreenNameEn, long ScreenID, long BusinessSystemID)
        {
            var objScreen = new Screen();
            var BusinessList = new List<Screen>();
            BusinessList = _context.Screens.Where(x => x.ScreenID != ScreenID).ToList();
            objScreen = BusinessList.Where(x => x.ScreenNameAr == ScreenNameAr || x.ScreenNameEn == ScreenNameEn && x.BusinessSystemID == BusinessSystemID).FirstOrDefault();
            if (objScreen != null)
            {
                return objScreen;
            }
            else
            {
                return new Screen();
            }

        }
        public ScreenAction VerifyBusinessScreensActionDuplicateTOAdd(string ActionName, long ScreenID)
        {
            return _context.ScreenActions.Where(x => x.ActionName == ActionName && x.ScreenID == ScreenID).FirstOrDefault();
        }
        public ScreenAction VerifyBusinessScreensActionDuplicateTOUpdate(string ActionName, long ActionID, long ScreenID)
        {
            var objScreenAction = new ScreenAction();
            var ActionList = new List<ScreenAction>();
            ActionList = _context.ScreenActions.Where(x => x.ScreenID == ScreenID).ToList();
            ActionList = ActionList.Where(x => x.ActionID != ActionID).ToList();
            objScreenAction = ActionList.Where(x => x.ActionName == ActionName).FirstOrDefault();
            if (objScreenAction != null)
            {
                return objScreenAction;
            }
            else
            {
                return new ScreenAction();
            }

        }
        public bool AddUpdateScreen(Screen objScreen)
        {
            bool IsOk = false;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    objScreen.Status = true;
                    context.Screens.Add(objScreen);
                    if (objScreen.ScreenID == 0)
                    {
                        context.Screens.Add(objScreen);
                        _context.SaveChanges();

                        int Result = context.SaveChanges();
                        if (Result > 0)
                        {
                            IsOk = true;
                        }
                    }
                    else
                    {
                        _context.Entry(objScreen).State = System.Data.Entity.EntityState.Modified;
                        _context.SaveChanges();
                        IsOk = true;
                    }

                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public bool AddUpdateAction(ScreenAction objScreenAction)
        {
            bool IsOk = false;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    objScreenAction.Status = true;

                    if (objScreenAction.ActionID == 0)
                    {
                        context.ScreenActions.Add(objScreenAction);
                        _context.SaveChanges();

                        int Result = context.SaveChanges();
                        if (Result > 0)
                        {
                            IsOk = true;
                        }
                    }
                    else
                    {
                        var Result = _context.ScreenActions.Find(objScreenAction.ActionID);
                        Result.ActionName = objScreenAction.ActionName;
                        Result.ActionTypeID = objScreenAction.ActionTypeID;
                        Result.ActionCommunicationMethod = objScreenAction.ActionCommunicationMethod;
                        Result.ActionDesc = objScreenAction.ActionDesc;
                        //context.ScreenActions.Add(Result);
                        _context.SaveChanges();
                        IsOk = true;
                    }

                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public bool RemoveScreen(long ScreenID)
        {
            bool IsOk = false;
            Screen objScreen = new Screen();

            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    var obj = context.Screens.Find(ScreenID);
                    objScreen.BusinessSystemID = obj.BusinessSystemID;
                    objScreen.Status = false;
                    objScreen.ScreenNameEn = obj.ScreenNameEn;
                    objScreen.ScreenID = obj.ScreenID;
                    objScreen.ScreenNameAr = obj.ScreenNameAr;
                    objScreen.ScreenDescEn = obj.ScreenDescEn;
                    objScreen.ScreenDescAr = obj.ScreenDescAr;
                    context.Screens.Add(objScreen);
                    _context.Entry(objScreen).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    IsOk = true;

                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public bool RemoveAction(long ActionID)
        {
            bool IsOk = false;
            ScreenAction objAction = new ScreenAction();

            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    var obj = context.ScreenActions.Find(ActionID);
                    objAction.ActionCommunicationMethod = obj.ActionCommunicationMethod;
                    objAction.Status = false;
                    objAction.ActionID = obj.ActionID;
                    objAction.ActionDesc = obj.ActionDesc;
                    objAction.ActionName = obj.ActionName;
                    objAction.ActionTypeID = obj.ActionTypeID;
                    objAction.ScreenID = obj.ScreenID;
                    context.ScreenActions.Add(objAction);
                    _context.Entry(objAction).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                    IsOk = true;

                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public List<Screen> GetScreenByID(long ScreenID)
        {
            List<Screen> objReturn = new List<Screen>();
            try
            {
                objReturn = _context.Screens.Where(x => x.ScreenID == ScreenID).ToList();
                return objReturn;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public List<ScreenAction> GetScreenActionsByID(long ActionID)
        {
            List<ScreenAction> objReturn = new List<ScreenAction>();
            try
            {
                objReturn = _context.ScreenActions.Where(x => x.ActionID == ActionID).ToList();
                return objReturn;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Role Management

        public Role VerifyDuplicateRoleToAdd(string RoleNameEn, string RoleNameAr, long BusinessSystemID)
        {
            return _context.Roles.Where(x => x.RoleNameEn == RoleNameEn || x.RoleNameAr == RoleNameAr && x.BusinessSystemID == BusinessSystemID).ToList().FirstOrDefault();
        }
        public Role VerifyDuplicateRoleToUpdate(string RoleNameEn, string RoleNameAr, long BusinessSystemID, long RoleID)
        {
            var objRole = new Role();
            var RoleList = new List<Role>();
            RoleList = _context.Roles.Where(x => x.BusinessSystemID == BusinessSystemID).ToList();
            RoleList = RoleList.Where(x => x.RoleID != RoleID).ToList();
            objRole = RoleList.Where(x => x.RoleNameEn == RoleNameEn || x.RoleNameAr == RoleNameAr).FirstOrDefault();
            if (objRole != null)
            {
                return objRole;
            }
            else
            {
                return new Role();
            }

        }
        public int AddUpdateRole(Role objRole)
        {
            int Result = 0;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    objRole.Status = true;
                    if (objRole.RoleID == 0)
                    {
                        context.Roles.Add(objRole);
                        _context.SaveChanges();
                        context.SaveChanges();
                        Result = Convert.ToInt32(objRole.RoleID);
                    }
                    else
                    {
                        var RoleResult = _context.Roles.Find(objRole.RoleID);
                        RoleResult.RoleNameAr = objRole.RoleNameAr;
                        RoleResult.RoleNameEn = objRole.RoleNameEn;
                        RoleResult.RoleDescEn = objRole.RoleDescEn;
                        RoleResult.RoleDescAr = objRole.RoleDescAr;
                        RoleResult.BusinessSystemID = objRole.BusinessSystemID;

                        Result = context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {

                }
                return Result;

            }

        }
        public bool RemoveRole(long RoleID)
        {
            bool IsOk = false;

            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    var obj = context.Roles.Find(RoleID);
                    obj.Status = false;
                    int Result = context.SaveChanges();
                    IsOk = true;

                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }
        public Role GetRoleByID(long RoleID)
        {
            Role objRole = new Role();
            try
            {
                objRole = _context.Roles.Find(RoleID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return objRole;
        }
        public List<Role> GetRolesListByBusinessSystemID(long BusinessSystemID)
        {
            List<Role> objRole = new List<Role>();
            try
            {
                objRole = _context.Roles.Where(x => x.BusinessSystemID == BusinessSystemID && x.Status == true).ToList(); ;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return objRole;
        }
        public List<Role> GetRolesList()
        {
            List<Role> objRole = new List<Role>();
            try
            {
                objRole = _context.Roles.Where(x => x.Status == true).ToList(); ;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return objRole;
        }
        public List<UserRole> GetRolesListByUserID(long UserID)
        {
            List<UserRole> objRole = new List<UserRole>();
            try
            {
                objRole = _context.UserRoles.Where(x => x.UserID == UserID).ToList(); ;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return objRole;
        }
        public List<RolePermission> GetActionsByRoleID(long RoleID)
        {
            List<RolePermission> objRole = new List<RolePermission>();
            try
            {
                objRole = _context.RolePermissions.Where(x => x.RoleID == RoleID).ToList(); ;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return objRole;
        }

        public int AddRolePermissions(List<RolePermission> objRolePermission)
        {
            int Result = 0;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    if (objRolePermission.Count > 0)
                    {
                        long RoleID = objRolePermission.FirstOrDefault().RoleID.Value;

                        List<RolePermission> objList = _context.RolePermissions.Where(x => x.RoleID == RoleID).ToList();
                        foreach (var item in objList)
                        {
                            var p = context.RolePermissions.SingleOrDefault(x => x.RolePermissionID == item.RolePermissionID);
                            context.RolePermissions.Remove(p);
                            context.SaveChanges();
                        }

                    }

                    foreach (var item in objRolePermission)
                    {
                        RolePermission objRolePermissions = new RolePermission();
                        objRolePermissions.ActionID = item.ActionID;
                        objRolePermissions.RoleID = item.RoleID;
                        context.RolePermissions.Add(objRolePermissions);
                        Result = context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {

                }
                return Result;

            }
        }

        public int AddUserRoles(List<UserRole> objRoles)
        {
            int Result = 0;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    if (objRoles.Count > 0)
                    {
                        long UserID = objRoles.FirstOrDefault().UserID;

                        List<UserRole> objList = _context.UserRoles.Where(x => x.UserID == UserID).ToList();
                        foreach (var item in objList)
                        {
                            var p = context.UserRoles.SingleOrDefault(x => x.Id == item.Id);
                            context.UserRoles.Remove(p);
                            context.SaveChanges();
                        }

                    }
                    foreach (var item in objRoles)
                    {
                        UserRole objUserRole = new UserRole();
                        objUserRole.UserID = item.UserID;
                        objUserRole.RoleID = item.RoleID;
                        context.UserRoles.Add(objUserRole);
                        Result = context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {

                }
                return Result;

            }
        }

        public bool SaveAuditTrailEComplaint(EAuditTrail objAuditTrail)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
