﻿using Common;
using DataAccess.CommonRespository;
using DataAccess.Database;
using eInspectionSystemWebApi.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccess.Repository
{
    public class UserRepository : SharedComponentGenericRepository<User>, IUserRepository
    {
        public UserRepository(SharedComponentsEntities context)
            : base(context)
        {

        }

        public User GetUserInfo(string UserName, string Password)
        {
            User objuser = new User();
            var dbuser = _context.Users.Where(x => x.UserName.ToLower() == UserName.ToLower() && x.UserPassword == Password);
            if (dbuser.Count() > 0)
            {
                return dbuser.FirstOrDefault();
            }
            else
            {
                return objuser;
            }
        }

        public User GetUserInfoByUserName(string UserName)
        {
            User objuser = new User();
            var dbuser = _context.Users.Where(x => x.UserName.ToLower() == UserName.ToLower());
            if (dbuser.Count() > 0)
            {
                return dbuser.FirstOrDefault();
            }
            else
            {
                return objuser;
            }
        }

        public List<Screen> GetScreensList(long BusinessSystemID)
        {
            return _context.Screens.Where(x => x.BusinessSystemID == BusinessSystemID).ToList();

        }

        public User GetUserInfoByEmail(string Email)
        {
            User objuser = new User();
            var dbuser = _context.Users.Where(x => x.UserEmail == Email.ToLower() && x.UserStatus == true);
            if (dbuser.Count() > 0)
            {
                return dbuser.FirstOrDefault();
            }
            else
            {
                return objuser;
            }
        }

        public List<User> GetUsersListByBusinessSystemID(long BusinessSystemID, bool IsForAdmin)
        {
            if (IsForAdmin)
            {
                if (BusinessSystemID > 0)
                {
                    return _context.Users.Where(x => x.BusinessSystemID.Value == BusinessSystemID).ToList();
                }
                else
                {
                    return _context.Users.ToList();
                }
            }
            else
            {
                if (BusinessSystemID > 0)
                {
                    return _context.Users.Where(x => x.BusinessSystemID.Value == BusinessSystemID && x.UserStatus == true).ToList();
                }
                else
                {
                    return _context.Users.Where(x => x.UserStatus == true).ToList();

                }
            }

        }
        public List<UserRole> GetUsersByRoleID(long RoleID)
        {
            return _context.UserRoles.Where(x => x.RoleID == RoleID).ToList();
        }

        public List<User> GetUsersByCompanyID(long CompanyID)
        {
            return _context.Users.Where(x => x.UserCompanyID == CompanyID && x.UserStatus == true).ToList();
        }
        public User VerifyDuplicateUser(string UserNameEn, string Useremail, string Usermobile, string EmiratesId)
        {

            User objuser = new User();
            var dbUser = _context.Users.Where(x => x.UserName.ToLower() == UserNameEn.ToLower() || x.UserEmail == Useremail || x.UserMobile == Usermobile.ToLower() || x.UserEID == EmiratesId);
            if (dbUser.Count() > 0)
            {
                objuser = dbUser.FirstOrDefault();
            }

            return objuser;
        }

        public User VerifyDuplicateUserToUpdate(string UserNameEn, string Useremail, string Usermobile, string EmiratesId, long UserId)
        {

            User objuser = new User();
            var dbUser = _context.Users.Where(x => x.UserID != UserId).ToList();
            dbUser = dbUser.Where(x => x.UserName == Convert.ToString(UserNameEn) || x.UserEmail == Convert.ToString(Useremail) || x.UserMobile == Convert.ToString(Usermobile) || x.UserEID == Convert.ToString(EmiratesId)).ToList();

            if (dbUser.Count() > 0)
            {
                objuser = dbUser.FirstOrDefault();
            }

            return objuser;
        }

        public UserSubscriptionRequest VerifyPublicDuplicateUser(string UserNameEn, string Useremail, string Usermobile, string EmiratesId)
        {
            long RequestStatusID = Convert.ToInt64(RequestStatuses.Rejected);
            UserSubscriptionRequest objuser = new UserSubscriptionRequest();
            var dbUser = _context.UserSubscriptionRequests.Where(x => x.UserName.ToLower() == UserNameEn.ToLower() || x.UserEmail == Useremail || x.UserMobile == Usermobile.ToLower() || x.UserEID == EmiratesId).ToList();
            dbUser = dbUser.Where(x => x.RequestStatusID.Value != RequestStatusID).ToList();
            if (dbUser != null)
            {
                if (dbUser.Count() > 0)
                {
                    objuser = dbUser.FirstOrDefault();
                }
            }
            return objuser;
        }

        public List<ScreenPermissionModel> GetScreensListByUserID(long UserID)
        {
            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter { ParameterName = "UserID", Value = UserID };
                using (SharedComponentsEntities context = new SharedComponentsEntities())
                {
                    IEnumerable<ScreenPermissionModel> ScreensList = context.Database.SqlQuery<ScreenPermissionModel>("exec Proc_GetScreenListByUserID  @UserID", parameters).ToList();

                    return ScreensList.ToList();


                }
            }
            catch (DbUpdateException e)
            {
                SqlException s = e.InnerException.InnerException as SqlException;

                return null;
            }
        }

        public bool UpdateUserTask(UserTask objUserTask)
        {
            bool IsOk = false;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    context.UserTasks.Add(objUserTask);
                    int Result = context.SaveChanges();
                    if (Result > 0)
                    {
                        IsOk = true;
                    }
                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }

        public bool RegisterPublicUser(UserSubscriptionRequest objUserSubscriptionRequest)
        {
            bool IsOk = false;
            using (SharedComponentsEntities context = new SharedComponentsEntities())
            {
                try
                {
                    context.UserSubscriptionRequests.Add(objUserSubscriptionRequest);
                    int Result = context.SaveChanges();
                    if (Result > 0)
                    {
                        IsOk = true;
                    }
                }
                catch (Exception ex)
                {
                    IsOk = false;

                }
                return IsOk;

            }

        }

        public bool UpdateLinkStatus(long UserID, bool Status)
        {
            bool IsOk = false;
            try
            {

                var Result = _context.Users.Find(UserID);

                Result.IsClicked = Status;
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                IsOk = true;
            }
            return IsOk;
        }
        public List<UserType> GetUsersType()
        {

            return _context.UserTypes.ToList();
        }
    }
}
