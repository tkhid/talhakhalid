﻿using DataAccess.Database;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.CommonRespository
{
    public interface ISharedComponentUnitOfWork
    {

        /// <summary>
        /// Saves this instance
        /// </summary>
        int Save();

        /// <summary>
        /// DB context
        /// </summary>
        /// <returns></returns>
        SharedComponentsEntities DataContext();

        /// <summary>
        /// Begins transaction
        /// </summary>
        /// <returns></returns>
        void BeginTransaction();

        /// <summary>
        /// Commits transaction
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// Rolls back transaction
        /// </summary>
        void RollBackTransaction();

        /// <summary>
        /// Get User repository
        /// </summary>
        IUserRepository UserRepository { get; }
        
        
        /// <summary>
        /// Get Shared Component
        /// </summary>
        ISharedComponentRepository SharedComponentRepository { get; }

    }

}
