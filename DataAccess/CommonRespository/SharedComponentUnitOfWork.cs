﻿using DataAccess.Database;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
using Db = DataAccess.Database;

namespace DataAccess.CommonRespository
{
    public class SharedComponentUnitOfWork : ISharedComponentUnitOfWork, IDisposable
    {
        #region Generic Section

        protected readonly SharedComponentsEntities _dbContext;

        /// <summary>
        /// _dbTransaction
        /// </summary>
        private DbContextTransaction _dbTransaction = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        public SharedComponentUnitOfWork()
        {
            _dbContext = new SharedComponentsEntities();
        }

        /// <summary>
        /// Data Context.
        /// </summary>
        /// <returns></returns>
        public SharedComponentsEntities DataContext()
        {
            return _dbContext ?? new SharedComponentsEntities();
        }
        public int Save()
        {
            int rowsEffected = 0;

            try
            {
                rowsEffected = _dbContext.SaveChanges();
            }
            catch (OptimisticConcurrencyException)
            {
                RollBackTransaction();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder validationErros = new StringBuilder();

                foreach (var item in ex.EntityValidationErrors)
                {
                    foreach (var error in item.ValidationErrors)
                    {
                        validationErros.Append(string.Format("Property Name ={0} Validation Error ={1}", error.PropertyName, error.ErrorMessage));
                    }
                }

                throw new Exception(validationErros.ToString());
            }

            return rowsEffected;
        }

        public void BeginTransaction()
        {
            _dbContext.Database.Connection.Open();
            _dbTransaction = _dbContext.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (_dbTransaction != null)
                _dbTransaction.Commit();

            if (_dbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                _dbContext.Database.Connection.Close();
        }

        public void RollBackTransaction()
        {
            if (_dbTransaction != null)
                _dbTransaction.Rollback();
            if (_dbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                _dbContext.Database.Connection.Close();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_dbTransaction != null)
                    {
                        _dbTransaction.Dispose();
                    }
                    if (_dbContext != null)
                    {
                        _dbContext.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        private IUserRepository _UserRepository;
        public IUserRepository UserRepository
        {
            get
            {

                if (_UserRepository == null)
                {
                    _UserRepository = new UserRepository(_dbContext);
                }
                return _UserRepository;
            }
        }

        private ISharedComponentRepository _SharedComponentRepository;
        public ISharedComponentRepository SharedComponentRepository
        {
            get
            {
                if (_SharedComponentRepository == null)
                {
                    _SharedComponentRepository = new SharedComponentRepository(_dbContext);
                }
                return _SharedComponentRepository;
            }
        }
    }
}
