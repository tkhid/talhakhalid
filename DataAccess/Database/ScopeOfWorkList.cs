//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IA.EC.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScopeOfWorkList
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScopeOfWorkList()
        {
            this.ScopeOfWorkDetails = new HashSet<ScopeOfWorkDetail>();
            this.ScopeOfWorkSubCatDetailRelations = new HashSet<ScopeOfWorkSubCatDetailRelation>();
            this.SOWHistories = new HashSet<SOWHistory>();
        }
    
        public long SOWID { get; set; }
        public string SOWDisplayID { get; set; }
        public string SOWDescriptionEn { get; set; }
        public string SOWDescriptionAr { get; set; }
        public Nullable<long> SOWCreatorID { get; set; }
        public Nullable<System.DateTime> SOWDate { get; set; }
        public long SOWStatusID { get; set; }
        public long SOWTypeID { get; set; }
        public Nullable<bool> SOWActiveState { get; set; }
        public Nullable<System.DateTime> SOWLastChangeDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScopeOfWorkDetail> ScopeOfWorkDetails { get; set; }
        public virtual ScopeOfWorkStatu ScopeOfWorkStatu { get; set; }
        public virtual ScopeOfWorkType ScopeOfWorkType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScopeOfWorkSubCatDetailRelation> ScopeOfWorkSubCatDetailRelations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SOWHistory> SOWHistories { get; set; }
    }
}
