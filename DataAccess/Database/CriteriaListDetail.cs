//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IA.EC.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class CriteriaListDetail
    {
        public long CriteriaListDetailID { get; set; }
        public long CriteriaListID { get; set; }
        public Nullable<long> CriteriaCategoryID { get; set; }
        public long CriteiaListFieldToCompareID { get; set; }
        public string CriteiaListComparisonOperator { get; set; }
        public string CriteiaListComparisonValueFrom { get; set; }
        public string CriteiaListComparisonValueTo { get; set; }
    
        public virtual CriteriaCategory CriteriaCategory { get; set; }
        public virtual CriteriaList CriteriaList { get; set; }
    }
}
