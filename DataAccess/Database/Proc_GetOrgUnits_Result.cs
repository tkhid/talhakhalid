//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess.Database
{
    using System;
    
    public partial class Proc_GetOrgUnits_Result
    {
        public long OUID { get; set; }
        public string OUNameAr { get; set; }
        public string OUNameEn { get; set; }
        public string OUDescEn { get; set; }
        public string OUDescAr { get; set; }
        public Nullable<long> OUHead { get; set; }
        public Nullable<int> count { get; set; }
    }
}
