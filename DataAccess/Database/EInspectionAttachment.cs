//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IA.EC.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class EInspectionAttachment
    {
        public long AttachmentID { get; set; }
        public string SourceComponentID { get; set; }
        public string AttachmentDescription { get; set; }
        public string AttachmentFileLocation { get; set; }
        public Nullable<long> AttachmentAddedByID { get; set; }
        public Nullable<System.DateTime> AttachmentDate { get; set; }
    }
}
