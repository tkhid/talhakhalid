//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IA.EC.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class InspectionPlanHistory
    {
        public long InspectionPlanHistoryID { get; set; }
        public long InspectionPlanID { get; set; }
        public string ChangeDescription { get; set; }
        public Nullable<long> ChangeMadeByID { get; set; }
        public Nullable<System.DateTime> ChangeDate { get; set; }
        public long InspectionPlanStatusAfterChange { get; set; }
    
        public virtual InspectionPlanList InspectionPlanList { get; set; }
        public virtual InspectionPlanStatu InspectionPlanStatu { get; set; }
    }
}
