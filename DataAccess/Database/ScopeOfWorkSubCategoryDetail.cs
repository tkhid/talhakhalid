//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IA.EC.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScopeOfWorkSubCategoryDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ScopeOfWorkSubCategoryDetail()
        {
            this.ExecutionSummaryReportDetails = new HashSet<ExecutionSummaryReportDetail>();
            this.RecommendationsLetterDetails = new HashSet<RecommendationsLetterDetail>();
            this.ScopeOfWorkSubCatDetailRelations = new HashSet<ScopeOfWorkSubCatDetailRelation>();
            this.SubCatDetailOnsiteReportRelations = new HashSet<SubCatDetailOnsiteReportRelation>();
        }
    
        public long SOWSubCategoryDetailID { get; set; }
        public long SOWSubCategoryID { get; set; }
        public string SOWSubCategoryDetailAr { get; set; }
        public string SOWSubCategoryDetailEn { get; set; }
        public string SOWSubCategoryDetailRelatedLawDescription { get; set; }
        public string SOWSubCategoryDetailRelatedLawDetails { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExecutionSummaryReportDetail> ExecutionSummaryReportDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RecommendationsLetterDetail> RecommendationsLetterDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScopeOfWorkSubCatDetailRelation> ScopeOfWorkSubCatDetailRelations { get; set; }
        public virtual ScopeOfWorkSubCategory ScopeOfWorkSubCategory { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubCatDetailOnsiteReportRelation> SubCatDetailOnsiteReportRelations { get; set; }
    }
}
