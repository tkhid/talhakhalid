﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using static IA.MM.Constant.ExternalService;

namespace Common
{
    public class Systems
    {

        public string Decrypt(string base64StringToDecrypt)
        {
            base64StringToDecrypt = base64StringToDecrypt.Replace("secret", "+");
            //Set up the encryption objects
            string strMessage = string.Empty;
            try
            {
                string passphrase = ConfigurationManager.AppSettings["EncryptionKey"].ToString();
                using (AesCryptoServiceProvider acsp = GetProvider(Encoding.UTF8.GetBytes(passphrase)))
                {
                    byte[] RawBytes = Convert.FromBase64String(base64StringToDecrypt);
                    //byte[] RawBytes = System.Text.UTF8Encoding.ASCII.GetBytes(base64StringToDecrypt);

                    ICryptoTransform ictD = acsp.CreateDecryptor();

                    //RawBytes now contains original byte array, still in Encrypted state

                    //Decrypt into stream
                    MemoryStream msD = new MemoryStream(RawBytes, 0, RawBytes.Length);
                    CryptoStream csD = new CryptoStream(msD, ictD, CryptoStreamMode.Read);
                    //csD now contains original byte array, fully decrypted

                    //return the content of msD as a regular string
                    strMessage = (new StreamReader(csD)).ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                strMessage = Convert.ToString(StatusType.Error);
            }
            return strMessage;
        }

        public string Encrypt(string plainSourceStringToEncrypt)
        {
            //Set up the encryption objects
            string strMessage = string.Empty;
            try
            {
                string passPhrase = ConfigurationManager.AppSettings["EncryptionKey"].ToString();
                using (AesCryptoServiceProvider acsp = GetProvider(Encoding.UTF8.GetBytes(passPhrase)))
                {
                    byte[] sourceBytes = Encoding.ASCII.GetBytes(plainSourceStringToEncrypt);
                    ICryptoTransform ictE = acsp.CreateEncryptor();

                    //Set up stream to contain the encryption
                    MemoryStream msS = new MemoryStream();

                    //Perform the encrpytion, storing output into the stream
                    CryptoStream csS = new CryptoStream(msS, ictE, CryptoStreamMode.Write);
                    csS.Write(sourceBytes, 0, sourceBytes.Length);
                    csS.FlushFinalBlock();

                    //sourceBytes are now encrypted as an array of secure bytes
                    byte[] encryptedBytes = msS.ToArray(); //.ToArray() is important, don't mess with the buffer

                    //return the encrypted bytes as a BASE64 encoded string
                    strMessage = Convert.ToBase64String(encryptedBytes);
                    strMessage = strMessage.Replace("+", "secret");
                }
            }
            catch (Exception ex)
            {
                strMessage = Convert.ToString(StatusType.Error);
            }
            return strMessage;
        }

        private static AesCryptoServiceProvider GetProvider(byte[] key)
        {
            AesCryptoServiceProvider result = new AesCryptoServiceProvider();
            result.BlockSize = 128;
            result.KeySize = 128;
            result.Mode = CipherMode.CBC;
            result.Padding = PaddingMode.PKCS7;

            result.GenerateIV();
            result.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            byte[] RealKey = GetKey(key, result);
            result.Key = RealKey;
            // result.IV = RealKey;
            return result;
        }

        private static byte[] GetKey(byte[] suggestedKey, SymmetricAlgorithm p)
        {
            byte[] kRaw = suggestedKey;
            List<byte> kList = new List<byte>();

            for (int i = 0; i < p.LegalKeySizes[0].MinSize; i += 8)
            {
                kList.Add(kRaw[(i / 8) % kRaw.Length]);
            }
            byte[] k = kList.ToArray();
            return k;
        }
    }

    public class DepartmentListModelCommon
    {

        public long OUID { get; set; }
        public string OUNameAr { get; set; }
        public string OUDescAr { get; set; }
        public string OUNameEn { get; set; }
        public string OUDescEn { get; set; }
        public Nullable<long> OUHead { get; set; }

    }
    public class CriteriaDeleteDBModel
    {
        public long UserID { get; set; }
        public string DeleteID { get; set; }
        public long BuisnessSystemID { get; set; }
        public string ActionMachineName { get; set; }

    }
    public class NominationRequestDetailModel
    {
        public long NominationRequestDetailID { get; set; }
        public long NominationRequestID { get; set; }
        public long NominationRequestOrgUnitID { get; set; }
        public long NominationInsuranceCompanyID { get; set; }
        public string NominationReason { get; set; }
        public int CurrentYearPlan { get; set; }
        public int FutureYearPlan { get; set; }
        public DateTime NominationDate { get; set; }
        public int count { get; set; }

    }

    public class InsuranceCompaniesModel
    {
        public long InsuranceCompanyID { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string InsuranceCompanyBuisnessNature { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        public DateTime InsuranceCompanyFirstLicensingDate { get; set; }
        public DateTime InsuranceCompanyLastLicensingDate { get; set; }
        public string InsuranceCompanyLicensingNumber { get; set; }
        public int count { get; set; }

    }

    public class InsuranceCompanyDetailModel
    {
        public int SequenceID { get; set; }
        public long InsuranceCompanyID { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string InsuranceCompanyBuisnessNature { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        public DateTime InsuranceCompanyFirstLicensingDate { get; set; }
        public DateTime InsuranceCompanyLastLicensingDate { get; set; }
        public string InsuranceCompanyLicensingNumber { get; set; }
        public string InsuranceCompanySiteLocation { get; set; }
        public string InsuranceCompanyLocationOnMap { get; set; }
        public string TelephoneNumber { get; set; }
        public string Faxnumber { get; set; }
        public string OwnerNumber { get; set; }
        public string OwnerTelephone { get; set; }
        public string OwnerMobile { get; set; }
        public string OwnerEmailAddress { get; set; }
        public string GeneralManagerName { get; set; }
        public string GeneralManagerTelephone { get; set; }
        public string GeneralManagerMobile { get; set; }
        public string GeneralManagerEmailAddress { get; set; }
        public string InusranceCompanyEmailAddress { get; set; }
        public string InsuranceCompanyWebsite { get; set; }
    }

    public class CriteriaListDetailPopupModel
    {
        public long CriteriaListDetailID { get; set; }
        public long CriteriaListID { get; set; }
        public long CriteriaListFieldToCompareID { get; set; }
        public string CriteriaListFieldToCompareEn { get; set; }
        public string CriteriaListFieldToCompareAr { get; set; }
        public string CriteriaListComparisonOperator { get; set; }
        public string CriteriaListComparisonValueFrom { get; set; }
        public string CriteriaListComparisonValueTo { get; set; }
        public long CriteriaListSystemID { get; set; }
        public string CriteriaListSystemNameEn { get; set; }
        public string CriteriaListSystemNameAr { get; set; }
        public long CriteriaListTableID { get; set; }
        public string CriteriaListTableName { get; set; }


    }
    public class SearchModel
    {
        public string InsuranceCompanyName { get; set; }
        public int InsuranceCompanyBuisnessNatureID { get; set; }
        public string InsuranceCompanyAddress { get; set; }
        public DateTime InsuranceCompanyFirstLicensingDate { get; set; }
        public DateTime InsuranceCompanyLastLicensingDate { get; set; }
        public string InsuranceCompanyLicensingNumber { get; set; }

        // public List<CriteriaListDetailPopupModel> ActiveCriteriaSelection { get; set; }


    }
    public class NominatedStaff
    {
        public int staffId { get; set; }
        public string NominatedPersonEn { get; set; }
        public string NominatedPersonAr { get; set; }
        public string StaffTypeCode { get; set; }
        public int StaffTypeId { get; set; }
        public int StaffStatusID { get; set; }
        public string TransactionID { get; set; }
    }

    public class StaffDetail
    {

        public string MobileNo { get; set; }
        public DateTime DateToBeCreated { get; set; }
        public string StaffNameEn { get; set; }
        public string StaffNameAr { get; set; }
        public string Position { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string NationalityNameEn { get; set; }
        public string NationalityNameAr { get; set; }
        public DateTime Dob { get; set; }
        public string PassportNo { get; set; }
        public int ResidenceStatusId { get; set; }
        public string EmirateID { get; set; }
        public string VisaNo { get; set; }
        public string Address { get; set; }

    }

    public class StaffListCommon
    {
        public int count { get; set; }
        public int StaffID { get; set; }
        public string NameEn { get; set; }
        public string Namear { get; set; }
        public string Phone { get; set; }
        public string CountryNameEn { get; set; }
        public string CountryName { get; set; }
        public string StaffTypeCode { get; set; }
        public int StaffStatusID { get; set; }
        public string EmirateID { get; set; }
        public string VisaNo { get; set; }
        public string PassportNo { get; set; }
        public int ResidenceStatusId { get; set; }
        public DateTime BirthDate { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }

    }
    public class InterviewList
    {
        public string IntervieiwerName { get; set; }
        public string PositionName { get; set; }

        public int InterviewId { get; set; }
    }
    public class StaffQualificationList
    {
        public int FilePathID { get; set; }
        public int StaffStatusID { get; set; }
        public int StaffQualificationId { get; set; }
        public int count { get; set; }
        public int StaffID { get; set; }
        public string Nominated { get; set; }
        public string CertificateNumber { get; set; }
        public string certName { get; set; }
        public string Specialization { get; set; }
        public string Institution { get; set; }
        public DateTime AchivedDate { get; set; }
        public string NationalityNameEn { get; set; }
        public string NationalityNameAr { get; set; }
        public string Description { get; set; }


    }
    public class StaffAttachmentList
    {
        public int StaffStatusID { get; set; }
        public int StaffID { get; set; }
        public int FileID { get; set; }
        public string FilePathText { get; set; }
        public string FileType { get; set; }
        public string Nominated { get; set; }
        public string FileTypeCode { get; set; }
        public int count { get; set; }
    }
    public class StaffExpList
    {
        public int StaffStatusID { get; set; }
        public int FilePathID { get; set; }
        public int ExperienceID { get; set; }
        public int count { get; set; }

        public int StaffID { get; set; }
        public string Nominated { get; set; }
        public string CompanyName { get; set; }
        public string Position { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Description { get; set; }
        public string Specification { get; set; }
        public string FileName { get; set; }
        public string ExperiencType { get; set; }
    }

    public class GridActivities
    {
        public int PageStart { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int FilteredCount { get; set; }
        public string SortDirection { get; set; }
        public string SortColumn { get; set; }

    }

    public class NominationRequestModel
    {
        public long NominationRequestID { get; set; }
        public string NominationRequestDisplayID { get; set; }
        public DateTime NominationRequestDate { get; set; }
        public string DepartmentConcatenatedName { get; set; }
        public string DepartmentNameEn { get; set; }
        public string DepartmentNameAr { get; set; }
        public string NominationRequestDateString { get; set; }
        public int count { get; set; }
    }

    public class CriteriaListModel
    {
        public long CriteriaListID { get; set; }
        public string CriteriaListDisplayID { get; set; }
        public string CriteriaListName { get; set; }
        public string CriteriaListDescription { get; set; }
        public long CriteriaListStatusID { get; set; }
        public bool? CriteriaListActiveState { get; set; }
        public string CriteriaListStatusEn { get; set; }
        public string CriteriaListStatusAr { get; set; }
        public DateTime CriteriaListDate { get; set; }
        public int count { get; set; }

    }

    public class SearchCriteria
    {
        public string CriteriaListName { get; set; }
        public string CriteriaListDescription { get; set; }
        public int CriteriaListStatusID { get; set; }
        public DateTime CriteriaDateTo { get; set; }
        public DateTime CriteriaDateFrom { get; set; }

    }
    public class ScopeOfWorkListSearchModel
    {
        public List<ScopeOfWorkListDBModel> Data { get; set; }
        public GridActivitiesCommonModel GridActivities { get; set; }


    }
    public class ScopeOfWorkMainCategorySystemModel
    {
        public List<SOWMainCategoryCustomModel> Data { get; set; }
        public GridActivitiesCommonModel GridActivities { get; set; }


    }
    public class ScopeOfWorkSubCategorySystemModel
    {
        public List<SOWSubCategoryCustomModel> Data { get; set; }
        public GridActivitiesCommonModel GridActivities { get; set; }


    }
    public class ScopeOfWorkSubCategoryDetailSystemModel
    {
        public List<SOWSubCategoryDetailCustomModel> Data { get; set; }
        public GridActivitiesCommonModel GridActivities { get; set; }


    }
    public class ScopeOfWorkAttachmentsSystemModel
    {
        public List<EInspectionAttachmentCustomModel> Data { get; set; }
        public GridActivitiesCommonModel GridActivities { get; set; }


    }
    public class ScopeOfWorkCommentsSystemModel
    {
        public List<SOWCommentCustomModel> Data { get; set; }
        public GridActivitiesCommonModel GridActivities { get; set; }


    }
    public class ScopeOfWorkHistorySystemModel
    {
        public List<SOWHistoryCustomModel> Data { get; set; }
        public GridActivitiesCommonModel GridActivities { get; set; }


    }

    public class SOWSearchModel
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public long SowStatusId { get; set; }
        public long SowTypeId { get; set; }

        //public string PageNumber { get; set; }
        //public string PageSize { get; set; }
        //public string SortColum { get; set; }
        //public string SortDir { get; set; }

    }
    public class ScopeOfWorkListDBModel
    {
        public long SOWID { get; set; }
        public string SOWDisplayID { get; set; }
        public string SOWDescriptionEn { get; set; }
        public string SOWDescriptionAr { get; set; }
        public long SOWCreatorID { get; set; }
        //  [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime SOWDate { get; set; }
        public string SOWDateString { get; set; }
        public int count { get; set; }
        public long SOWTypeID { get; set; }
        public long SOWStatusID { get; set; }
        public bool SOWActiveState { get; set; }
        public string SowStatus { get; set; }
        public string SowTypeEn { get; set; }
        public string SowTypeAr { get; set; }
        public string SOWStatusEn { get; set; }
        public string SOWStatusAr { get; set; }
        public string CreatedByName { get; set; }
        public string SOWLastChangeDateString { get; set; }
        public DateTime SOWLastChangeDate { get; set; }


    }



    public class ScopeOfWorkMainCategorySearchModel
    {

        public long SOWMainCategoryID { get; set; }
        public string SOWMainCategoryNameAr { get; set; }
        public string SOWMainCategoryNameEn { get; set; }
        public string SOWMainCategoryDescriptionAr { get; set; }
        public string SOWMainCategoryDescriptionEn { get; set; }
        public int TotalRecords { get; set; }
        public string PageNumber { get; set; }
        public string PageSize { get; set; }
        public string SortColum { get; set; }
        public string SortDir { get; set; }

    }


    public class SOWMainCategoryCustomModel
    {
        public long SOWMainCategoryID { get; set; }
        public string SOWMainCategoryNameAr { get; set; }
        public string SOWMainCategoryNameEn { get; set; }
        public string SOWMainCategoryDescriptionAr { get; set; }
        public string SOWMainCategoryDescriptionEn { get; set; }
        public int count { get; set; }


    }
    public class SOWSubCategoryCustomModel
    {
        public long SOWSubCategoryID { get; set; }
        public long SOWMainCategoryID { get; set; }
        public string SOWSubCategoryNameAr { get; set; }
        public string SOWSubCategoryNameEn { get; set; }
        public string SOWSubCategoryDescriptionEn { get; set; }
        public string SOWSubCategoryDescriptionAr { get; set; }

        public string SOWSubCategoryRelatedLawDescription { get; set; }
        public string SOWSubCategoryRelatedLawDetails { get; set; }
        public int count { get; set; }

    }
    public class SOWSubCategoryDetailCustomModel
    {

        public long SOWSubCategoryDetailID { get; set; }
        public long SOWSubCategoryID { get; set; }
        public string SOWSubCategoryDetailAr { get; set; }
        public string SOWSubCategoryDetailEn { get; set; }
        public string SOWSubCategoryDetailRelatedLawDescription { get; set; }
        public string SOWSubCategoryDetailRelatedLawDetails { get; set; }

        public int count { get; set; }

    }
    public class SOWHistoryCustomModel
    {

        public long SOWHistoryID { get; set; }
        public long SOWID { get; set; }
        public string ChangeDescription { get; set; }
        public long ChangeMadeByID { get; set; }
        public string ChangeMadeName { get; set; }

        public System.DateTime ChangeDate { get; set; }
        public string ChangeDateString { get; set; }
        public Nullable<long> SOWStatusAfterChange { get; set; }

        public string SOWStatusEn { get; set; }
        public string SOWStatusAr { get; set; }
        public int count { get; set; }

    }
    public class ScopeOfWorkListPrintApiModel
    {
        public List<ScopeOfWorkCustomPrintModel> Data { get; set; }

    }

    public class ScopeOfWorkCustomPrintModel
    {
        public Int64 SOWID { get; set; }
        public string SOWDisplayID { get; set; }
        public string SOWDescriptionEn { get; set; }
        public string SOWStatusEn { get; set; }
        public string SOWSubCategoryDescriptionEn { get; set; }
        public string SOWSubCategoryDescriptionAr { get; set; }
        public string SOWSubCategoryNameAr { get; set; }
        public string SOWSubCategoryNameEn { get; set; }
        public string SOWMainCategoryNameEn { get; set; }
        public string SOWActiveStateString { get; set; }
        public bool SOWActiveState { get; set; }
        public DateTime SOWDate { get; set; }
        public string SOWDateString { get; set; }
        public long SOWCreatorID { get; set; }
        public string CreatedByName { get; set; }
        public string SOWSubCategoryDetailEn { get; set; }
        public string SOWSubCategoryDetailAr { get; set; }
        public string SOWTypeEn { get; set; }

    }
    public class SOWCommentCustomModel
    {

        public long CommentID { get; set; }
        public string CommentDescription { get; set; }
        public long CommentAddedByID { get; set; }
        public string CommentAddedBy { get; set; }

        public System.DateTime CommentDate { get; set; }
        public string CommentDateString { get; set; }
        public int count { get; set; }

    }
    public class EInspectionAttachmentCustomModel
    {
        public long AttachmentID { get; set; }
        public string SourceComponentID { get; set; }
        public string AttachmentDescription { get; set; }
        public string AttachmentFileLocation { get; set; }
        public long AttachmentAddedByID { get; set; }
        public System.DateTime AttachmentDate { get; set; }
        public string AttachmentDateString { get; set; }
        public string AttachmentBy { get; set; }

        public int count { get; set; }
        public bool HasAccess { get; set; }
    }

    public class GridActivitiesCommonModel
    {
        public int PageStart { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int FilteredCount { get; set; }
        public string SortDirection { get; set; }
        public string SortColumn { get; set; }
    }



    public class ScreenPermissionModel
    {
        public long ActionID { get; set; }
        public long ActionTypeID { get; set; }
        public long ScreenID { get; set; }
        public long RoleID { get; set; }
        public string ActionName { get; set; }
        public string ActionDesc { get; set; }
        public string ActionCommunicationMethod { get; set; }

        public string ScreenNameAr { get; set; }
        public string ScreenDescAr { get; set; }
        public string ScreenNameEn { get; set; }
        public string ScreenDescEn
        {
            get; set;

        }

    }
    public class UIRListSearchModel
    {
        public List<UIRDBModel> Data { get; set; }
        public GridActivitiesCommonModel GridActivities { get; set; }


    }
    public class UIRCustomSearchSystemModel
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public long CompanyId { get; set; }
        public long DepartmentId { get; set; }


    }
    public class UIRDBModel
    {
        public int count { get; set; }
        public long UrgentInspectionRequestID { get; set; }
        public string UrgentInspectionRequestDisplayID { get; set; }
        public Nullable<System.DateTime> UrgentInspectionRequestDate { get; set; }
        public string UrgentInspectionRequestDateString { get; set; }

        public long UrgentInspectionRequestDepartmentID { get; set; }
        public long UrgentInspectionRequestCompanyID { get; set; }
        public string UrgentInspectionRequestDetails { get; set; }
        public long UrgentInspectionRequestStatusID { get; set; }
        public Nullable<long> UrgentInspectionRequestAddedByID { get; set; }
        public string CommentDescription { get; set; }
        public string UrgentInspectionRequestAddedByName { get; set; }
        public string comment { get; set; }

        public string DepartmentName { get; set; }
        public string DepartmentNameAr { get; set; }

        public string CompanyName { get; set; }
        public string CompanyNameAr { get; set; }


        public long UrgentInspectionStatusID { get; set; }
        public string UrgentInspectionStatusEn { get; set; }
        public string UrgentInspectionStatusAr { get; set; }

    }
    public class UrgentInspectionSystemSearchModel
    {
        public List<UIRDBModel> Data { get; set; }
        public GridActivitiesCommonModel GridActivities { get; set; }


    }
    public class UserTasksInputModelCommon
    {
        public long OriginSystemID { get; set; }
        public string UserID { get; set; }
        public DateTime TaskDate { get; set; }
        public string TaskDescription { get; set; }
        public string TaskRecepientList { get; set; }
        public long TaskStatus { get; set; }
        public string TaskURL { get; set; }
        public string OUID { get; set; }
        public bool IsUrgent { get; set; }
        public long ActionID { get; set; }
    }

    public class NotificationModel
    {
        public string UserID { get; set; }
        public string RoleID { get; set; }
        public long ActionTypeID { get; set; }
        public string OUID { get; set; }
        public bool IsUrgent { get; set; }
    }
    public class ComplaintNotificationModel
    {
        public string UserID { get; set; }
        public string CompanyID { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public long ActionTypeID { get; set; }
        public string OUID { get; set; }
        public bool IsUrgent { get; set; }
    }

    public class AuditTrailModelCommon
    {
        public long BusinessSystemID { get; set; }
        public long ActionTypeID { get; set; }
        public long ActionUserID { get; set; }
        public string ActionMachineName { get; set; }
        public string ActionComponentID { get; set; }
        public string ActionDescription { get; set; }
    }

    public class OrgUnitCustomModel
    {
        public long OUID { get; set; }
        //public string UserID { get; set; }
        public string OUNameAr { get; set; }
        public string OUDescAr { get; set; }
        public string OUNameEn { get; set; }
        public string OUDescEn { get; set; }
        public Nullable<long> OUHead { get; set; }
        public int count { get; set; }

    }

    public class OrgUnitSearchModel
    {
        public string OUNameAr { get; set; }
        public string OUDescAr { get; set; }
        public string OUNameEn { get; set; }
        public string OUDescEn { get; set; }


    }
    public class BusinessSystemsSearchModel
    {
        public string BusinessSystemNameAr { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string BusinessSystemDescEn { get; set; }
        public string BusinessSystemDescAr { get; set; }


    }

    public class BusinessSystemsCustomModel
    {
        public long BusinessSystemID { get; set; }
        public string UserID { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
        public long? BusinessSystemOwnerOUID { get; set; }
        public string BusinessSystemDescAr { get; set; }
        public string BusinessSystemDescEn { get; set; }
        public int count { get; set; }

    }

    public class UserRolesCustomModel
    {
        public long UserID { get; set; }
        public string UserEmail { get; set; }
        public string UserMobile { get; set; }

    }
}
