﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CommonConfiguratons
{
    public class Configurations
    {


        public static string GetHostName()
        {

            return Dns.GetHostName();
        }
        public static string GenerateOTP()
        {


            string strPwdchar = "abcdefghijklmnopqrstuvwxyz0123456789";
            string strPwd = "";
            Random rnd = new Random();
            for (int i = 0; i < 4; i++)
            {
                int iRandom = rnd.Next(5, strPwdchar.Length - 1);
                strPwd += strPwdchar.Substring(iRandom, 1);
            }
            return strPwd;


        }
    }
}
