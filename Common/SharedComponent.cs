﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class SharedComponent
    {
    }

    public class CriteriaFieldEModel
    {
        public long CriteriaFieldID { get; set; }
        public long? CriteriaTableID { get; set; }
        public string CriteriaFieldNameAr { get; set; }
        public string CriteriaFieldNameEn { get; set; }
        public string CriteriaFieldType { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public string BusinessSystemTableNameEn { get; set; }
        public string ExternalSystemConnection { get; set; }
        public CriteriaTableEModel CriteriaTable { get; set; }
    }

    public class CriteriaTableEModel
    {

        public long CriteriaTableID { get; set; }
        public long? CriteriaSystemID { get; set; }
        public string CriteriaTableName { get; set; }
        public string CriteriaSystemNameAr { get; set; }
        public string CriteriaSystemNameEn { get; set; }
        public List<CriteriaFieldEModel> CriteriaFields { get; set; }
    }

    public class BusinessSystemEModel
    {

        public long BusinessSystemID { get; set; }
        public string BusinessSystemNameAr { get; set; }
        public string BusinessSystemNameEn { get; set; }
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
        public int BusinessSystemOwnerOUID { get; set; }
        public string BusinessSystemDescAr { get; set; }
        public string BusinessSystemDescEn { get; set; }

        public List<CriteriaTableEModel> CriteriaTables { get; set; }
    }

    public class BusinessSystemViewModel
    {
        public List<BusinessSystemEModel> Data { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class FieldViewModel
    {
        public List<CriteriaFieldEModel> Data { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class DepartmentsListViewModel
    {
        public List<DepartmentListModelCommon> Data { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }

    }



}
