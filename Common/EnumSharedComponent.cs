﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class EnumSharedComponent
    {
        public enum Screens
        {
            Dashboard = 1,
            SelectionCriteria = 2,
            InsuranceCompanies = 3,
            SOW = 5,
            InspectionPlan = 8,
            Urgentinspectionrequests = 9,
            NominationRequest = 10,
            SubmitComplaint = 26
        }
        public enum Roles
        {
            Complaintsofficer = 18,
            InsuranceCompany = 19,
            Complainant = 20
        }
    }
}
