﻿
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace eInspectionSystemWebApi.Common
{
    public static class AutoMapperUtil
    {
        public static bool In<T>(this T source, params T[] list)
        {
            return list.Contains(source);
        }
        public static List<TDestination> GetList<TSource, TDestination>(List<TSource> before)
        {
            //  Mapper.CreateMap<TSource, TDestination>();

            if (before.Count > 0)
                AutoMapperUtil.IgnoreProperties<TSource, TDestination>(before[0]);
            else
                Mapper.Initialize(config =>
                {
                    config.CreateMap<TSource, TDestination>();
                });
            //  AutoMapper.Mapper.CreateMap<TSource, TDestination>();

            var after = Mapper.Map<List<TSource>, List<TDestination>>(before);
            return after;
        }
        public static TDestination Get<TSource, TDestination>(TSource before)
        {
            // Mapper.CreateMap<TSource, TDestination>();

            AutoMapperUtil.IgnoreProperties<TSource, TDestination>(before);

            var after = Mapper.Map<TSource, TDestination>(before);
            return after;
        }

        private static void IgnoreProperties<TSource, TDestination>(TSource source)
        {
            Type t = typeof(TSource);

            PropertyInfo[] properties = t.GetProperties();
            bool found = false;

            for (int i = 0; i < properties.Length; i++)
            {
                switch (properties[i].PropertyType.FullName)
                {
                    case "System.Web.HttpPostedFileBase":
                    case "SharjahChamberWebApi.Models.DocumentViewModel":
                    case "DataContract.Implementation.DocumentModel":

                        try
                        {
                            Mapper.Initialize(config =>
                            {
                                config.CreateMap<TSource, TDestination>().ForMember(properties[i].Name, opt => opt.Ignore());
                            });
                            //Mapper.CreateMap<TSource, TDestination>().ForMember(properties[i].Name, opt => opt.Ignore());
                        }
                        catch (Exception)
                        {
                            continue;
                        }

                        found = true;
                        break;

                    default:
                        break;
                }
            }

            if (!found)
            {
                Mapper.Initialize(config =>
                {
                    config.CreateMap<TSource, TDestination>();
                });
            }
            //   Mapper.CreateMap<TSource, TDestination>();

        }
    }
}
