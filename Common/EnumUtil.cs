﻿namespace eInspectionSystemWebApi.Common
{
    public enum RowStatus
    {
        Active = 1,
        InActive = 2,
        Delete = 3
    };

    public enum EinspectionStatusType
    {
        Success = 200,
        Empty = 201,
        Error = 203,
        Duplicate = 204,
        InvalidCredentials = 205,
        NoActiveEmailExists = 206,
        EmailSent = 207,
        AccountBlocked = 208,
        InvalidUserName = 209,
        InvalidPassworde = 210,
        UserNameAlreadyExists = 211,
        UserEmailAlreadyExists = 212,
        UserEIDAlreadyExists = 213,
        UserMobileAlreadyExists = 214,
        Failure = 215,
        AccountIsInactive = 216
    }

    public enum CompareOperator
    {
        Greater = 1,
        Less = 2,
        GreaterThanEqualto = 3,
        LessthanEqualto = 4,
        Between = 5,
        Equals = 6,


    }
    public enum InspectionPlanStatus
    {
        New = 1,
        Updated = 2,
        WaitingForReview = 3,
        Reviewed = 4,
        Rejected = 5,
        WaitingForApproval = 6,
        Approved = 7,
        Deleted = 8
    }

    public enum InspectionActivityStatus
    {
        New = 1,
        Started = 2,
        InProgress = 3,
        Finished = 4,
        Deleted = 5
    }

    public enum SystemActionsTypes
    {
        Add = 1,
        Update = 2,
        Deleted = 3,
        Login = 4,
        LogOut = 5,
        Registration = 6
    }

    public enum ValidationFlag
    {
        InvalidUserName = 1,
        InvalidPassworde = 2,
        AccountBlocked = 3,
        AccountInActive = 4
    }
    public enum SOWStatus
    {
        New = 1,
        Updated = 2,
        Waitingforapproval = 3,
        Approved = 4,
        Rejected = 5,
        Deleted = 6
    }
    public enum SOWState
    {
        InActive = 0,
        Active = 1
    }

    public enum CriteriaListStatus
    {
        New = 1,
        Updated = 2,
        Waitingforapproval = 3,
        Approved = 4,
        Rejected = 5,
        Deleted = 6

    }

    public enum ActionType
    {
        Add = 1,
        Update = 2,
        Delete = 3,
        Remove = 4,
        Print = 5,
        Clone = 6,
        View = 7,
        Active = 8,
        Reject = 9,
        SubmitForApproval = 10,
        Review = 11,
        Preview = 12,
        Approve = 13,
        Login = 14,
        AddToPlan = 15,
        Accept = 16,
        SelectCompanies = 17,
        SubmitForReview = 18,
        Resolve = 19,
        CloseAndRate = 20,
        Escalate = 21,
        Assignment = 22,
        Request = 23,
        Confirmation = 24,
        ResolveAndClose = 25,
        Send = 26,
        SignAndSend = 27,
        Reply = 28,
        Rating = 29

    }
    public enum RegisterUserTypes
    {
        Public = 1,
        Internal = 2,
        Other = 3,
        SuperAdmin = 4
    }

    public enum RequestStatuses
    {
        New = 1,
        Approved = 2,
        Rejected = 3
    }

    public enum BusinessSystemsEnum
    {
        EInspection = 2,
        LicenseRegistration = 3,
        EComplaint = 4,
        Enforcement = 5,
        MeetingManagement = 6

    }
    public enum UrgentInspectionTypes
    {
        Normal = 1,
        Urgent = 2
    }

    public enum EmailStatus
    {
        New = 1,
        Failed = 2,
        Sent = 3
    }
    public enum ActionCommunicationMethod
    {
        SMS = 1,
        Email = 2,
        Both = 3
    }
    public enum Language
    {
        English = 1,
        Arabic = 2
    }
    public enum RolesList
    {
        Inspector = 3,
        SystemAdmin = 4,
        InpectionExpert = 5,
        InspectionHead = 6,
        GeneralManager = 7,
        InspectionSecretary = 8,
        Excellenceuser = 9,
        LiecensingAndregistrationUser = 10,
        LegalUser = 11,
        ComplaintsUser = 12,
        GeneralManagerSecretary = 13,
        InsuranceUser = 14,
        ComplaintsOfficer = 18,
        InsuranceCompany = 19,
        Complainant_Consumer = 20,
        LROfficeManager = 32,
        LRGeneralManager = 33,
        FinancialExpert = 34,
        SeniorLROfficer = 35,
        LROfficer = 36,
        LegalOfficer = 37,
        IAOfficer = 38,
        LegalAffairsOfficer = 39,
        BoardMeetingCoordinator = 40,
        LegalDepartmentOffice = 41,
        Client = 42,
        LRVisitTeam = 43,
        ActuarialExpert = 44,
        LRConsultantOfficer = 45,
        IALegalOfficer = 46,
        LRADmin = 47,
        LegalExpert = 33,
        ECAdmin = 10040,
        IACallAgent = 10041,
        ComplaintOfficeHead = 10042,
        InternalOrganization = 10043,
        ExternalExpert = 10044,
        HeadOfDepartment = 10045,
        HEGeneralManager = 10046
    }

    public enum RatingType
    {
        IAResolution = 1,
        SubmitComplaint = 2,
        SubmitInquiry = 3,
        CloseComplaint = 4

    }
    public enum EntityTypes
    {
        TPA = 54,
        InsuranceCompany = 55,
        InsuranceBroker = 57,
        InsuranceAgent = 58,
        InsuranceConsultancy = 59,
        Acturial = 62,
        SurveyorLoassAdjusterorDamageEstimation = 69

    }
}
