﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eInspectionSystemWebApi.Common
{
    public class Constants
    {
        public Constants()
        {

        }

        public const string SESSION_CMS_TITLE = "cmstitle";

        public const string SESSION_FORM_DOCS = "FormsDocuments";

        public const string IMAGES_SETTINGS = "Settings";

        public const string OBJECT_NOT_FOUND_MESSAGE = "Object not found with id = ";

        public const string OBJECT_NULL_MESSAGE = "Model object is null";

        public const string PARAMETER_NULL_MESSAGE = "Fuction argument is null";

        public const string LOGGED_IN_USER_ID = "loggeduerid";

        public const string CURRENT_ACTION_METHOD = "actionmethod";

        public const string CURRENT_CONTROLLER = "controller";

        public const string CURRENT_LANGUAGE_URL = "languageurl";

        public const string SELECTED_LANGUAGE = "language";

        public const string DOCUMENT_UPLOAD_FOLDER = "Documents";

        public const string MESSAGE = "message";

        public const string RECORD_ADDED_MESSAGE = "Record added sucessfully";

        public const string RECORD_Already_MESSAGE = "Record already exist";

        public const string RECORD_UPDATED_MESSAGE = "Record updated sucessfully";

        public const string RECORD_DELETED_MESSAGE = "Record Deleted sucessfully";

        public const string ACTION_APPLIED_MESSAGE = "Action applied sucessfully";

        public const string AUTHENTICATION_SUCCESS_MESSAGE = "Authenticated sucessfully";

        public const string AUTHENTICATION_FAILURE_MESSAGE = "Authentication failed!";

        public const string INSPECTION_ACTIVITY_REMOVE_MESSAGE = "Process already started, cannot remove.";

        public const string FORGOTPASSWORD = "Forgot Password";

        public const string RECORD_NOT_UPDATED_MESSAGE = "Record not updated sucessfully";
        private static string invalidLink = IA.MM.Resources.Resources.lblLinkIsExpired;
        private static string expiredLink = IA.MM.Resources.Resources.lblLinkIsExpired;
        private static string oTPInvalid = IA.MM.Resources.Resources.lblOTPIsNotValid;
        private static string oTPExpired = IA.MM.Resources.Resources.lblOTPIsExpired;
        private static string oTPNotAvailable = IA.MM.Resources.Resources.lblOTPIsNoMoreValid;
        private static string passwordChanged = IA.MM.Resources.Resources.lblPasswordIsChangedSuccessfully;
        private static string oldPasswordIncorrectd = IA.MM.Resources.Resources.lblOldPasswordIsIncorrect;
        private static string oldPasswordCantBeNew = IA.MM.Resources.Resources.lblNewPasswordMustBeDifferentFromOld;
        public const string SystemError = "System error";
        private static string invalidCredentials = IA.MM.Resources.Resources.lblInvalidCredentials;
        private static string invalidEmail = IA.MM.Resources.Resources.lblInvalidUserEmailPleaseTryAgain;
        private static string accountBlocked = IA.MM.Resources.Resources.lblAccountBlockedFor15Minutes;
        private static string invaliduserName = IA.MM.Resources.Resources.lblInvalidUsernamePleaseTryAgain;
        private static string invalidPassword = IA.MM.Resources.Resources.lblInvalidPasswordPleaseTryAgain;
        private static string unRegisteredEmail = IA.MM.Resources.Resources.lblEmailIsNotRegistered;
        public const string CheckEmail = "Please check your email";
        public const string BusinessSystemAlreadyExists = "Business System already exists.";
        public const string RoleAlreadyExists = "Role already exists.";
        public const string ActionGroupAlreadyExists = "Action group already exists.";
        public const string OrgUnitAlreadyExists = "Organizational unit already exists.";
        public const string ScreenAlreadyExists = "Screen already exists.";
        public const string ActionAlreadyExists = "Action already exists.";
        private static string userIsInActive = IA.MM.Resources.Resources.lblAccountBlockedOrInactive;
        public const string SMSSent = "SMS Sent to your mobile no";
        public const string FailedLoginAttemp = "Failed login attempt due to invalid credentials";
        public const string LoginSuccess = "Logged in successfully";
        public const string BusinessSystemAdded = "Business system added successfully";
        public const string ForgotPasswordRequestSent = "Forgot password request sent successfully";
        private static string forgotEmailSent = IA.MM.Resources.Resources.lblForgetPasswordValidEmailMessage;
        private static string UserLoggedOut = IA.MM.Resources.Resources.UserLoggedOut;
        private static string UserNameAlreadyExists = IA.MM.Resources.Resources.UserNameAlreadyExists;
        private static string UserEmailAlreadyExists = IA.MM.Resources.Resources.UserEmailAlreadyExists;
        private static string UserEIDAlreadyExists = IA.MM.Resources.Resources.UserEIDAlreadyExists;
        private static string UserMobileAlreadyExists = IA.MM.Resources.Resources.UserMobileAlreadyExists;
        private static string UserRegister = IA.MM.Resources.Resources.UserRegister;
        private static string UserinfoUpdate = IA.MM.Resources.Resources.UserinfoUpdate;
        public static string userNameAlreadyExists
        {
            get
            {
                return UserNameAlreadyExists;
            }
        }
        public static string userEmailAlreadyExists
        {
            get
            {
                return UserEmailAlreadyExists;
            }
        }
        public static string userEIDAlreadyExists
        {
            get
            {
                return UserEIDAlreadyExists;
            }
        }
        public static string userMobileAlreadyExists
        {
            get
            {
                return UserMobileAlreadyExists;
            }
        }
        public static string userRegister
        {
            get
            {
                return UserRegister;
            }
        }
        public static string userinfoUpdate
        {
            get
            {
                return UserinfoUpdate;
            }
        }
        public static string InvalidCredentials
        {
            get
            {
                return invalidCredentials;
            }
        }
        public static string userLoggedOut
        {
            get
            {
                return UserLoggedOut;
            }
        }


        public static string AccountBlocked
        {
            get
            {
                return accountBlocked;
            }
        }

        public static string UserIsInActive
        {
            get
            {
                return userIsInActive;
            }
        }

        public static string ForgotEmailSent
        {
            get
            {
                return forgotEmailSent;
            }
        }

        public static string UnRegisteredEmail
        {
            get
            {
                return unRegisteredEmail;
            }
        }

        public static string ExpiredLink
        {
            get
            {
                return expiredLink;
            }
        }

        public static string InvalidLink
        {
            get
            {
                return invalidLink;
            }
        }

        public static string OTPInvalid
        {
            get
            {
                return oTPInvalid;
            }
        }

        public static string OTPExpired
        {
            get
            {
                return oTPExpired;
            }
        }

        public static string OTPNotAvailable
        {
            get
            {
                return oTPNotAvailable;
            }
        }

        public static string PasswordChanged
        {
            get
            {
                return passwordChanged;
            }
        }

        public static string OldPasswordIncorrectd
        {
            get
            {
                return oldPasswordIncorrectd;
            }
        }

        public static string OldPasswordCantBeNew
        {
            get
            {
                return oldPasswordCantBeNew;
            }
        }

        public static string InvalidEmail
        {
            get
            {
                return invalidEmail;
            }
        }

        public static string InvaliduserName
        {
            get
            {
                return invaliduserName;
            }
        }

        public static string InvalidPassword
        {
            get
            {
                return invalidPassword;
            }
        }
    }
}
