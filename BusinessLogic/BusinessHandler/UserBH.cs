﻿using DataAccess.CommonRespository;
using DataAccess.Database;
using DataContract.Implementation;
using eInspectionSystemWebApi.Common;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
namespace BusinessLogic.BusinessHandler
{
    public class UserBH
    {
        private readonly ISharedComponentUnitOfWork _uow;
        public UserBH(ISharedComponentUnitOfWork uow)
        {
            _uow = uow;
        }

        /// <summary>
        /// Get User info
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public UserModel GetUserInfo(string UserName, string Password)
        {
            UserModel objUser = new UserModel();
            bool IsAccessible = true;
            int Time = 0;
            int LoginAttempt = 0;
            var dbUserName = _uow.UserRepository.GetUserInfoByUserName(UserName);
            if (dbUserName.UserID > 0)
            {
                if (dbUserName.LoginAttempts != null)
                {
                    LoginAttempt = dbUserName.LoginAttempts.Value;
                    if (dbUserName.LoginAttempts > 4)
                    {
                        IsAccessible = false;
                        if (dbUserName.LastAttempt != null)
                        {
                            TimeSpan varTime = DateTime.Now - dbUserName.LastAttempt.Value;
                            int intMinutes = (int)varTime.TotalMinutes;
                            if (intMinutes > 15)
                            {

                                IsAccessible = true;
                            }
                            else
                            {
                                IsAccessible = false;
                                Time = intMinutes;

                            }

                        }
                    }
                }
                if (IsAccessible)
                {
                    var dbUser = _uow.UserRepository.GetUserInfo(UserName, Password);

                    if (dbUser != null)
                    {
                        if (dbUser.UserID > 0)
                        {
                            if (dbUser.UserStatus.Value)
                            {
                                objUser.UserID = dbUser.UserID;
                                objUser.UserName = Convert.ToString(dbUser.UserName);
                                objUser.UserEmpID = Convert.ToString(dbUser.UserEmpID);
                                objUser.UserEID = Convert.ToString(dbUser.UserEID);
                                objUser.UserEmail = Convert.ToString(dbUser.UserEmail);
                                objUser.UserMobile = Convert.ToString(dbUser.UserMobile);
                                objUser.UserSignature = dbUser.UserSignature;
                                objUser.BusinessSystemID = dbUser.BusinessSystemID;
                                objUser.UserDisplayName = dbUser.UserDisplayName;
                                objUser.UserOUID = dbUser.UserOUID;
                                objUser.UserTypeID = dbUser.UserTypeID;
                                //if(dbUser.UserCompanyID)
                                objUser.UserCompanyID = dbUser.UserCompanyID == null ? 0 : dbUser.UserCompanyID.Value;
                                if (dbUser.OrgUnit != null)
                                {
                                    objUser.OUNameAr = dbUser.OrgUnit.OUNameAr;
                                    objUser.OUNameEn = dbUser.OrgUnit.OUNameEn;
                                }
                                if (dbUser.Designation != null)
                                {
                                    objUser.DesignationEn = dbUser.Designation.DesignationEn;
                                    objUser.DesignationAr = dbUser.Designation.DesignationAr;
                                }
                                if (dbUser.Country != null)
                                {
                                    objUser.CountryNameEn = dbUser.Country.CountryNameAr;
                                    objUser.CountryNameAr = dbUser.Country.CountryNameEn;
                                }
                                if (dbUser.UserRoles != null)
                                {
                                    // objUser.CountryNameEn = dbUser.UserRoles.CountryNameAr;
                                    //  objUser.CountryNameAr = dbUser.Country.CountryNameEn;
                                }
                                if (dbUser.UserType != null)
                                {
                                    objUser.UserTypeEn = dbUser.UserType.UserTypeEn;
                                    objUser.UserTypeAr = dbUser.UserType.UserTypeAr;
                                }

                                if (dbUser.UserProfileImage != null)
                                {

                                    objUser.ProfileImage = dbUser.UserProfileImage;
                                }
                            }
                            else
                            {
                                objUser.UserID = dbUserName.UserID;
                                objUser.LoginAttempt = LoginAttempt;
                                objUser.AccessToken = Convert.ToString(ValidationFlag.AccountInActive);

                            }

                        }
                        else
                        {
                            objUser.UserID = dbUserName.UserID;
                            objUser.LoginAttempt = LoginAttempt;
                            objUser.AccessToken = Convert.ToString(ValidationFlag.InvalidPassworde);
                        }



                    }
                }

                else
                {
                    objUser.UserID = dbUserName.UserID;
                    objUser.AccessToken = Convert.ToString(ValidationFlag.AccountBlocked);
                    objUser.LoginAttempt = LoginAttempt;
                }

            }
            else
            {
                objUser.AccessToken = Convert.ToString(ValidationFlag.InvalidUserName);
            }

            return objUser;
        }


        /// <summary>
        /// Get User Roles and Permissions
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public RolesAndPermissionsModel GetUserRolesAndPermissions(long UserID)
        {
            RolesAndPermissionsModel objRolesAndPermissionsModel = new RolesAndPermissionsModel();
            var dbUser = _uow.UserRepository.GetByID(UserID);
            if (dbUser != null)
            {
                if (dbUser.UserID > 0)
                {
                    objRolesAndPermissionsModel.UserID = dbUser.UserID;
                    objRolesAndPermissionsModel.UserName = dbUser.UserName;
                    objRolesAndPermissionsModel.BusinessSystemID = dbUser.BusinessSystem.BusinessSystemID;
                    objRolesAndPermissionsModel.BusinessSystemNameAr = dbUser.BusinessSystem.BusinessSystemNameAr;
                    objRolesAndPermissionsModel.BusinessSystemNameEn = dbUser.BusinessSystem.BusinessSystemNameEn;

                    objRolesAndPermissionsModel.UserRoles = new List<RoleModel>();

                    var dbScreens = _uow.UserRepository.GetScreensList(dbUser.BusinessSystemID.Value);
                    if (dbUser.UserRoles != null)
                    {
                        foreach (var itemRole in dbUser.UserRoles)
                        {
                            if (itemRole.Role != null)
                            {
                                var objUserRoleModel = new RoleModel();
                                objUserRoleModel.RoleID = itemRole.RoleID;
                                objUserRoleModel.RoleNameAr = itemRole.Role.RoleNameAr;
                                objUserRoleModel.RoleNameEn = itemRole.Role.RoleNameEn;

                                objUserRoleModel.RolePermissions = new List<RolePermissionModel>();

                                foreach (var itemScreen in dbScreens)
                                {
                                    var objRolePermissionModel = new RolePermissionModel();

                                    objRolePermissionModel.ScreenID = itemScreen.ScreenID;
                                    objRolePermissionModel.ScreenNameEn = itemScreen.ScreenNameEn;
                                    objRolePermissionModel.ScreenNameAr = itemScreen.ScreenNameAr;

                                    objRolePermissionModel.Actions = new List<ScreenActionsModel>();

                                    foreach (var itemScreenAction in itemScreen.ScreenActions.Where(x => !dbUser.UserPermissions.Any(y => y.ActionID == x.ActionID)))
                                    {
                                        var objScreenActionsModel = new ScreenActionsModel();

                                        objScreenActionsModel.ActionID = itemScreenAction.ActionID;
                                        objScreenActionsModel.ActionName = itemScreenAction.ActionName;
                                        objScreenActionsModel.ActionCommunicationMethod = itemScreenAction.ActionCommunicationMethod;
                                        objScreenActionsModel.ActionDesc = itemScreenAction.ActionDesc;
                                        objRolePermissionModel.Actions.Add(objScreenActionsModel);

                                    }

                                    objUserRoleModel.RolePermissions.Add(objRolePermissionModel);
                                }


                                objRolesAndPermissionsModel.UserRoles.Add(objUserRoleModel);
                            }
                        }

                    }



                }
            }

            return objRolesAndPermissionsModel;
        }

        /// <summary>
        /// GetUserInfoByEmail
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public UserModel GetUserInfoByEmail(string Email)
        {
            UserModel objUser = new UserModel();
            var dbUser = _uow.UserRepository.GetUserInfoByEmail(Email);
            if (dbUser != null)
            {
                if (dbUser.UserID > 0)
                {
                    objUser.UserID = dbUser.UserID;
                    objUser.UserName = Convert.ToString(dbUser.UserName);
                    objUser.BusinessSystemID = dbUser.BusinessSystemID;
                    objUser.UserEmail = Convert.ToString(dbUser.UserEmail);
                    objUser.UserDisplayName = Convert.ToString(dbUser.UserDisplayName);
                    objUser.UserCompanyID = dbUser.UserCompanyID == null ? 0 : dbUser.UserCompanyID.Value;
                    objUser.IsClicked = dbUser.IsClicked != null ? dbUser.IsClicked.Value : false;
                    if (dbUser.LinkExpiry != null)
                    {
                        objUser.LinkExp = dbUser.LinkExpiry.Value;
                    }
                }
            }

            return objUser;
        }

        /// <summary>
        /// GetUserInfoByEmail
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public UserModel GetInfoByID(long UserID)
        {
            UserModel objUser = new UserModel();
            var dbUser = _uow.UserRepository.GetByID(UserID);
            if (dbUser != null)
            {
                if (dbUser.UserID > 0)
                {
                    objUser.UserID = dbUser.UserID;
                    objUser.UserName = Convert.ToString(dbUser.UserName);
                    objUser.UserEmpID = Convert.ToString(dbUser.UserEmpID);
                    objUser.UserEID = Convert.ToString(dbUser.UserEID);
                    objUser.UserEmail = Convert.ToString(dbUser.UserEmail);
                    objUser.UserMobile = Convert.ToString(dbUser.UserMobile);
                    objUser.UserSignature = dbUser.UserSignature;
                    objUser.BusinessSystemID = dbUser.BusinessSystemID;
                    objUser.UserDisplayName = dbUser.UserDisplayName;
                    objUser.Password = dbUser.UserPassword;
                    objUser.UserOUID = dbUser.UserOUID;
                    objUser.UserStatus = dbUser.UserStatus;
                    objUser.UserDeviceUDID = dbUser.UserDeviceUDID;
                    objUser.LoginAttempt = dbUser.LoginAttempts == null ? 0 : dbUser.LoginAttempts.Value;
                    objUser.UserTypeID = dbUser.UserTypeID;
                    objUser.ActionGroupID = dbUser.ActionGroupID == null ? 0 : dbUser.ActionGroupID.Value;
                    objUser.UserCompanyID = dbUser.UserCompanyID == null ? 0 : dbUser.UserCompanyID.Value;
                    objUser.CreationDate = dbUser.UserCreationDate == null ? "" : dbUser.UserCreationDate.Value.ToString("dd/MMM/yyyy");
                    objUser.SuspensionDate = dbUser.LastAttempt == null ? "" : dbUser.LastAttempt.Value.ToString("dd/MMM/yyyy");
                    objUser.ProfileImage = dbUser.UserProfileImage;
                    if (dbUser.OrgUnit != null)
                    {
                        objUser.OUNameAr = dbUser.OrgUnit.OUNameAr;
                        objUser.OUNameEn = dbUser.OrgUnit.OUNameEn;

                    }
                    if (dbUser.Designation != null)
                    {
                        objUser.DesignationEn = dbUser.Designation.DesignationEn;
                        objUser.DesignationAr = dbUser.Designation.DesignationAr;
                        objUser.UserDesignationID = dbUser.Designation.DesignationID;
                    }
                    if (dbUser.BusinessSystem != null)
                    {
                        objUser.BusinessSystemNameEn = dbUser.BusinessSystem.BusinessSystemNameEn;
                        objUser.BusinessSystemNameAr = dbUser.BusinessSystem.BusinessSystemNameAr;
                    }
                    if (dbUser.Country != null)
                    {
                        objUser.CountryNameEn = dbUser.Country.CountryNameAr;
                        objUser.CountryNameAr = dbUser.Country.CountryNameEn;
                        objUser.CountryID = dbUser.Country.CountryID;

                    }
                    if (dbUser.UserRoles != null)
                    {
                        // objUser.CountryNameEn = dbUser.UserRoles.CountryNameAr;
                        //  objUser.CountryNameAr = dbUser.Country.CountryNameEn;
                    }
                    if (dbUser.UserType != null)
                    {
                        objUser.UserTypeEn = dbUser.UserType.UserTypeEn;
                        objUser.UserTypeAr = dbUser.UserType.UserTypeAr;
                    }
                    var Roles = _uow.SharedComponentRepository.GetRolesListByUserID(dbUser.UserID);
                    objUser.RoleIDs = new List<int>();
                    List<int> obj = new List<int>();
                    foreach (var item in Roles)
                    {
                        obj.Add(Convert.ToInt32(item.RoleID));
                    }
                    objUser.RoleIDs = obj;
                }
            }

            return objUser;
        }

        /// <summary>
        /// VerifyUserByID
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public bool VerifyUserByID(long UserID)
        {
            bool IsValid = false;
            var dbUser = _uow.UserRepository.GetByID(UserID);
            if (dbUser != null)
            {
                if (dbUser.UserID > 0)
                {
                    IsValid = true;
                }
            }
            else
            {
                IsValid = false;
            }

            return IsValid;
        }

        /// <summary>
        /// GetUserExpiryByEmail
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public string GetUserExpiryByEmail(string Email)
        {
            var dbUser = _uow.UserRepository.GetUserInfoByEmail(Email);
            if (dbUser != null)
            {
                if (dbUser.UserID > 0)
                {
                    return Convert.ToString(dbUser.LinkExpiry);
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// GetUsersListByBusinessSystemID
        /// </summary>
        /// <param name="BusinessSystemID"></param>
        /// <param name="IsForAdmin"></param>
        /// <returns></returns>
        public List<UserModel> GetUsersListByBusinessSystemID(long BusinessSystemID, bool IsForAdmin = false)
        {
            List<UserModel> objUserList = new List<UserModel>();
            var dbUser = _uow.UserRepository.GetUsersListByBusinessSystemID(BusinessSystemID, IsForAdmin);
            foreach (var item in dbUser)
            {

                if (item.UserID > 0)
                {
                    var objUser = new UserModel();
                    objUser.UserID = item.UserID;
                    objUser.UserName = Convert.ToString(item.UserName);
                    objUser.UserEmpID = Convert.ToString(item.UserEmpID);
                    objUser.UserEID = Convert.ToString(item.UserEID);
                    objUser.UserEmail = Convert.ToString(item.UserEmail);
                    objUser.UserMobile = Convert.ToString(item.UserMobile);
                    objUser.UserSignature = item.UserSignature;
                    objUser.UserDisplayName = item.UserDisplayName;
                    objUser.UserStatus = item.UserStatus;
                    objUser.UserCompanyID = item.UserCompanyID == null ? 0 : item.UserCompanyID.Value;
                    objUser.UserTypeID = item.UserTypeID != null ? item.UserTypeID.Value : 0;
                    if (item.UserStatus.Value)
                    {
                        objUser.UserDispalyStatus = "Active";
                    }
                    else
                    {
                        objUser.UserDispalyStatus = "InActive";
                    }
                    if (item.Designation != null)
                    {
                        objUser.DesignationEn = item.Designation.DesignationEn;
                        objUser.DesignationAr = item.Designation.DesignationAr;
                    }
                    if (item.OrgUnit != null)
                    {
                        objUser.OUNameEn = item.OrgUnit.OUNameEn;
                        objUser.OUNameAr = item.OrgUnit.OUNameAr;
                    }
                    if (item.Country != null)
                    {
                        objUser.CountryNameEn = item.Country.CountryNameAr;
                        objUser.CountryNameAr = item.Country.CountryNameEn;
                    }
                    if (item.BusinessSystem != null)
                    {
                        objUser.BusinessSystemNameEn = item.BusinessSystem.BusinessSystemNameEn;
                        objUser.BusinessSystemNameAr = item.BusinessSystem.BusinessSystemNameAr;
                    }
                    if (item.UserType != null)
                    {
                        objUser.UserTypeEn = item.UserType.UserTypeEn;
                        objUser.UserTypeAr = item.UserType.UserTypeAr;
                    }
                    objUserList.Add(objUser);
                }

            }

            return objUserList;
        }
        /// <summary>
        /// GetUserSuperviserByActionGroupsAndUsertypeID
        /// </summary>
        /// <param name="objGetUserParamModel"></param>
        /// <returns></returns>
        public List<UserModel> GetUserSuperviserByRolesAndUsertypeID(GetUserParamModel objGetUserParamModel)
        {
            List<UserModel> objUserList = new List<UserModel>();
            foreach (var RoleID in objGetUserParamModel.RoleID)
            {
                var dbUser = _uow.UserRepository.GetUsersByRoleID(RoleID);
                foreach (var item in dbUser)
                {
                    if (item.UserID > 0)
                    {
                        var objUser = new UserModel();
                        objUser.UserID = item.UserID;
                        if (item.User != null)
                        {
                            if (item.User.UserStatus.Value)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(objGetUserParamModel.UserTypeID)))
                                {
                                    if (item.User.UserTypeID == objGetUserParamModel.UserTypeID)
                                    {
                                        objUser.UserName = Convert.ToString(item.User.UserName);
                                        objUser.UserEmpID = Convert.ToString(item.User.UserEmpID);
                                        objUser.UserEID = Convert.ToString(item.User.UserEID);
                                        objUser.UserEmail = Convert.ToString(item.User.UserEmail);
                                        objUser.UserMobile = Convert.ToString(item.User.UserMobile);
                                        objUser.UserDisplayName = item.User.UserDisplayName;
                                        if (item.User.Designation != null)
                                        {
                                            objUser.DesignationEn = item.User.Designation.DesignationEn;
                                            objUser.DesignationAr = item.User.Designation.DesignationAr;

                                        }
                                        objUserList.Add(objUser);
                                    }
                                }
                            }
                        }
                    }

                }
            }

            return objUserList;
        }
        /// <summary>
        /// GetUserSuperviserByActionGroupID
        /// </summary>
        /// <param name="ActionGroupID"></param>
        /// <returns></returns>
        public List<UserModel> GetUsersByRoleID(long RoleID)
        {
            List<UserModel> objUserList = new List<UserModel>();
            var dbUser = _uow.UserRepository.GetUsersByRoleID(RoleID);
            foreach (var item in dbUser)
            {
                if (item.UserID > 0)
                {
                    var objUser = new UserModel();
                    objUser.UserID = item.UserID;
                    if (item.User != null)
                    {
                        if (item.User.UserStatus.Value)
                        {
                            objUser.UserName = Convert.ToString(item.User.UserName);
                            objUser.UserEmpID = Convert.ToString(item.User.UserEmpID);
                            objUser.UserEID = Convert.ToString(item.User.UserEID);
                            objUser.UserEmail = Convert.ToString(item.User.UserEmail);
                            objUser.UserMobile = Convert.ToString(item.User.UserMobile);
                            objUser.UserDisplayName = item.User.UserDisplayName;
                            if (item.User.Designation != null)
                            {
                                objUser.DesignationEn = item.User.Designation.DesignationEn;
                                objUser.DesignationAr = item.User.Designation.DesignationAr;

                            }
                            objUserList.Add(objUser);
                        }
                    }
                }

            }

            return objUserList;
        }
        /// <summary>
        /// GetUsersByCompanyID
        /// </summary>
        /// <param name="CompanyID"></param>
        /// <returns></returns>
        public List<UserBasicModel> GetUsersByCompanyID(long CompanyID)
        {
            List<UserBasicModel> objUserList = new List<UserBasicModel>();
            var dbUser = _uow.UserRepository.GetUsersByCompanyID(CompanyID);
            foreach (var item in dbUser)
            {
                
                    var objUser = new UserBasicModel();
                    objUser.UserID = item.UserID;
                    objUser.UserName = Convert.ToString(item.UserName);
                    objUser.UserEmail = Convert.ToString(item.UserEmail);
                    objUser.UserMobile = Convert.ToString(item.UserMobile);
                    objUser.UserDisplayName = item.UserDisplayName;
                    if (item.Designation != null)
                    {
                        objUser.DesignationEn = item.Designation.DesignationEn;
                        objUser.DesignationAr = item.Designation.DesignationAr;

                    }
                    objUserList.Add(objUser);
            }

            return objUserList;
        }

        /// <summary>
        /// GetUsersByRolesID
        /// </summary>
        /// <param name="objGetUserParamMode"></param>
        /// <returns></returns>
        public List<UserModel> GetUsersByRolesID(GetUserParamModel objGetUserParamMode)
        {
            List<UserModel> objUserList = new List<UserModel>();
            foreach (var RolesID in objGetUserParamMode.RoleID)
            {
                var dbUser = _uow.UserRepository.GetUsersByRoleID(RolesID);
                foreach (var item in dbUser)
                {
                    if (item.UserID > 0)
                    {
                        var objUser = new UserModel();
                        objUser.UserID = item.UserID;
                        if (item.User != null)
                        {
                            if (item.User.UserStatus.Value)
                            {
                                objUser.UserName = Convert.ToString(item.User.UserName);
                                objUser.UserEmpID = Convert.ToString(item.User.UserEmpID);
                                objUser.UserEID = Convert.ToString(item.User.UserEID);
                                objUser.UserEmail = Convert.ToString(item.User.UserEmail);
                                objUser.UserMobile = Convert.ToString(item.User.UserMobile);
                                objUser.UserDisplayName = item.User.UserDisplayName;
                                if (item.User.Designation != null)
                                {
                                    objUser.DesignationEn = item.User.Designation.DesignationEn;
                                    objUser.DesignationAr = item.User.Designation.DesignationAr;

                                }
                                objUserList.Add(objUser);
                            }
                        }
                    }

                }
            }
            return objUserList;
        }
        /// <summary>
        /// UpdateUserStatus
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public bool UpdateUserStatus(long UserID, bool Status)
        {
            bool Isok = false;
            var obj = _uow.UserRepository.GetByID(UserID);
            int result = 0;
            if (obj == null)
                throw new Exception("object not found with ID = " + UserID);
            try
            {
                obj.UserStatus = Status;
                result = _uow.Save();
                if (result > 0)
                {
                    Isok = true;
                }

            }
            catch (Exception ex)
            {
                Isok = false;
            }
            return Isok;
        }

        /// <summary>
        /// UpdateLinkExpiryTime
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="dtexpiry"></param>
        /// <returns></returns>
        public bool UpdateLinkExpiryTime(long UserID, DateTime dtexpiry)
        {
            bool Isok = false;
            var obj = _uow.UserRepository.GetByID(UserID);
            int result = 0;
            if (obj == null)
                throw new Exception("object not found with ID = " + UserID);
            try
            {

                obj.LinkExpiry = dtexpiry;
                result = _uow.Save();
                if (result > 0)
                {
                    Isok = true;
                }

            }
            catch (Exception ex)
            {
                Isok = false;
            }
            return Isok;
        }

        /// <summary>
        /// UpdateUserProfile
        /// </summary>
        /// <param name="UserModel"></param>
        /// <returns></returns>
        public MainUserModel UpdateUserProfile(MainUserModel UserModel)
        {
            MainUserModel objUserModel = new MainUserModel();
            var objUser = _uow.UserRepository.GetByID(UserModel.UserID);
            int result = 0;

            if (objUser == null)
                throw new Exception("object not found with ID = " + UserModel.UserID);
            try
            {
                if (UserModel.BusinessSystemID != null)
                {
                    objUser.BusinessSystemID = UserModel.BusinessSystemID;
                }
                if (UserModel.UserCompanyID != null)
                {
                    objUser.UserCompanyID = UserModel.UserCompanyID;
                }
                if (UserModel.UserDesignationID != null)
                {
                    objUser.UserCompanyID = UserModel.UserCompanyID;
                }
                if (UserModel.UserDeviceUDID != null)
                {
                    objUser.UserDeviceUDID = UserModel.UserDeviceUDID;
                }
                if (UserModel.UserEID != null)
                {
                    objUser.UserEID = UserModel.UserEID;
                }
                if (UserModel.UserEmail != null)
                {
                    objUser.UserEmail = UserModel.UserEmail;
                }
                if (UserModel.UserEmpID != null)
                {
                    objUser.UserEmpID = UserModel.UserEmpID;
                }
                if (UserModel.UserMobile != null)
                {
                    objUser.UserMobile = UserModel.UserMobile;
                }

                if (UserModel.UserName != null)
                {
                    objUser.UserName = UserModel.UserName;
                }
                if (UserModel.UserNationalityID != null)
                {
                    objUser.UserNationalityID = UserModel.UserNationalityID;
                }
                if (UserModel.UserOUID != null)
                {
                    objUser.UserOUID = UserModel.UserOUID;
                }
                if (UserModel.UserSignature != null)
                {
                    objUser.UserSignature = UserModel.UserSignature;
                }
                if (UserModel.UserTypeID != null)
                {
                    objUser.UserTypeID = UserModel.UserTypeID;
                }
                if (UserModel.DisplayName != null)
                {
                    objUser.UserDisplayName = UserModel.DisplayName;
                }

                objUser.UserProfileImage = UserModel.ProfileImage;
                result = _uow.Save();

                objUserModel = UserModel;



            }
            catch (Exception ex)
            {
            }
            return objUserModel;
        }

        /// <summary>
        /// RegisterUser
        /// </summary>
        /// <param name="UserModel"></param>
        /// <returns></returns>
        public MainUserModel RegisterUser(MainUserModel UserModel)
        {
            MainUserModel objUserModel = new MainUserModel();
            var objUser = new User();
            if (UserModel.UserID > 0)
            {
                objUser = _uow.UserRepository.GetByID(UserModel.UserID);
            }

            int result = 0;

            try
            {
                if (UserModel.BusinessSystemID != null)
                {
                    objUser.BusinessSystemID = UserModel.BusinessSystemID;
                }
                if (UserModel.UserCompanyID != null)
                {
                    objUser.UserCompanyID = UserModel.UserCompanyID;
                }
                if (UserModel.UserDesignationID != null)
                {
                    objUser.UserDesignationID = UserModel.UserDesignationID;
                }
                if (UserModel.UserDeviceUDID != null)
                {
                    objUser.UserDeviceUDID = UserModel.UserDeviceUDID;
                }
                if (UserModel.UserEID != null)
                {
                    objUser.UserEID = UserModel.UserEID;
                }
                if (UserModel.UserEmail != null)
                {
                    objUser.UserEmail = UserModel.UserEmail;
                }
                if (UserModel.UserEmpID != null)
                {
                    objUser.UserEmpID = UserModel.UserEmpID;
                }
                if (UserModel.UserTypeID != null)
                {
                    objUser.UserTypeID = UserModel.UserTypeID;
                }
                if (UserModel.UserMobile != null)
                {
                    objUser.UserMobile = UserModel.UserMobile;
                }

                if (UserModel.UserName != null)
                {
                    objUser.UserName = UserModel.UserName;
                }
                if (UserModel.UserNationalityID != null)
                {
                    objUser.UserNationalityID = UserModel.UserNationalityID;
                }
                if (UserModel.UserDesignationID != null)
                {
                    objUser.UserDesignationID = UserModel.UserDesignationID;
                }
                if (UserModel.UserOUID != null)
                {
                    objUser.UserOUID = UserModel.UserOUID;
                }
                if (UserModel.UserSignature != null)
                {
                    objUser.UserSignature = UserModel.UserSignature;
                }

                if (UserModel.DisplayName != null)
                {
                    objUser.UserDisplayName = UserModel.DisplayName;
                }
                if (UserModel.UserCompanyID != null)
                {
                    objUser.UserCompanyID = UserModel.UserCompanyID;
                }
                if (UserModel.ActionGroupID != null)
                {
                    objUser.ActionGroupID = UserModel.ActionGroupID;
                }
                objUser.UserPassword = UserModel.Password;
                objUser.UserCreationDate = DateTime.Now;
                objUser.UserStatus = UserModel.UserStatus;

                if (objUser.UserID > 0)
                {
                    //_uow.UserRepository.Edit(objUser);
                }
                else
                {
                    _uow.UserRepository.Add(objUser);
                }

                if (UserModel.UserName != null)
                {
                    result = _uow.Save();
                }
                if (result > 0)
                {
                    UserModel.Password = string.Empty;
                    objUserModel = UserModel;
                }

            }
            catch (Exception ex)
            {
            }

            return objUserModel;
        }

        /// <summary>
        /// RegisterPublicUser
        /// </summary>
        /// <param name="UserModel"></param>
        /// <returns></returns>
        public bool RegisterPublicUser(PublicUserModel UserModel)
        {
            var objUser = new UserSubscriptionRequest();
            bool IsOk = false;
            try
            {

                objUser.BusinessSystemID = null;

                if (UserModel.UserEID != null)
                {
                    objUser.UserEID = UserModel.UserEID;
                }
                if (UserModel.UserEmail != null)
                {
                    objUser.UserEmail = UserModel.UserEmail;
                }

                if (UserModel.UserMobile != null)
                {
                    objUser.UserMobile = UserModel.UserMobile;
                }

                if (UserModel.UserName != null)
                {
                    objUser.UserName = UserModel.UserName;
                }

                if (UserModel.UserNationalityID != null)
                {
                    objUser.UserNationalityID = UserModel.UserNationalityID;
                }

                if (UserModel.UserDisplayName != null)
                {
                    objUser.UserDisplayName = UserModel.UserDisplayName;
                }

                objUser.RequestStatusID = Convert.ToInt64(RegisterUserTypes.Public);
                objUser.UserTypeID = Convert.ToInt64(RegisterUserTypes.Public);

                objUser.UserPassword = UserModel.UserPassword;
                objUser.RequestDate = DateTime.Now;
                objUser.UserStatus = true;
                IsOk = _uow.UserRepository.RegisterPublicUser(objUser);



            }
            catch (Exception ex)
            {
                IsOk = false;
            }

            return IsOk;
        }

        /// <summary>
        /// VerifyDuplicateUser
        /// </summary>
        /// <param name="UserNameAr"></param>
        /// <param name="UserNameEn"></param>
        /// <param name="Useremail"></param>
        /// <param name="Usermobile"></param>
        /// <param name="EmiratesId"></param>
        /// <returns></returns>
        public User VerifyDuplicateUserToAdd(string UserNameEn, string Useremail, string Usermobile, string EmiratesId)
        {
            return _uow.UserRepository.VerifyDuplicateUser(UserNameEn, Useremail, Usermobile, EmiratesId);

        }

        /// <summary>
        /// VerifyPublicDuplicateUser
        /// </summary>
        /// <param name="UserNameEn"></param>
        /// <param name="Useremail"></param>
        /// <param name="Usermobile"></param>
        /// <param name="EmiratesId"></param>
        /// <returns></returns>

        public UserSubscriptionRequest VerifyPublicDuplicateUser(string UserNameEn, string Useremail, string Usermobile, string EmiratesId)
        {
            return _uow.UserRepository.VerifyPublicDuplicateUser(UserNameEn, Useremail, Usermobile, EmiratesId);

        }

        /// <summary>
        /// VerifyDuplicateUserToUpdate
        /// </summary>
        /// <param name="UserNameEn"></param>
        /// <param name="Useremail"></param>
        /// <param name="Usermobile"></param>
        /// <param name="EmiratesId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public User VerifyDuplicateUserToUpdate(string UserNameEn, string Useremail, string Usermobile, string EmiratesId, long UserId)
        {
            return _uow.UserRepository.VerifyDuplicateUserToUpdate(UserNameEn, Useremail, Usermobile, EmiratesId, UserId);

        }

        /// <summary>
        /// UpdateLoginAttemptsFailure
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public bool UpdateLoginAttemptsFailure(long UserID)
        {
            bool Isok = false;
            var obj = _uow.UserRepository.GetByID(UserID);
            int result = 0;
            if (obj == null)
                throw new Exception("object not found with ID = " + UserID);
            try
            {
                if (obj.LoginAttempts != null)
                {
                    obj.LoginAttempts = obj.LoginAttempts + 1;
                }
                else
                {
                    obj.LoginAttempts = 1;
                }

                obj.LastAttempt = DateTime.Now;
                result = _uow.Save();
                if (result > 0)
                {
                    Isok = true;
                }

            }
            catch (Exception ex)
            {
                Isok = false;
            }
            return Isok;
        }

        /// <summary>
        /// UpdateLoginAttemptsSuccess
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public bool UpdateLoginAttemptsSuccess(long UserID)
        {
            bool Isok = false;
            var obj = _uow.UserRepository.GetByID(UserID);
            int result = 0;
            if (obj == null)
                throw new Exception("object not found with ID = " + UserID);
            try
            {
                obj.LoginAttempts = 0;
                obj.LastAttempt = DateTime.Now;
                result = _uow.Save();
                if (result > 0)
                {
                    Isok = true;
                }

            }
            catch (Exception ex)
            {
                Isok = false;
            }
            return Isok;
        }

        /// <summary>
        /// UpdatePassword
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="UserPassword"></param>
        /// <returns></returns>
        public bool UpdatePassword(long UserID, string UserPassword)
        {
            bool Isok = false;
            var obj = _uow.UserRepository.GetByID(UserID);
            int result = 0;
            if (obj == null)
                throw new Exception("object not found with ID = " + UserID);
            try
            {
                obj.UserPassword = UserPassword;
                obj.LinkExpiry = DateTime.Now.AddHours(-48);
                result = _uow.Save();
                if (result > 0)
                {
                    Isok = true;
                }

            }
            catch (Exception ex)
            {
                Isok = false;
            }
            return Isok;
        }

        /// <summary>
        /// SendOTP
        /// </summary>
        /// <param name="Connection"></param>
        /// <param name="UserID"></param>
        /// <param name="OTP"></param>
        /// <returns></returns>
        public bool SendOTP(string Connection, long UserID, string OTP)
        {
            bool IsDone = false;
            var obj = _uow.UserRepository.GetByID(UserID);
            if (obj != null)
            {
                string connetionString = null;
                SqlConnection cnn;
                SqlDataAdapter adapter = new SqlDataAdapter();
                string sql = null;
                connetionString = Connection;

                cnn = new SqlConnection(connetionString);
                sql = "INSERT INTO [dbo].[SMS]([UID],[MobileNo],[Name],[SMSText],[SMSStatus],[final_date],[msg_serial],[Lang],[State],[dateofsubmit],[AdminName],[Emirate],[URL],[provider])";
                sql += "VALUES('" + obj.UserEmail + "', '" + obj.UserMobile + "', NULL, '" + OTP + "', NULL, NULL, NULL , 'en-US', 'FORGET PASSWORD', GETDATE(), NULL, NULL, NULL,NULL )";

                try
                {
                    cnn.Open();
                    adapter.InsertCommand = new SqlCommand(sql, cnn);
                    adapter.InsertCommand.ExecuteNonQuery();
                    IsDone = true;
                }
                catch (Exception ex)
                {
                    IsDone = false;

                }
            }
            return IsDone;
        }

        public bool SendMessage(string Connection, string OTP, string UserEmail, string UserMobile, string TemplateType)
        {
            bool IsDone = false;
            string connetionString = null;
            SqlConnection cnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            connetionString = Connection;

            cnn = new SqlConnection(connetionString);
            sql = "INSERT INTO [dbo].[SMS]([UID],[MobileNo],[Name],[SMSText],[SMSStatus],[final_date],[msg_serial],[Lang],[State],[dateofsubmit],[AdminName],[Emirate],[URL],[provider])";
            sql += "VALUES('" + UserEmail + "', '" + UserMobile + "', NULL, '" + OTP + "', NULL, NULL, NULL , 'en-US', '" + TemplateType + "', GETDATE(), NULL, NULL, NULL,NULL )";

            try
            {
                cnn.Open();
                adapter.InsertCommand = new SqlCommand(sql, cnn);
                adapter.InsertCommand.ExecuteNonQuery();
                IsDone = true;
            }
            catch (Exception ex)
            {
                IsDone = false;

            }

            return IsDone;
        }

        /// <summary>
        /// GetScreensList
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public List<RolePermissionModel> GetScreensList(long UserID)
        {
            List<RolePermissionModel> objRolePermissionModel = new List<RolePermissionModel>();
            var dbScreens = _uow.UserRepository.GetScreensListByUserID(UserID);
            foreach (var itemScreens in dbScreens)
            {
                var objModel = new RolePermissionModel();
                foreach (var itemScreensRoles in dbScreens.Where(x => x.RoleID == itemScreens.RoleID))
                {

                    objModel.ScreenID = itemScreens.ScreenID;
                    objModel.ScreenNameEn = itemScreens.ScreenNameEn;
                    objModel.ScreenNameAr = itemScreens.ScreenNameAr;
                    objModel.RoleID = itemScreens.RoleID;
                    objModel.Actions = new List<ScreenActionsModel>();
                    foreach (var itemScreensActions in dbScreens.Where(x => x.ScreenID == itemScreens.ScreenID))
                    {
                        var obj = new ScreenActionsModel();
                        obj.ActionID = itemScreensActions.ActionID;
                        obj.ActionName = itemScreensActions.ActionName;
                        obj.ActionCommunicationMethod = itemScreensActions.ActionCommunicationMethod;
                        obj.ActionDesc = itemScreensActions.ActionDesc;
                        obj.ActionTypeID = itemScreensActions.ActionTypeID;
                        objModel.Actions.Add(obj);
                    }

                }
                objRolePermissionModel.Add(objModel);

            }

            return objRolePermissionModel.ToList();
        }

        /// <summary>
        /// UpdateUserTask
        /// </summary>
        /// <param name="objUserTasksModel"></param>
        /// <returns></returns>
        public bool UpdateUserTask(UserTasksInputModel objUserTasksModel)
        {

            UserTask objUserTask = new UserTask();
            objUserTask.OriginSystemID = objUserTasksModel.OriginSystemID;
            objUserTask.TaskDate = DateTime.Now;
            objUserTask.TaskDescription = objUserTasksModel.TaskDescription;
            objUserTask.TaskRecepientList = objUserTasksModel.TaskRecepientList;
            objUserTask.TaskStatus = objUserTasksModel.TaskStatus;
            objUserTask.TaskURL = objUserTasksModel.TaskURL;

            return _uow.UserRepository.UpdateUserTask(objUserTask);



        }
        /// <summary>
        /// UpdateLinkStatus
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public bool UpdateLinkStatus(long UserID, bool Status)
        {
            return _uow.UserRepository.UpdateLinkStatus(UserID, Status);
        }
        /// <summary>
        /// GetUsersType
        /// </summary>
        /// <returns></returns>
        public List<UserType> GetUsersType()
        {
            return _uow.UserRepository.GetUsersType();
        }

    }
}