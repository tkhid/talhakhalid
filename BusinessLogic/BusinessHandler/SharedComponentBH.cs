﻿using Common;
using DataAccess.CommonRespository;
using DataAccess.Database;
using DataContract.Implementation;
using eInspectionSystemWebApi.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.BusinessHandler
{
    public class SharedComponentBH
    {
        private readonly ISharedComponentUnitOfWork _uow;
        string message = "";
        public SharedComponentBH(ISharedComponentUnitOfWork uow)
        {
            _uow = uow;
        }

        #region Common

        public List<DesignationModel> GetDesignations()
        {
            List<DesignationModel> objDesignationModel = new List<DesignationModel>();
            try
            {
                var dbDesignations = _uow.SharedComponentRepository.GetDesignations();
                foreach (var item in dbDesignations)
                {
                    var obj = new DesignationModel();
                    obj.DesignationID = item.DesignationID;
                    obj.DesignationEn = item.DesignationEn;
                    obj.DesignationAr = item.DesignationAr;
                    objDesignationModel.Add(obj);
                }
            }
            catch (Exception ex)
            {
            }
            return objDesignationModel;

        }
        
        public List<CountryModel> GetCountriesList()
        {
            List<CountryModel> objCountryModel = new List<CountryModel>();
            try
            {
                var dbCountry = _uow.SharedComponentRepository.GetCountryList();
                foreach (var item in dbCountry)
                {
                    var obj = new CountryModel();
                    obj.CountryID = item.CountryID;
                    obj.CountryNameEn = item.CountryNameEn;
                    obj.CountryNameAr = item.CountryNameAr;
                    objCountryModel.Add(obj);
                }
            }
            catch (Exception ex)
            {
            }
            return objCountryModel;

        }
        public List<CityModel> GetCitiesList(long CountryID)
        {
            List<CityModel> objCityModel = new List<CityModel>();
            try
            {
                var dbCity = _uow.SharedComponentRepository.GetCitiesList(CountryID);
                foreach (var item in dbCity)
                {
                    var obj = new CityModel();
                    obj.CityID = item.CityID;
                    obj.CityNameAr = item.CityNameAr;
                    obj.CityNameEn = item.CityNameEn;
                    objCityModel.Add(obj);
                }
            }
            catch (Exception ex)
            {
            }
            return objCityModel;

        }
         
        public List<EmailNotification> SendNoticationEmailService()
        {
            var Result = _uow.SharedComponentRepository.GetNotificationsListToEmail();
            List<EmailNotification> objList = new List<EmailNotification>();
            foreach (var item in Result)
            {
                var obj = new EmailNotification();
                obj.ActionCommunicationMethod = item.ActionCommunicationMethod;
                obj.EmailBody = item.EmailBody;
                obj.EmailLanguageID = item.EmailLanguageID;
                obj.EmailStatusID = item.EmailStatusID;
                obj.EmailSubject = item.EmailSubject;
                obj.IsUrgent = item.IsUrgent;
                obj.NotificationID = item.NotificationID;
                obj.RecepientEmail = item.RecepientEmail;
                obj.UserID = item.UserID;
                objList.Add(obj);
            }
            return objList;

        }
        public bool UpdateEmailNotificationStatus(long NotificationID, long StatusID)
        {
            bool IsOk = false;
            if (StatusID == Convert.ToInt64(EmailStatus.Failed))
            {
                IsOk = _uow.SharedComponentRepository.UpdateEmailNotificationStatus(NotificationID, StatusID);
            }
            if (StatusID == Convert.ToInt64(EmailStatus.Sent))
            {
                IsOk = _uow.SharedComponentRepository.UpdateEmailNotificationStatus(NotificationID, StatusID);
            }

            return IsOk;

        }
       
        public void SaveAuditTrail(long BusinessSystemID, long ActionTypeID, long ActionUserID, string ActionComponentID, string ActionMachineName, string ActionDescription)
        {
            try
            {

                SharedAuditTrail objTrail = new SharedAuditTrail();

                objTrail.ActionComponentID = ActionComponentID;
                objTrail.ActionDescription = ActionDescription;
                objTrail.ActionMachineName = ActionMachineName;
                objTrail.ActionTime = DateTime.Now;
                objTrail.ActionTypeID = ActionTypeID;
                objTrail.ActionUserID = ActionUserID;
                objTrail.BusinessSystemID = BusinessSystemID;

                _uow.SharedComponentRepository.SaveAuditTrail(objTrail);
                int result = _uow.Save();
            }

            catch (Exception ex) { throw; }
        }
        public SharedAuditTrail GetOTPByUserID(long UserID)
        {
            SharedAuditTrail objTrail = new SharedAuditTrail();
            try
            {
                var result = _uow.SharedComponentRepository.GetOTPByUserID(UserID);
                objTrail.ActionID = result.ActionID;
                objTrail.ActionTime = result.ActionTime;
                objTrail.ActionDescription = result.ActionDescription.Replace("OTP - ", "");
            }

            catch (Exception ex)
            {
            }

            return objTrail;
        }

        #endregion

       
        #region Role Management

        public int AddUpdateRole(RolesModel objRolesModel)
        {
            int Result = 0;
            try
            {
                Role objRole = new Role();
                objRole.BusinessSystemID = objRolesModel.BusinessSystemID;
                objRole.RoleID = objRolesModel.RoleID;
                objRole.RoleNameEn = objRolesModel.RoleNameEn;
                objRole.RoleNameAr = objRolesModel.RoleNameAr;
                objRole.RoleDescEn = objRolesModel.RoleDescEn;
                objRole.RoleDescAr = objRolesModel.RoleDescAr;
                objRole.Status = true;
                Result = _uow.SharedComponentRepository.AddUpdateRole(objRole);
            }
            catch (Exception ex)
            {
                Result = 0;
            }
            return Result;
        }
        public bool RemoveRole(long RoleID)
        {
            bool IsoK = false;
            try
            {
                IsoK = _uow.SharedComponentRepository.RemoveRole(RoleID);
            }
            catch (Exception ex)
            {
                IsoK = false;
            }
            return IsoK;
        }
        public List<RolesModel> GetRolesList()
        {
            try
            {
                List<RolesModel> Objreturn = new List<RolesModel>();
                var Obj = _uow.SharedComponentRepository.GetRolesList();
                RolesModel Objadd = new RolesModel();
                foreach (var item in Obj)
                {
                    Objadd = new RolesModel();
                    Objadd.BusinessSystemID = item.BusinessSystemID;
                    Objadd.RoleID = item.RoleID;
                    Objadd.RoleNameEn = Convert.ToString(item.RoleNameEn);
                    Objadd.RoleNameAr = Convert.ToString(item.RoleNameAr);
                    Objadd.RoleDescAr = Convert.ToString(item.RoleDescAr);
                    Objadd.RoleDescEn = Convert.ToString(item.RoleDescEn);
                    if (item.BusinessSystem != null)
                    {
                        Objadd.BusinessSystemNameEn = item.BusinessSystem.BusinessSystemNameEn;
                        Objadd.BusinessSystemNameAr = item.BusinessSystem.BusinessSystemNameAr;

                    }

                    Objreturn.Add(Objadd);
                }
                return Objreturn;
            }
            catch (Exception ex) { throw ex; }

        }
        public RolesModel GetRoleByID(long RoleID)
        {
            try
            {
                var result = _uow.SharedComponentRepository.GetRoleByID(RoleID);
                RolesModel ObjRolesModel = new RolesModel();
                ObjRolesModel.BusinessSystemID = result.BusinessSystemID;
                ObjRolesModel.RoleID = result.RoleID;
                ObjRolesModel.RoleNameEn = Convert.ToString(result.RoleNameEn);
                ObjRolesModel.RoleNameAr = Convert.ToString(result.RoleNameAr);
                ObjRolesModel.RoleDescAr = Convert.ToString(result.RoleDescAr);
                ObjRolesModel.RoleDescEn = Convert.ToString(result.RoleDescEn);
                if (result.BusinessSystem != null)
                {
                    ObjRolesModel.BusinessSystemNameEn = result.BusinessSystem.BusinessSystemNameEn;
                    ObjRolesModel.BusinessSystemNameAr = result.BusinessSystem.BusinessSystemNameAr;

                }


                return ObjRolesModel;
            }
            catch (Exception ex) { throw ex; }

        }
        public bool VerifyDuplicateRoleToAdd(string RoleNameEn, string RoleNameAr, long BusinessSystemID)
        {
            var dbResult = _uow.SharedComponentRepository.VerifyDuplicateRoleToAdd(RoleNameEn, RoleNameAr, BusinessSystemID);

            if (dbResult != null)
            {
                if (dbResult.RoleID > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            else
            {
                return true;
            }
        }
        public bool VerifyDuplicateRoleToUpdate(string RoleNameEn, string RoleNameAr, long BusinessSystemID, long RoleID)
        {
            var dbResult = _uow.SharedComponentRepository.VerifyDuplicateRoleToUpdate(RoleNameEn, RoleNameAr, BusinessSystemID, RoleID);
            if (dbResult != null)
            {
                if (dbResult.RoleID > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            else
            {
                return true;
            }
        }

        #endregion
    }
}
